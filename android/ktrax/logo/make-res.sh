#!/bin/bash

#
# Creates PNGs of logos.
#
# TODO/FIXME
# * There is a manual step involved.  Argh.
# * RowingCAS is a layer of PowerCAS in the SVG... Use
#   an Inkscape command line parameter?  How did this 
#   ever work?!
# 

#
# find inkscape or falls back to MacOS path
#

inkscape=`which inkscape`
if [[ ! -x "$inkscape" ]]
then
  inkscape=/Applications/Inkscape.app/Contents/Resources/bin/inkscape
fi
if [[ -x "$inkscape" ]]
then
  echo "Inkscape found at : \"$inkscape\""
else
  echo "No inkscape executable found."
  exit 1
fi

# doitall <logo-basename> <directory>
function doitall() {
  logo=$1
  directory=$2

  echo "Generating for $logo"
  echo "Destination directory: $directory"

  # required temporary folders.
  mkdir -p "build"

  # Parameters
  hires_size=2400

  # File names, directories
  src=$logo.svg
  hires=build/$logo-hires.png
  hires_ds=build/$logo-hires-ds.png
  dest=../app/src/$directory/res/drawable
  filename=ic_launcher.png

  doit() {
    mkdir -p "$dest-$1"
    convert $hires -resize "$2"x"$2" -strip $dest-$1/$filename
  }

  usageGimpHireDs() {
    echo "Please use gimp to add a drop shadow to build/$logo-hires.png"
    echo "Parameters: 100px left, 100px down, 100px blur, 80% opacity"
    echo "Allow resizing: True"
    echo "Filters -> Light and Shadow -> Drop Shadow"
    echo "Save as: $hires_ds"
  }

# Make high resolution PNG
# Commandline: $inkscape --help
$inkscape --export-area-page           \
  --export-png=$hires                  \
  --export-width=$hires_size           \
  --export-height=$hires_size          \
  $src

  doit mdpi    48
  doit hdpi    72
  doit xhdpi   96
  doit xxhdpi 144

  # Return if the drop-shadow image isn't available.  This
  # is needed for Google Play Store listings, favicon,
  # web site logo etc.
  if [[ ! -f "$hires_ds" ]]
  then
    usageGimpHireDs
    return
  fi

  convert $hires_ds -resize 512x512 build/$logo-512.png
  convert $hires_ds -resize 64x64 build/$logo-favicon.png

  # For iTunes Connect: No alpha
  convert $hires -resize 1024x1024 -background white -alpha remove -flatten -alpha off build/$logo-1024.png
}

doitall powercas powercas
doitall rowingcas rowingcas
doitall ktrax ktrax_android
