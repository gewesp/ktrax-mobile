#! /bin/sh


./make-res.sh

logo=rowingcas
hires=build/$logo-hires.png
dest=../../../ios/RowingCAS/build/logo
filename=icon

resize() {
  convert $hires -resize "$1"x"$1" -strip $dest/$filename-$1.png
}

echo "Generating iOS logos in $dest"

# 2x/3x 29
resize 58
resize 87

# 2x/3x 40
resize 80
resize 120

# 2x/3x 60
resize 120
resize 180

# 1x/2x 76 (iPad)
resize 76
resize 152
