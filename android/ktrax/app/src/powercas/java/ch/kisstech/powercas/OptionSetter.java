package ch.kisstech.ktrax;

import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import ch.kisstech.AndroidUtil;

public class OptionSetter {

    private static Map<Integer, Integer> vehicleTypeMenuToId;
    private static Map<Integer, Integer> vehicleTypeIdToMenu;

    static {
        Map<Integer, Integer> temp = new TreeMap<Integer, Integer>();

        temp.put(R.id.type_pedestrian, VehicleType.PowerCas.VEHICLE_PEDESTRIAN);
        // temp.put(R.id.type_bicycle, VehicleType.PowerCas.VEHICLE_BICYCLE);
        temp.put(R.id.type_passenger_car, VehicleType.PowerCas.VEHICLE_PASSENGER_CAR);
        temp.put(R.id.type_excavator, VehicleType.PowerCas.VEHICLE_EXCAVATOR);
        temp.put(R.id.type_roller, VehicleType.PowerCas.VEHICLE_ROLLER);
        temp.put(R.id.type_truck, VehicleType.PowerCas.VEHICLE_TRUCK);

        vehicleTypeMenuToId = Collections.unmodifiableMap(temp);

        temp = new TreeMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> entry : vehicleTypeMenuToId.entrySet())
        {
            temp.put(entry.getValue(), entry.getKey());
        }

        vehicleTypeIdToMenu = Collections.unmodifiableMap(temp);
    }

    public static void specificSyncMenu(
            final PersistentLogic pers, final Menu menu) {
        Integer menuId = vehicleTypeIdToMenu.get(pers.getVehicleType());
        if (menuId == null)
            return;

        MenuItem t = menu.findItem(menuId);
        if (t == null)
            return;

        t.setChecked(true);
    }

    public static boolean specificSetOption(
            final PersistentLogic pers_unused, final Context ctx, final int id) {
        switch (id) {
            case R.id.set_name1:
                AndroidUtil.showTextEditDialog(
                        ctx, new PersistentLogic.NameSetter(1),
                        Config.NAME1_TITLE,
                        Config.NAME1_MESSAGE,
                        PersistentLogic.getInstance().getName1(),
                        Config.NAME1_HINT);
                return true;

                /*
            case R.id.set_name2:
                AndroidUtil.showTextEditDialog(
                        ctx, new PersistentLogic.NameSetter(2),
                        Config.NAME2_TITLE,
                        Config.NAME2_MESSAGE,
                        PersistentLogic.getInstance().getName2(),
                        Config.NAME2_HINT);
                return true;
                */

            default:
                break;
        }

        Integer ret = vehicleTypeMenuToId.get(id);
        if (ret != null) {
            pers_unused.setVehicleType(ret);
            return true;
        }

        return false;
    }
}
