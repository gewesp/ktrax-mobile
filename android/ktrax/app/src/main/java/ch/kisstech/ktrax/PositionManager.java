//
// Position manager class.
//
// Handles location updates, NMEA, speed/COG etc.
//
// Uses callbacks, no threading involved.
//
// NMEA parsing is just for cross-checking with the usual position updates.
// on S4 active, both seem pretty consistent as long as GPS is on.
//
// Cross checking between onLocationChanged() and onNmeaReceived() shows
// that the values normally agree very well, i.e. any filtering is 
// done in the GPS module.  (Samsung Galaxy S4 active)
// onLocationChanged() is not called (?) when GPS is OFF or acquiring.
// onLocationChanged() *is* called regularly even on standstill.
// 

package ch.kisstech.ktrax;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.location.LocationProvider;

import android.util.Log;

import androidx.core.content.ContextCompat;

import java.util.*;

import ch.kisstech.AndroidUtil;


// NMEA listener not necessary for normal operation.
public class PositionManager
        implements LocationListener /* , GpsStatus.NmeaListener */ {

    static final String TAG = "Ktrax.PositionManager";

    // For callback...
    NetworkManager network_manager;

    private final PositionManagerStatus status;

    private LocationManager locationManager;

    public static PersistentLogic getGlobals() {
        return PersistentLogic.getInstance();
    }

    PositionManagerStatus getStatus() {
        return status;
    }

    void updateStatus(final boolean new_position_valid) {
        status.updateStatus(new_position_valid);
    }

    void showGpsEnableDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle   (R.string.activate_gps_title  )
                .setMessage(R.string.activate_gps_message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AndroidUtil.settingsLocation(activity);
                    }
                })
                .create()
                .show();
    }

    // private volatile PositionTime nmea_position_time;

    // gpstime_ms - fix_systime of location updates.  Should
    // drift only slowly, may be used to calculate approximate
    // GPS time if only systime is available.
    // private long timediff;

    // public PositionTime nmeaPositionTime() {
    //    return nmea_position_time;
    // }

    public PositionManager(Activity activity,
                           LocationManager lm_in,
                           NetworkManager network_manager_in) {
        status = new PositionManagerStatus();
        try {
            Log.i(TAG, "Starting up");
            network_manager = network_manager_in;
            locationManager = lm_in;
            status.setEnabled(false);

            // nmea_position_time = new PositionTime();

            if (null == network_manager) {
                throw new Exception("NULL network manager passed!");
            }

            List<String> providers = locationManager.getProviders(true);

            for (String provider : providers) {
                Log.i(TAG, "Location provider: " + provider);
            }

            // startGpsUpdates();

            // locationManager.addNmeaListener(this);
            final boolean e = locationManager.isProviderEnabled(
                    LocationManager.GPS_PROVIDER);
            status.setEnabled(e);
            Log.i(TAG, "GPS enabled as a location provider: " + e);

            if (!e) {
                showGpsEnableDialog(activity);
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot initialize GPS: " + e.getMessage());
        }
    }

    // Idempotent start/stop functions
    synchronized public void startGpsUpdates() {
        Log.i(TAG, "Starting GPS updates ...");
        if (ContextCompat.checkSelfPermission(
                getGlobals().theGlobalActivity(), 
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {

          locationManager.requestLocationUpdates(
                  LocationManager.GPS_PROVIDER,
                  Config.GPS_UPDATE_TIME,
                  Config.GPS_UPDATE_DISTANCE,
                  this);

          status.updates_allowed = true;

          Log.i(TAG, "Starting GPS updates: OK");
        } else {
          Log.i(TAG, "Starting GPS updates: Denied by user");
          status.updates_allowed = false;
        }
    }

    synchronized public void stopGpsUpdates() {
        Log.i(TAG, "Stopping GPS updates");

        if (status.updates_allowed) {
          locationManager.removeUpdates(this);
        }
    }

    // Yes, in Java you do everything by hand.  Even destructors.
    public void shutdown() {
        Log.i(TAG, "Shutting down");

        // qlocationManager.removeNmeaListener(this);
        stopGpsUpdates();
        network_manager = null;
    }

    // Compute derivatives if dt < 1.5s
    private static final double MAX_DERIVATIVE_DT = 1.5;

    // Don't compute derivatives if accuracy is worse than that [m].
    private static final float MAX_DERIVATIVE_ACCURACY = 120.5f;

    // Compute turn rate only if we have more than that speed [m/s].
    private static final double MIN_DERIVATIVE_SPEED = 15.0;

    // Exponential moving average factors for vertical speed
    // and turn rate.  The closer to 1, the slower the values change.
    private static final double DERIVATIVE_FACTOR_VS        = 0.9;
    private static final double DERIVATIVE_FACTOR_TURN_RATE = 0.9;

    // Previous location
    private Location prev;

    // Computed derivatives (not provided by Android)
    // Vertical speed, smoothed by exponential moving average [m/s]
    private double vertical_speed = 0.0;
    // Turn rate, smoothed by exponential moving average [degree/s]
    private double turn_rate      = 0.0;
    private void updateDerivatives(Location curr) {
      if (prev == null) {
        prev = curr;
        vertical_speed = 0.0;
        turn_rate      = 0.0;
        return;
      }

      final double dt = 1e-3 * (curr.getTime() - prev.getTime());
      if (   prev.hasAccuracy() && curr.hasAccuracy()
          && dt > 0 && dt <= MAX_DERIVATIVE_DT
          && prev.getAccuracy() <= MAX_DERIVATIVE_ACCURACY
          && curr.getAccuracy() <= MAX_DERIVATIVE_ACCURACY) {

        if (prev.hasAltitude() && curr.hasAltitude()) {
          final double dalt = curr.getAltitude() - prev.getAltitude();
          vertical_speed = DERIVATIVE_FACTOR_VS * vertical_speed
            + (1.0 - DERIVATIVE_FACTOR_VS) * (dalt / dt);
        } else {
          vertical_speed = 0.0;
        }

        if (   prev.hasSpeed() && curr.hasSpeed()
            && prev.getSpeed() >= MIN_DERIVATIVE_SPEED
            && curr.getSpeed() >= MIN_DERIVATIVE_SPEED
            && prev.hasBearing() && curr.hasBearing()) {
          double dcourse = curr.getBearing() - prev.getBearing();
          if (dcourse >=  180.0) { dcourse -= 360.0; }
          if (dcourse <= -180.0) { dcourse += 360.0; }
          turn_rate = DERIVATIVE_FACTOR_TURN_RATE * turn_rate
            + (1.0 - DERIVATIVE_FACTOR_TURN_RATE) * (dcourse / dt);
        } else {
          turn_rate = 0.0;
        }
      } else {
        vertical_speed = 0.0;
        turn_rate      = 0.0;
      }

      prev = curr;
    }


    // LocationListener interface
    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Location update received");
        updateDerivatives(location);
        // TODO: NullPointerException here on first GPS reception (?),
        // also seen on shutdown.  Connection with obnoxious
        // Google dialog asking to share position data?
        // We *should* be in the main thread, so no synchronization.
        // Apparently, onLocationChanged() is still being called after
        // removeUpdates().  Argh.....
        if (null == network_manager) {
            Log.wtf(TAG, "NULL network_manager in onLocationChanged()?!");
        } else {
            network_manager.receiveGpsUpdate(
                location, vertical_speed, turn_rate);
        }
    }

    // Extract lat/lon/speed/course from NMEA as well to cross-check with
    // onLocationChanged.
    // Unit of timestamp: milliseconds (undocumented), time base:
    // unprecise local phone time.
    // Disabled for now due to usage of Scanner.

    /*
    @Override
    public void onNmeaReceived(long timestamp, String nmea) {
        // Disabled for now due to usage of Scanner.

        try {
            if (nmea.startsWith("$GPRMC")) {

                nmea_position_time = NMEA.ParseGPRMC(nmea);

            }
        } catch (Exception e) {
            Log.e(TAG, "Invalid NMEA string: " + nmea);
        }

    }
    */

    @Override
    public void onProviderDisabled(String provider) {
        Log.i(TAG, "Location provider disabled: " + provider);
        if (LocationManager.GPS_PROVIDER.equals(provider)) {
            status.setEnabled(false);
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i(TAG, "Location provider enabled: " + provider);
        if (LocationManager.GPS_PROVIDER.equals(provider)) {
            status.setEnabled(true);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, android.os.Bundle bundle) {
        String sstatus = "Unknown";
        if (LocationProvider.OUT_OF_SERVICE == status) {
            sstatus = "Out of service";
        } else if (LocationProvider.TEMPORARILY_UNAVAILABLE == status) {
            sstatus = "Temporarily unavailable";
        } else if (LocationProvider.AVAILABLE == status) {
            sstatus = "Available";
        }
        Log.i(TAG, "Location provider: " + provider + "; status: " + sstatus);
    }

}
