//
// Ktrax page update interface
// 

package ch.kisstech.ktrax;

public interface IPage {

    // Contract:  Update the page with the given information and post
    // a redraw.
    void update(final DisplayInfo info);

}
