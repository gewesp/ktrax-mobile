//
// Generate audio samples of sine waves from frequency/duration lists,
// with soft ramps to fade in and out of the tone.
//


package ch.kisstech.ktrax;

import android.util.Log;

import java.lang.Math;
import java.lang.System;


public class MelodyGenerator {
    private final static String TAG = "Ktrax.MelodyGenerator";

    private final double sample_rate;
    private final double off_ramp   ;
    private final double  on_ramp   ;

    // Returns frequency of the note the given halftone above or
    // below c''
    static double note(int halftone) {
        return 523.25 * Math.pow(2.0, halftone / 12.0);
    }

    MelodyGenerator(double sample_rate_in, 
                    double off_ramp_in, 
                    double on_ramp_in) {
      sample_rate = sample_rate_in;
      off_ramp    = off_ramp_in   ;
       on_ramp    =  on_ramp_in   ;

      assert 0   <= off_ramp   ;
      assert 0   <= on_ramp    ;
      assert 100 <= sample_rate;
    }

    // Returns continous envelope in [0, 1] for given on-time, taking into
    // account the on and off ramps.
    // 0 <= t, returns 0 for t >= ontime.
    double envelope(double t, double ontime) {
        assert 0 <= t;
        if (t >= ontime) {
            return 0.0;
        } else {
            if (t < on_ramp) {
                return t / on_ramp;
            } else if (ontime - off_ramp < t) {
                return (ontime - t) / off_ramp;
            } else {
                return 1.0;
            }
        }
    }

    // Make a sequence of beeps with given frequency, duration and ontime.
    short[] make_beeps(
        final double amplitude,
        double[] frequency, double[] duration, double[] ontime) {
        final int n = frequency.length;
        Log.i(TAG, "Making melody from " + n + " note(s)");
        assert duration.length == n;
        assert ontime.length == n;

        // Create n beeps in individual arrays.
        int total_samples = 0;
        short[][] beeps = new short[n][0];
        for (int i = 0; i < n; ++i) {
            beeps[i] = 
                make_beep(amplitude, frequency[i], duration[i], ontime[i]);
            total_samples += beeps[i].length;
        }

        // Concatenate them all.
        short[] ret = new short[total_samples];
        int pos = 0;
        for (int i = 0; i < n; ++i) {
            System.arraycopy(beeps[i], 0, ret, pos, beeps[i].length);
            pos += beeps[i].length;
        }
        Log.i(TAG, "Total number of samples: " + ret.length);
        return ret;
    }

    // Make a single beep with given frequency, total duration and
    // on-time, which should be < duration.
    short[] make_beep(
        final double amplitude, 
        final double frequency, 
        final double duration, 
        final double ontime) {
        assert amplitude >= 0;
        assert amplitude <= 1;
        assert duration  > 0;
        assert 0 <= ontime;
        assert      ontime < duration;

        int size = (int) (duration * sample_rate + .5);
        short ret[] = new short[size];

        for (int i = 0; i < size; ++i) {
            double t = (double) i / sample_rate;
            ret[i] = (short)
                (32760 * amplitude * envelope(t, ontime) * Math.sin(2 * Math.PI * frequency * t));
            // if (0 != ret[i]) {Log.d(TAG, "Sample: " + ret[i]);}
        }
        return ret;
    }

}
