package ch.kisstech.ktrax;

// Configuration for RowingCAS

public class Config {
    //////////////////////////////////////////////////////////////////////// 
    // Communication parameters
    //////////////////////////////////////////////////////////////////////// 

    final static public int LOOP_INTERVAL_FOREGROUND
        = BuildConfig.LOOP_INTERVAL_FOREGROUND;
    final static public int LOOP_INTERVAL_BACKGROUND
        = BuildConfig.LOOP_INTERVAL_BACKGROUND;

    // Threshold to detect orientation change (dimensionless,
    // delta of normalized acceleration vector)
    final static public float ORIENTATION_CHANGE_THRESHOLD = 0.08f;

    // Interval to send hello and info packets [ms].
    // In addition, they are sent on certain status changes.
    // They are repeated for the specified time.
    final static public int HELLO_SEND_INTERVAL = 60 * 1000;
    // (Approximate) number of repeats for hello packets.
    final static public int N_HELLO_REPEATS = 8;

    // Receive timeout [ms].  Applicable to RTT and also in the case
    // no packet comes over at all.
    // Should probably not be shorter than LOOP_INTERVAL.
    final static public int RECEIVE_TIMEOUT_RTT_MAX = 5000;

    // Longest interval to update UI.  Reception of OK packets
    // always posts an immediate update.
    final static public int UPDATE_UI_INTERVAL = 1000;

    // Audio notification for system status.  OK: dit, ERROR: dit dit dit
    // First time (after startup) [ms]
    final static public long AUDIO_STATUS_FIRST_MS = 5000;
    // How long in between [ms]
    final static public long AUDIO_STATUS_PERIOD_MS = 30000;
    // Duration of notification [ms]
    final static public long AUDIO_STATUS_DURATION_MS = 1000;

    // We consider our position invalid if we haven't
    // received an update in 4 seconds.
    final static public int GPS_TIMEOUT = 4000;

    // Interval for which TEST button is considered active after pressing [ms]
    // Show status indications during this time
    final static public int TEST_BUTTON_ACTIVE = 60000;

    // FAST update rate during the first 30 packets
    final static public int NUMBER_STARTUP_PACKETS = 30;

    ////////////////////////////////////////////////////////////////////////
    // Build-dependant config items
    // See the flavor sections in app/build.gradle for definitions
    ////////////////////////////////////////////////////////////////////////

    // Radar scale [m].  The value corresponds to the distance at
    // the outer circle.
    final static public float RADAR_SCALE = BuildConfig.RADAR_SCALE;
    final static public int UDP_PORT = BuildConfig.UDP_PORT;
    final static public boolean SHOW_TRACKING_STATUS =
        BuildConfig.SHOW_TRACKING_STATUS;
    final static public boolean USE_TRACK_UP = BuildConfig.USE_TRACK_UP;
    final static public boolean DRAW_RELATIVE_ALTITUDE =
        BuildConfig.DRAW_RELATIVE_ALTITUDE;

    // Time [ms] and distance [m] for GPS updates, see
    // requestLocationUpdates().  An update is sent iff the time has
    // elapsed *and* the minimum distance has been covered.
    final static public int GPS_UPDATE_TIME = 
        BuildConfig.GPS_UPDATE_TIME;
    final static public float GPS_UPDATE_DISTANCE = 
        BuildConfig.GPS_UPDATE_DISTANCE;

    // Speed above which we consider ourselves in motion [m/s]
    final static public float MOVEMENT_THRESHOLD_SPEED =
        BuildConfig.MOVEMENT_THRESHOLD_SPEED;

    // Update rates [ms]
    final static int UPDATE_RATE_NORMAL = BuildConfig.UPDATE_RATE_NORMAL;

    // Vehicle
    final static int DEFAULT_VEHICLE_TYPE = BuildConfig.DEFAULT_VEHICLE_TYPE;

    // RowingCAS/KTrax/...
    final static String APP_NAME = BuildConfig.APP_NAME;
    final static String SIGNATURE_TEXT = BuildConfig.SIGNATURE_TEXT;
    final static String TRACKING_BASE_URI = BuildConfig.TRACKING_BASE_URI;
    final static String HELP_DOC_URI      = BuildConfig.HELP_DOC_URI;

    // Options menu
    final static String NAME1_TITLE = BuildConfig.NAME1_TITLE;
    final static String NAME2_TITLE = BuildConfig.NAME2_TITLE;
    final static String NAME3_TITLE = BuildConfig.NAME3_TITLE;

    final static String NAME1_MESSAGE = BuildConfig.NAME1_MESSAGE;
    final static String NAME2_MESSAGE = BuildConfig.NAME2_MESSAGE;
    final static String NAME3_MESSAGE = BuildConfig.NAME3_MESSAGE;

    final static String NAME1_HINT = BuildConfig.NAME1_HINT;
    final static String NAME2_HINT = BuildConfig.NAME2_HINT;
    final static String NAME3_HINT = BuildConfig.NAME3_HINT;
}
