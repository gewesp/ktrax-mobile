//
// Vehicle types.  Must be in line with respective section
// in ktrax/constants.h
//

package ch.kisstech.ktrax;

public class VehicleType {
    // base types
    public final static int UNKNOWN = 0;

    // Human powered
    public final static int ROWING_BOAT = 101;

    // Wind powered
    public final static int SAILING_YACHT = 111;

    // Motor powered
    public final static int SMALL_MOTORBOAT = 121;

    // Aircraft:
    public final static int GLIDER      = 1;
    public final static int HELICOPTER  = 3;
    public final static int HANG_GLIDER = 6;
    public final static int PARAGLIDER  = 7;
    public final static int POWER_PLANE = 8;
    public final static int JET         = 9;
    public final static int UAV         = 10;

    public class PowerCas {
        // Land vehicles: 21-99
        public final static int VEHICLE_PEDESTRIAN = 21;
        public final static int VEHICLE_BICYCLE = 22;

        public final static int VEHICLE_PASSENGER_CAR = 30;
        // Powered vehicles typically found on construction sites
        public final static int VEHICLE_EXCAVATOR = 31;
        public final static int VEHICLE_ROLLER = 32;
        public final static int VEHICLE_TRUCK = 33;
    }
}
