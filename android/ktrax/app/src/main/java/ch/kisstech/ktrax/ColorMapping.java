//
// Tracking symbol colors
//

package ch.kisstech.ktrax;

public class ColorMapping { 
    public static String colorNameFromId(final int id) {
      switch(id) {
        case R.id.set_color_f00: return "Red"   ;
        case R.id.set_color_0f0: return "Green" ;
        case R.id.set_color_ff0: return "Yellow";
        case R.id.set_color_00f: return "Blue"  ;
        case R.id.set_color_60a: return "Purple";
        case R.id.set_color_a50: return "Brown" ;
        case R.id.set_color_0ff: return "Cyan"  ;
        case R.id.set_color_000: return "Black" ;
        case R.id.set_color_fff: return "White" ;
        default: return "";
      }
    }

    public static String HtmlColorFromId(final int id) {
      switch(id) {
        case R.id.set_color_f00: return "#ff0000";
        case R.id.set_color_0f0: return "#00ff00";
        case R.id.set_color_ff0: return "#ffff00";
        case R.id.set_color_00f: return "#0000ff";
        case R.id.set_color_60a: return "#6600aa";
        case R.id.set_color_a50: return "#aa5500";
        case R.id.set_color_0ff: return "#00ffff";
        case R.id.set_color_000: return "#000000";
        case R.id.set_color_fff: return "#ffffff";
        default: return "-";
      }
    }
}
