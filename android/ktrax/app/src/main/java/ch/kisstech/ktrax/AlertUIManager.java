//
// Alert and notification sounds.
//
// Android ID:
// "A 64-bit number (as a hex string) that is randomly 
// generated when the user first sets up the device and 
// should remain constant for the lifetime of the user's device."
// http://developer.android.com/reference/android/provider/Settings.Secure.html
//
// TODO: Unique device/user/installation ID.
// TODO: Move this TODO, has nothing to do with this file!
// See http://android-developers.blogspot.ch/2011/03/identifying-app-installations.html
// http://developer.android.com/reference/android/provider/Settings.Secure.html#ANDROID_ID
// ANDROID_ID seems to have problems, though:
// https://groups.google.com/forum/#!topic/android-developers/U4mOUI-rRPY
// https://code.google.com/p/android/issues/detail?id=10639
// Reported in 2.2, claimed fixed Mar 2011, but it's not clear whether
// updated devices would get a new ID.
//
// Time bases:
// * AndroidUtil.systemTimeMs() --- cf. documentation there
// * GPS date/time --- comes with valid GPS fixes
// * Server time --- not yet
//
// TODO: All alarms/notification sounds same time?  (Currently, only
// the 'happy sound' is 1 seconds, all others are 2 seconds, but 
// not necessarily so.  See also the note about immediate playback
// in NetworkManager.
//

package ch.kisstech.ktrax;

import android.content.Intent;
import android.util.Log;

public class AlertUIManager {

    private final static String TAG = "Ktrax.AlertUIMangager";

    // Default values for frequency, ramps, times etc.
    private final static double AMPLITUDE_FULL = 1.0;
    private final static double AMPLITUDE_SOFT = 2e-2;

    private final static double FREQUENCY = 860;
    private final static double OFF_RAMP  = .015;
    private final static double ON_RAMP   = .005;

    private final SoundManager sound_manager;

    // private final short[] samples_SILENCE;
    private final short[] samples_AL1;
    private final short[] samples_AL2;
    private final short[] samples_AL3;
    private final short[] samples_SOS;

    private final short[] samples_1_DIT;
    private final short[] samples_3_DIT;

    private final short[] samples_OK;
    private final short[] samples_OFF;
    private final short[] samples_ERROR;

    // The various notifications
    public static final int NOTIFICATION_NONE    = 0;
    public static final int NOTIFICATION_ALARM_1 = 1;
    public static final int NOTIFICATION_ALARM_2 = 2;
    public static final int NOTIFICATION_ALARM_3 = 3;
    public static final int NOTIFICATION_SOS     = 4;

    public static final int NOTIFICATION_1_DIT   = 11;
    public static final int NOTIFICATION_3_DIT   = 13;

    public static final int NOTIFICATION_OK      = 100;
    public static final int NOTIFICATION_ERROR   = 101;

    public static final int NOTIFICATION_OFF     = 200;

    // Silence duration (s)
    public static final float SILENCE_DURATION = 0.25f;

    AlertUIManager() {
        sound_manager = new SoundManager();

        Log.i(TAG, "Generating beeps and melodies");
        MelodyGenerator mg = new MelodyGenerator(
            sound_manager.getSampleRate(), OFF_RAMP, ON_RAMP);

        // 0.2 sec
        // samples_SILENCE = mg.make_beep(0, 0.2, 0);

        final double f = FREQUENCY;
        // Duration and on-time for an eight note
        final double ed = 0.25;
        final double eo = 0.18;
        // Duration of a quarter note
        final double qd = 2 * ed;
        // Duration and on-time of half note---should be impressive!
        final double hd = 4 * ed;
        final double ho = .97;
        // And sixteenth...
        final double sd = .125;
        final double so = .11 ;

        // 2 sec
        // dit dit ..... dit dit ......
        samples_AL1 = mg.make_beeps(
            AMPLITUDE_FULL,
            new double[] {f , f , 0.0   , f , f , 0.0    },
            new double[] {ed, ed, qd    , ed, ed, qd     },
            new double[] {eo, eo, 0.0   , eo, eo, 0.0    }
        );

        // 2 sec
        // dit dit dit dit dit dit dit dit
        samples_AL2 = mg.make_beeps(
            AMPLITUDE_FULL,
            new double[] {f , f , f , f , f , f , f , f },
            new double[] {ed, ed, ed, ed, ed, ed, ed, ed},
            new double[] {eo, eo, eo, eo, eo, eo, eo, eo}
        );

        // 2 sec
        // deeeeeet deeeeeet
        samples_AL3 = mg.make_beeps(
            AMPLITUDE_FULL,
            new double[] {f , f },
            new double[] {hd, hd},
            new double[] {ho, ho}
        );

        samples_SOS = samples_AL3;

        // 0.5 sec
        // SOFT: dit -
        samples_1_DIT = mg.make_beeps(
            AMPLITUDE_SOFT,
            new double[] {f , 0.0},
            new double[] {ed, ed },
            new double[] {eo, 0.0}
        );

        // 1 sec
        // SOFT: dit dit dit -
        samples_3_DIT = mg.make_beeps(
            AMPLITUDE_SOFT,
            new double[] {f , f , f , 0.0},
            new double[] {ed, ed, ed, ed },
            new double[] {eo, eo, eo, 0.0}
        );

        // Notes for triad and Beethoven's 5th
        double c2 = MelodyGenerator.note(0);
        double e2 = MelodyGenerator.note(4);
        double g2 = MelodyGenerator.note(7);
        double c3 = MelodyGenerator.note(12);

        double ef2 = MelodyGenerator.note(3);

        // 1 sec
        samples_OK = mg.make_beeps(
            AMPLITUDE_FULL,
            new double[] {c2, e2, g2, c3, 0.0},
            new double[] {sd, sd, sd, sd, qd },
            new double[] {so, so, so, so, 0.0}
        );
       
        // 1sec
        samples_OFF = mg.make_beeps(
            AMPLITUDE_FULL,
            new double[] {c3, g2, e2, c2, 0.0},
            new double[] {sd, sd, sd, sd, qd },
            new double[] {so, so, so, so, 0.0}
        );

        // 2 sec
        samples_ERROR = mg.make_beeps(AMPLITUDE_FULL,
            new double[] {0.0, g2, g2, g2, ef2},
            new double[] {ed , ed, ed, ed,  hd},
            new double[] {eo , eo, eo, eo,  ho}
        );

        Log.i(TAG, "Beeps and melodies generated OK");
    }

    public void shutdown() {
        Log.i(TAG, "Shutting down");
        sound_manager.shutdown();
    }

    public void SetNotification(final int notification) {
        // Log.d(TAG, "Notification: " + notification);

        switch(notification) {
            case NOTIFICATION_NONE:
                // sound_manager.setSamples(samples_SILENCE);
                // One eight
                sound_manager.setSilence(SILENCE_DURATION);
                break;
            case NOTIFICATION_ALARM_1:
                sound_manager.setSamples(samples_AL1);
                break;
            case NOTIFICATION_ALARM_2:
                sound_manager.setSamples(samples_AL2);
                break;
            case NOTIFICATION_ALARM_3:
                sound_manager.setSamples(samples_AL3);
                break;
            case NOTIFICATION_SOS:
                sound_manager.setSamples(samples_SOS);
                break;
            case NOTIFICATION_OK:
                sound_manager.setSamples(samples_OK);
                break;
            case NOTIFICATION_OFF:
                sound_manager.setSamples(samples_OFF);
                break;

            case NOTIFICATION_1_DIT:
                sound_manager.setSamples(samples_1_DIT);
                break;
            case NOTIFICATION_3_DIT:
                sound_manager.setSamples(samples_3_DIT);
                break;
            case NOTIFICATION_ERROR:
                sound_manager.setSamples(samples_ERROR);
                break;

            default:
                Log.e(TAG, "Unknown notification: " + notification);
                break;
        }
    }

}
