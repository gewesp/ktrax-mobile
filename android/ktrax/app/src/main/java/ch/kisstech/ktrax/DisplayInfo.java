//
// Collection of all info to be displayed
//

package ch.kisstech.ktrax;

public class DisplayInfo {
    public PositionTime          pt      = new PositionTime         ();
    public ServerInfo            si      = new ServerInfo           ();
    public ClientInfo            ci      = new ClientInfo           ();
    public PositionManagerStatus pmstat  = new PositionManagerStatus();
    public NetworkManagerStatus  netstat = new NetworkManagerStatus ();
    public TrackingStatus        trackstat = new TrackingStatus     ();

    public DisplayInfo() {}

    public boolean allOk() {
        return pmstat.ok() && netstat.ok();
    }
}
