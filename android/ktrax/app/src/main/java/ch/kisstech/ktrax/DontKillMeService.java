//
// The Android service class
//
// Tons of boilerplate necessary to keep the system from
// killing us without warning.
//
// Also shows an icon and some useless text.
//

package ch.kisstech.ktrax;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


// Another Most Useless API verbosity...
// Funny enough, this gives a different number than is displayed
// on the status bar on the Galaxy S4 (98% instead of 97%).
 class BatteryStateReceiver extends BroadcastReceiver {
    private final static String TAG = "Ktrax.BatteryState";
    @Override
    public void onReceive(Context arg0, Intent intent) {
        final int lvl   = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        final int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        final float pct = scale > 0 ? 100.0f * lvl / scale : -1.0f;

        Log.i(TAG, "Battery level: " + pct);
        PersistentLogic.getInstance().battery_level_percent = pct;
     }
}


public class DontKillMeService extends Service {

  private final static String TAG = "Ktrax.DontKillMeService";

 // A app-wide unique ID identifying the notification
  private final static int ONGOING_NOTIFICATION_ID = 4711;

  private final BatteryStateReceiver batRec = new BatteryStateReceiver();


  // TODO: Remove this code once we only target API 28 or above.
  // See https://stackoverflow.com/questions/47531742/startforeground-fail-after-upgrade-to-android-8-1
  
  //
  // This is the trick:
  // " [...] if the service is declared to run in the foreground (discussed 
  // later), then it will almost never be killed. [...]"
  // http://developer.android.com/guide/components/services.html#Foreground
  // Oct 2021 WTF, "almost never" ?!! How helpful...
  //

  private void makeForegroundServiceVeryOld() {
      Log.i(TAG, "Initializing Foreground Service for Android versions older than 8.0");

      // swich
      PendingIntent pendingIntent = PendingIntent.getActivity(
              this, 0, new Intent(this, RadarActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

      Notification not = new NotificationCompat.Builder(this)
               .setContentTitle(getText(R.string.notification_title))
               .setContentText (String.format(getString(R.string.notification_running), PersistentLogic.getInstance().getAppVersion()))
               .setSmallIcon(R.drawable.notification_icon_r)
               // .setLargeIcon(aBitmap)
               .setContentIntent(pendingIntent)
               .build();

      startForeground(ONGOING_NOTIFICATION_ID, not);

      Log.i(TAG, "Foreground service running");
  }

  // https://stackoverflow.com/questions/47531742/startforeground-fail-after-upgrade-to-android-8-1
  // This is the startMyOwnForeground() function from SO
  private void makeForegroundServiceAPI28OrLater() {
    Log.i(TAG, "Initializing Foreground Service for Android 8.0 or later");
    String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
    String channelName = "KTrax Background Service";
    NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
    chan.setLightColor(Color.BLUE);
    chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    assert manager != null;
    manager.createNotificationChannel(chan);

    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
    Notification notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.notification_icon_r)
            .setContentTitle("KTrax is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build();

    startForeground(2, notification);

    Log.i(TAG, "Foreground service running");
  }

  // More useless boilerplate as of October 2021.
  // As of October 2021, call boilerplate code here.
  // Previous versions had it in onStartCommand().
  @Override
  public void onCreate() {
      super.onCreate();

      // Wow.  Apparently, this refers to Android 8.0, released in August 2017.
      // https://developer.android.com/reference/android/os/Build.VERSION_CODES#O
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Log.i(TAG, "onCreate(): Initializing Foreground Service for Android 8.0 or later");
        makeForegroundServiceAPI28OrLater();
      }

      Log.i(TAG, "onCreate()");

      // https://developer.android.com/reference/android/content/Intent#ACTION_BATTERY_CHANGED
      // Broadcast Action: This is a sticky broadcast containing the charging state, 
      // level, and other information about the battery. See BatteryManager for 
      // documentation on the contents of the Intent.
      registerReceiver(batRec, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
  }


  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
      Log.i(TAG, "onStartCommand()");

      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
        makeForegroundServiceVeryOld();
      }

      // If we get killed, after returning from here, restart
      // TODO: But what means 'we get killed'??
      return START_STICKY;
  }

  @Override
  public IBinder onBind(Intent intent) {
      Log.i(TAG, "onBind()");
      // We don't provide binding, so return null
      return null;
  }

  @Override
  public void onDestroy() {
      Log.i(TAG, "onDestroy()");
      // Notification removed anyway, no need to call stopForeground(true).
      unregisterReceiver(batRec);
      stopForeground(false);
      Toast.makeText(this, getString(R.string.toast_stopped), Toast.LENGTH_LONG).show();
  }
}
