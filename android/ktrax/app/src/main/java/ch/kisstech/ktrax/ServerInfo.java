//
// Data received from server
//

package ch.kisstech.ktrax;

public class ServerInfo {
    // Maximum number of targets 
    final static int MAX_TARGETS = 5;

    // GPS time [ms] for which data applies, echo of latest GPS time
    // of client -> server packet.
    // TODO: An original idea was to use that, why is it now gone?
    // public long gpstime = 0;

    // Total number of clients
    public int n_clients = 0;

    // 'Up' direction on the radar map [degree] (usually North,
    // Heading or Track)
    public double map_orientation = 0;

    // Are we moving (yes, that's determined by the server)?
    public short moving = 0;

    // Number of elements in target array < MAX_TARGETS
    public int n_targets = 0;

    // Notification from server
    public int notification = 0;

    public Alarm target[];
    ServerInfo() {
        target = new Alarm[MAX_TARGETS];
        for (int i = 0; i < MAX_TARGETS; ++i) {
            target[i] = new Alarm();
        }
    }
}
