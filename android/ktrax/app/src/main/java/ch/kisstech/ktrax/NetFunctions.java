//
// Network management functions, running in a separate thread.
//
// Since this class runs a separate thread in a loop it became
// the central point of the business logic.
//
// TODO: 
// * UI update immediately after packet has been received?
// * Composite NetworkManagerStatus similar to PositionManagerStatus.
// * ktrax_toast fails due to issues in showToast():
//   Can't create handler inside thread that has not called Looper.prepare()
//
// Dec 27, 2014: Replaced Java Scanner class by splitting the
// string on whitespace and individually parsing the components.
// This results in HUGE memory savings.
// See http://stackoverflow.com/questions/8135903/java-memory-usage-for-scanner
//

package ch.kisstech.ktrax;

import android.util.Log;

import java.net.*;

public class NetFunctions {
    final static String TAG = "Ktrax.NetFunctions";
    final static int RECEIVE_BUFFER_SIZE = 1000;

    public static boolean sendString(DatagramSocket sock, InetAddress address,
                                     int port, String s) {
        byte[] buf = s.getBytes();
        DatagramPacket p = new DatagramPacket(buf, buf.length, address, port);
        // DatagramPacket p = new DatagramPacket(buf, buf.length);
        try {
            sock.send(p);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Couldn't send packet: " + e.getMessage());
            return false;
        }
    }

    // Java optimization hacks throughout...
    static byte[] receive_buffer = new byte[RECEIVE_BUFFER_SIZE];
    static final String EMPTY_STRING = "";
    public static String receiveString(DatagramSocket sock) {
        DatagramPacket p = new DatagramPacket(receive_buffer, 
                                              RECEIVE_BUFFER_SIZE);

        try {
            sock.receive(p);
            return new String(p.getData(), 0, p.getLength(), "ASCII");
        }
        // TODO: The exception here seems to be the most important
        // source of memory consumption after the abandonment
        // of Scanner.  Probably due to the stack trace being
        // converted.  No easy alternative though.
        catch (SocketTimeoutException e) {
            return EMPTY_STRING;
        }
        catch (Exception e) {
            Log.e(TAG, "Couldn't receive packet: " + e.getMessage());
            return "E";
        }
    }

}
