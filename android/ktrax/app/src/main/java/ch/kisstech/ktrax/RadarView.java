//
// Canvas drawing functions have undocumented units, probably pixels?!
//
// AIS symbology: See [1]
// [1] INTERIM GUIDELINES FOR THE PRESENTATION AND DISPLAY OF AIS TARGET 
//     INFORMATION.  International Maritime Organization, Document SN/Circ.217,
//     2001.
//     http://www.imo.org/blast/blastDataHelper.asp?data_id=5391&filename=217.pdf
// [2] AIS - Symbol modifications and CSP changes.  Paper for Consideration 
//     by CSMWG15
//     http://www.iho.int/mtg_docs/com_wg/DIPWG/CSMWG15/CSMWG15-10B_revised_AIS_symbols.pdf
//     RESBL color?  --> light blue?
// [3] GUIDELINES FOR THE PRESENTATION OF NAVIGATION-RELATED SYMBOLS, TERMS AND ABBREVIATIONS.
//     IMO SN/Circ.243, 2004.  Referenced by [2].
//     http://www.iho.int/mtg_docs/industry/ECDIS_workshop_12-3/SN.1-Circ.243%20-%20Guidelines%20For%20The%20Presentation%20Of%20%20Navigation-Related%20Symbols,%20Terms%20And%20Abbreviations%20%28Secretariat%29.pdf
//
// No heading/no COG: [3] defines a circle for 'tracked target', supposedly
// as opposed to 'AIS target'.
// The color not specified (except the mysterious RESBL above); dangerous targets 
// should be RED on color displays and larger.
//
// TODO:
// * Zoom
//

package ch.kisstech.ktrax;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import ch.kisstech.AndroidUtil;

public class RadarView extends View {
    private static final String TAG = "Ktrax.RadarView";

    private static final Paint paint_radar        = new Paint();
    private static final Paint paint_radar_dashed = new Paint();
    private static final Paint paintTarget        = new Paint();
    private static final Paint paintNorth         = new Paint();
    private final static Paint paintText          = new Paint();

    // We keep the paths around in order to save temporaries...
    private static final Path pathTriangle = new Path();
    private static final Path pathNorthIndicator = new Path();

    // Ratio of outer circle wrt. canvas
    private static final float RADAR_RATIO = .9f;

    // Ratio of crosshair wrt. canvas
    // private static final float CROSSHAIR_RATIO = .95f;

    // Ratio of moving target markers
    private static final float TARGET_RATIO = .075f;

    // Ratio of stopped target marker (circle)
    private static final float STOPPED_TARGET_RATIO = .04f;

    // Size increment for dangerous targets
    private static final float DANGEROUS_INCREMENT = 1.5f;

    // Ratio for distance of North from center
    private static final float NORTH_DISTANCE_RATIO = .92f;

    // Ratio for North symbol size
    private static final float NORTH_SYMBOL_RATIO = .1f;

    // x/y of 'N', in 'size' units from center
    private static final float TEXT_X = -1.0f * RADAR_RATIO;

    private static final float NORTH_UP_Y = -0.9f * RADAR_RATIO;
 
    // Text size in 'size' units
    private static final float TEXT_SIZE_RATIO = 0.12f;
    private static final float LINE_SPACE_RATIO = 0.16f;

    // SOS text size in 'size' units
    private static final float TEXT_SIZE_RATIO_SOS = 0.36f;

    // SOS displacement in 'size' units
    private static final float TEXT_SOS_DX = -0.34f;
    private static final float TEXT_SOS_DY =  0.13f;

    // Distance from target centroid to relative altitude, relative
    // to target symbol size.
    private static final float RELATIVE_ALTITUDE_DISTANCE_RATIO = 1.0f;

    // Relative altitude text size, relative to target symbol size.
    private static final float RELATIVE_ALTITUDE_TEXT_SIZE_RATIO = 2.0f;

    // Stroke width ratio
    private final static float STROKE_WIDTH_RATIO = 0.025f;

    // Dash length ratio
    private final static float DASH_RATIO = 0.075f;

    // 3 coordinates for target triangle, units of TARGET_RATIO
    // Zero rotation: pointing UP
    private static final float P1X =  0.0f;
    private static final float P1Y = -1.0f;

    private static final float P2X =  0.5f;
    private static final float P2Y =  0.5f;
    private static final float P3X = -0.5f;
    private static final float P3Y =  0.5f;

    // 3 coordinates for the North triangle
    // Zero rotation: Pointing NORTH
    private static final float N1X =  1.0f;
    private static final float N1Y =  0.0f;
    private static final float N2X =  0.0f;
    private static final float N2Y = -1.0f;
    private static final float N3X = -1.0f;
    private static final float N3Y =  0.0f;

    // Parameters for for onDraw() call.
    // Static because RadarView gets recreated on orientation change.
    private static DisplayInfo displayInfo = new DisplayInfo();

    private static volatile long last_error_display = -1;

    // Errors linger for 10s
    private static final long ERROR_DISPLAY_PERIOD_MS = 10000;

    // Show accuracy if > 20m
    private static final double ACCURACY_SHOW_THRESHOLD = 20.5;


    // Width/height as obtained from onSizeChanged().
    private static float myWidth ;
    private static float myHeight;

    private void init() {
        Log.i(TAG, "RadarView.init()");
        setBackgroundColor(Color.BLACK);
        paint_radar       .setColor(Color.WHITE);
        paint_radar_dashed.setColor(Color.WHITE);
        paintText         .setColor(Color.WHITE);

        paintTarget       .setColor(Color.GREEN );
        // paintTarget       .setStyle(Paint.Style.FILL);

        paintNorth        .setStrokeWidth(0);

        paint_radar       .setStyle(Paint.Style.STROKE);
        paint_radar_dashed.setStyle(Paint.Style.STROKE);
        paintTarget       .setStyle(Paint.Style.STROKE);
        paintNorth        .setStyle(Paint.Style.FILL);
    }

    void setErrorOkColor(Paint paint, boolean ok) {
      paint.setColor(ok ? Color.GREEN : Color.RED);
    }

    // Boilerplate to make Android Studio happy, cf. 
    // http://stackoverflow.com/questions/13797349/java-not-using-the-2-or-3-argument-view-constructors-xml-attributes-will-not
    public RadarView(Context context) {
        super(context);
        init();
    }

    public RadarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    // Returns the target color according to notification level
    void setTargetPaint(final Alarm target) {
        switch (target.notification) {
            case AlertUIManager.NOTIFICATION_ALARM_1:
                paintTarget.setColor(Color.YELLOW);
                break;
            case AlertUIManager.NOTIFICATION_ALARM_2:
                // ORANGE
                paintTarget.setColor(Color.rgb(255, 102, 0));
                break;
            case AlertUIManager.NOTIFICATION_ALARM_3:
            case AlertUIManager.NOTIFICATION_SOS    :
                paintTarget.setColor(Color.RED);
                break;
            default:
                int col = Color.GREEN;
                try {
                    // Log.i(TAG, "Color: " + target.tracking_symbol_color);
                    col = Color.parseColor(target.tracking_symbol_color);
                } catch (IllegalArgumentException e) {}
                // Exception: Parsing failed (e.g., no color given), use GREEN.

                paintTarget.setColor(col);
                break;
        }
    }

    private void drawStoppedTarget(
        final Canvas canvas,
        final Paint paint,
        final float x, final float y, 
        final float size_px) {
        canvas.drawCircle(x, y, size_px, paint);
    }

    // Draws a single target with paintTarget, the given center x, y of
    // the shape and the angle.
    // x, y: position, size_px: target size in pixels
    // TODO: Use Matrix/path.transform()? --> lots of temporaries...
    private void drawTarget(
        final Canvas canvas,
        final Paint paint,
        final float x, final float y, 
        final float relalt,
        final float size_px, final float angle) {

        final float ca = (float)(size_px * Math.cos(angle));
        final float sa = (float)(size_px * Math.sin(angle));

        // We transform by
        // ( ca -sa )
        // ( sa  ca )
        final float p1x = x + ca * P1X - sa * P1Y;
        final float p1y = y + sa * P1X + ca * P1Y;

        final float p2x = x + ca * P2X - sa * P2Y;
        final float p2y = y + sa * P2X + ca * P2Y;

        final float p3x = x + ca * P3X - sa * P3Y;
        final float p3y = y + sa * P3X + ca * P3Y;

        if (Config.DRAW_RELATIVE_ALTITUDE && relalt < 1e10f) {
          final float tx = x + size_px * RELATIVE_ALTITUDE_DISTANCE_RATIO;
          final float ty = y + size_px * 0.5f * RELATIVE_ALTITUDE_TEXT_SIZE_RATIO;
          final float relalt_fl = relalt / 30.48f;
          String text = String.format("%.0f", relalt_fl);
          if (relalt_fl >= 0) {
            text = "+" + text;
          }
          paintText.setTextSize(size_px * RELATIVE_ALTITUDE_TEXT_SIZE_RATIO);
          paintText.setColor(paint.getColor());
          canvas.drawText(text, tx, ty, paintText);
        }

        // pathTriangle.setFillType(Path.FillType.EVEN_ODD);
        pathTriangle.reset();
        pathTriangle.moveTo(p1x, p1y); 
        pathTriangle.lineTo(p2x, p2y);
        pathTriangle.lineTo(p3x, p3y);
        pathTriangle.lineTo(p1x, p1y);
        pathTriangle.close();

        // Log.i(TAG, "Drawing target @ " + p1x + ", " + p1y);
        // Log.i(TAG, "Drawing target @ " + p2x + ", " + p2y);
        // Log.i(TAG, "Drawing target @ " + p3x + ", " + p3y);
        canvas.drawPath(pathTriangle, paint);
    }

    // Size:  Distance from center to nearest screen border, screen
    // coordinates (pixels?)
    private void drawTargets(
        final Canvas canvas, 
        final float map_orientation,
        final float cx, final float cy, final float size) {

        for (int i = 0; i < displayInfo.si.n_targets; ++i) {
            final float alpha = (float) ((displayInfo.si.target[i].bearing - map_orientation) * Math.PI / 180.0f);
            final float ca = (float) Math.cos(alpha);
            final float sa = (float) Math.sin(alpha);
            final float dist_px = (float) displayInfo.si.target[i].distance / 
              Config.RADAR_SCALE * RADAR_RATIO * size;

            final float x = cx + sa * dist_px;
            final float y = cy - ca * dist_px;

            final float cog = (float) displayInfo.si.target[i].display_course_deg;
            final float relalt = (float) displayInfo.si.target[i].relative_altitude;
            setTargetPaint(displayInfo.si.target[i]);

            // Target size increment in case of danger
            final float increment =
                   AlertUIManager.NOTIFICATION_NONE 
                == displayInfo.si.target[i].notification ?
                    1.0f : DANGEROUS_INCREMENT;

            if (cog > 1799.0f) {
                drawStoppedTarget(canvas, paintTarget, x, y,
                        size * STOPPED_TARGET_RATIO * increment);
            } else {
                drawTarget(canvas, paintTarget, x, y,
                        relalt,
                        size * TARGET_RATIO * increment,
                        (float) (Math.PI / 180.0 * (cog - map_orientation)));
            }
        }

        // Draw own ship
        if (0 == displayInfo.si.moving) {
            drawStoppedTarget(canvas, paint_radar, cx, cy, size * STOPPED_TARGET_RATIO);
        } else {
            drawTarget(canvas, paint_radar, cx, cy,
                    1e15f, // no relative altitude here...
                    size * TARGET_RATIO,
                    (float)(Math.PI / 180.0 * (displayInfo.ci.course - map_orientation)));
        }
    }
    
    // Draws a triangle pointing North based on map_orientation
    // cx, cy: Center of radar [px]
    // size:  Radar screen size in pixels (square up to screen boundaries).
    private void drawNorthIndicator(
        final Canvas canvas, 
        final float cx, 
        final float cy,
        final float size,
        final float map_orientation) {
        final float angle = -map_orientation * (float)Math.PI / 180.0f;

        final float radius_px = size * NORTH_DISTANCE_RATIO;
        final float ca = (float)Math.cos(angle);
        final float sa = (float)Math.sin(angle);

        // Reference point [px, screen] of triangle, outside outer circle
        final float x = cx + sa * radius_px;
        final float y = cy - ca * radius_px;

        // Scaled sine/cosine
        final float sca = size * NORTH_SYMBOL_RATIO * ca;
        final float ssa = size * NORTH_SYMBOL_RATIO * sa;

        // Three vertices [px, screen]
        final float n1x = x + sca * N1X - ssa * N1Y;
        final float n1y = y + ssa * N1X + sca * N1Y;

        final float n2x = x + sca * N2X - ssa * N2Y;
        final float n2y = y + ssa * N2X + sca * N2Y;

        final float n3x = x + sca * N3X - ssa * N3Y;
        final float n3y = y + ssa * N3X + sca * N3Y;

        pathNorthIndicator.reset();
        pathNorthIndicator.setFillType(Path.FillType.EVEN_ODD);
        pathNorthIndicator.moveTo(n1x, n1y);
        pathNorthIndicator.lineTo(n2x, n2y);
        pathNorthIndicator.lineTo(n3x, n3y);
        pathNorthIndicator.lineTo(n1x, n1y);
        pathNorthIndicator.close();

        canvas.drawPath(pathNorthIndicator, paintNorth);
    }

    // 0-1ms on Samsung S4 Active, one target
    private void drawRadar(Canvas canvas) {
        // onSizeChanged() widths are are *not* the same as 
        // those from canvas.
        // final float width  = canvas.getWidth ();
        // final float height = canvas.getHeight();
        final float width  = myWidth ;
        final float height = myHeight;

        // Log.i(TAG, "Canvas width = " + width + ", height = " + height);

        final float cx = width  / 2;
        final float cy = height / 2;

        final float size = Math.min(cx, cy);

        final float stroke = size * STROKE_WIDTH_RATIO;
        paint_radar       .setStrokeWidth(stroke);
        paint_radar_dashed.setStrokeWidth(stroke);
        paintTarget       .setStrokeWidth(stroke);

        final float dash = size * DASH_RATIO;
        paint_radar_dashed.setPathEffect(
                new DashPathEffect(new float[]{dash, dash}, 0.0f));

        final boolean low_accuracy = 
            displayInfo.pt.accuracy >= ACCURACY_SHOW_THRESHOLD;
        final boolean status_ok = displayInfo.allOk() && !low_accuracy;

        if (!status_ok) {
            paint_radar.setColor(Color.DKGRAY);
            paint_radar_dashed.setColor(Color.DKGRAY);
            paintNorth.setColor(Color.DKGRAY);
            paintText.setColor(Color.DKGRAY);
        } else {
            paint_radar.setColor(Color.WHITE);
            paint_radar_dashed.setColor(Color.WHITE);
            paintNorth.setColor(Color.WHITE);
            paintText.setColor(Color.WHITE);
        }

        canvas.drawCircle(cx, cy,       RADAR_RATIO * size, paint_radar       );
        canvas.drawCircle(cx, cy, .5f * RADAR_RATIO * size, paint_radar_dashed);

        /*
        canvas.drawLine(cx, cy - CROSSHAIR_RATIO * size, 
                        cx, cy + CROSSHAIR_RATIO * size, 
                        paint_radar);
        canvas.drawLine(cx - CROSSHAIR_RATIO * size, cy,
                        cx + CROSSHAIR_RATIO * size, cy,
                        paint_radar);
        */

        final float text_x = cx + size * TEXT_X;
        final float textsize = size * TEXT_SIZE_RATIO;
        final float linespace = size * LINE_SPACE_RATIO;

        float text_y = cy + size * NORTH_UP_Y;

        // TODO: Avoid round-trip to the server for map orientation?
        // Do heading smoothing on the client side.
        final float map_orientation = 
            (float) displayInfo.si.map_orientation 
          + (Config.USE_TRACK_UP ? 
              0.0f : AndroidUtil.getScreenRotation(getContext()));

        paintText.setTextSize(textsize);

        if (Config.SHOW_TRACKING_STATUS) {
            canvas.drawText(
                displayInfo.trackstat.getStatusString(displayInfo.allOk()), 
                            text_x, text_y, paintText);
            text_y += linespace;
        }

        drawNorthIndicator(canvas, cx, cy, size, map_orientation);

        final long now = AndroidUtil.systemTimeMs();
        if (   !status_ok 
            || TrackingStatus.TEST == displayInfo.trackstat.getStatus()
            || now <= last_error_display + ERROR_DISPLAY_PERIOD_MS) {
            setErrorOkColor(paintText, displayInfo.pmstat.ok());
            canvas.drawText("GPS: " 
                + displayInfo.pmstat.getStatusString(),
                    text_x, text_y, paintText);
            text_y += linespace;
            setErrorOkColor(paintText, displayInfo.netstat.networkOk());
            canvas.drawText("NET: " + displayInfo.netstat.getNetworkStatus(), text_x, text_y, paintText);
            text_y += linespace;
            setErrorOkColor(paintText, displayInfo.netstat.serverOk());
            canvas.drawText("SRV: " + displayInfo.netstat.getServerStatus(), text_x, text_y, paintText);
            text_y += linespace;
            setErrorOkColor(paintText, !low_accuracy);
            canvas.drawText(String.format("GPS accuracy: %.0fm", 
                                          displayInfo.pt.accuracy), text_x, text_y, paintText);
            // text_y += linespace;
            if (!status_ok) {
                last_error_display = now;
            }
        }

        if (displayInfo.ci.sos) {
          paintText.setTextSize(size * TEXT_SIZE_RATIO_SOS);
          paintText.setColor(Color.RED);
          canvas.drawText("SOS", cx + size * TEXT_SOS_DX,
                                 cy + size * TEXT_SOS_DY, paintText);
        }

        drawTargets(canvas, map_orientation, cx, cy, size);
    }


    // Update info and post a redraw.
    public void update(final DisplayInfo info) {
        displayInfo = info;
        // Log.i(TAG, "postInvalidate()");
        postInvalidate();
    }


    @Override
    public void onDraw(Canvas canvas) {
        if (null == displayInfo) {
            Log.i(TAG, "RadarView.onDraw(): No displayInfo");
            return;
        }

        final long begin = AndroidUtil.systemTimeMs();
        drawRadar(canvas);
        final long end   = AndroidUtil.systemTimeMs();
        displayInfo.ci.last_redraw_time = end;
        displayInfo.ci.drawing_time = (int)(end - begin);
        // Log.i(TAG, "Finished drawing, elapsed time: " + (end - begin) + "ms");
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        Log.i(TAG, "New width = " + xNew + ", height = " + yNew);
        myWidth  = xNew;
        myHeight = yNew;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        Log.i(TAG, "onTouchEvent(): " + ev);
        if (   null != displayInfo 
            && !displayInfo.pmstat.gps_enabled
            && MotionEvent.ACTION_DOWN == ev.getActionMasked()) {
            // Bring up 'Location' settings
            AndroidUtil.settingsLocation(getContext());
            return true;
        } else {
            return false;
        }
    }

}
