//
// Client info and statistics.
//

package ch.kisstech.ktrax;

public class ClientInfo {
    // Number of packets received.
    public int packets_received = 0;

    // Packet loss, based on sequence_number and packets_received.
    public float packet_loss_percent = 0.0f;

    // Round-trip latency [ms]
    public int rtt_latency = 0;

    // Last systime the radar was redrawn
    public long last_redraw_time = -1000000000;

    // SOS/emergency status?
    public boolean sos = false;

    // Drawing time for radar [ms]
    public int drawing_time = 0;

    // Current network type (GSM, 3G, 4G, etc)
    public short network_type = -1;

    // Cell ID, -1 if unknown
    public int cell_id = -1;

    // Are we currently roaming?
    public short roaming = 0;

    // Battery level [percent]
    public float battery_level_percent = 100.0f;

    // Device heading [degree], computed using magnetometer and
    // accelerometers.  AZIMUT_NONE means the respective sensors
    // are not present.
    public float heading = OrientationManager.AZIMUT_NONE;

    // Device course over ground computed from GPS.
    public float course = OrientationManager.AZIMUT_NONE;

    // Audio notification, merges server-side notification
    // and client-side (e.g. GPS acquisition/loss, errors, ...).
    // Server notifications have precedence.
    public int notification = AlertUIManager.NOTIFICATION_NONE;
}
