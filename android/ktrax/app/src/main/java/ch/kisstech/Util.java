//
// Various utility functions
//

package ch.kisstech;

import java.io.UnsupportedEncodingException;

public class Util {
    private final static String EMPTY_STRING_REPLACEMENT = "-";

    private final static String TAG = "KISSTech.Util";

    public static String removeBlanks(final String s) {
        return s.replace(" ", "");
    }

    public static String URLEncode(String s) {
        try {
            return java.net.URLEncoder.encode(s, java.nio.charset.StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    // Remove everything except alphanumeric and characters found in
    // 'extra' from s, returning the resulting string.
    // See the corresponding function cpl::util::verify_alnum() .
    public static String keepAlnum(final String s, final String extra) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            final char c = s.charAt(i);
            if (Character.isDigit(c)) {
                sb.append(c);
            } else if ('a' <= c && c <= 'z') {
                sb.append(c);
            } else if ('A' <= c && c <= 'Z') {
                sb.append(c);
            } else if (extra.indexOf(c) >= 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String avoidEmpty(final String s) {
        if ("".equals(s)) {
            return EMPTY_STRING_REPLACEMENT;
        } else {
            return s;
        }
    }

    // Returns a string without blanks and that isn't empty
    public static String sanitizeBlanksAndEmpty(final String s) {
        return avoidEmpty(removeBlanks(s));
    }

    // Returns a string containing just alpha-numeric characters
    // or those found in extra and that isn't empty
    public static String sanitizeAlnumAndEmpty(
        final String s, final String extra) {
        return avoidEmpty(keepAlnum(s, extra));
    }

    public static boolean isTrivialString(final String s) {
        return "".equals(s) || "-".equals(s);
    }

    public static String firstIfNonTrivial(final String s1, final String s2) {
        return !isTrivialString(s1) ? s1 : s2;
    }
}
