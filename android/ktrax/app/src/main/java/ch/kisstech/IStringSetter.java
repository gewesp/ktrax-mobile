package ch.kisstech;

// Interface to set, well, a String
public interface IStringSetter {
    void set(final String s);
}
