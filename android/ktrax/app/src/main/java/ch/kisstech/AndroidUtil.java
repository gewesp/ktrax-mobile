//
// Various Android-specific utility functions
//

package ch.kisstech;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.InputType;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.EditText;



public class AndroidUtil {

    private final static String TAG = "Ktrax.AndroidUtil";

    // http://developer.android.com/reference/android/os/SystemClock.html#elapsedRealtime()
    // System time in since startup [ms], monotonic.
    // Hopefully compatible with getElapsedRealtimeNanos() 
    // from android.Location (API 17+, not used as of RowingCAS 0.92.2).
    public static long systemTimeMs() {
        return SystemClock.elapsedRealtime();
    }

    // http://developer.android.com/reference/android/view/Display.html#getRotation()
    public static float getScreenRotation(Context ctx) {
        final int rotation = (
          (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE))
              .getDefaultDisplay()
              .getRotation();
        switch (rotation) {
            case Surface.ROTATION_90 : return  90.0f;
            case Surface.ROTATION_180: return 180.0f;
            case Surface.ROTATION_270: return 270.0f;
            case Surface.ROTATION_0  : return   0.0f;
            default: return 0.0f;
        }
    }

    // Bring up the Settings -> Location menu
    public static void settingsLocation(Context ctx) {
        Log.i(TAG, "Bringing up Location settings");
        ctx.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    // Shares a subject (e.g. for email) and text.
    public static void shareText(
        final Context ctx, final String subject, final String text) {
        final Intent i = new Intent(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT   , text   );
        i.setType("text/plain");
        ctx.startActivity(i);
    }

    // Shows a text edit dialog with OK/Cancel.  Invokes setter if
    // text has been entered with OK.
    // http://stackoverflow.com/questions/10903754/input-text-dialog-android
    public static void showTextEditDialog(
        final Context ctx, 
        final IStringSetter setter,
        final String title,
        final String message,
        final String current,
        final String hint) {

        final EditText et = new EditText(ctx);
        et.setHint("e.g. " + hint);
        et.setInputType(
                InputType.TYPE_CLASS_TEXT 
              | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
              | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        et.setText(Util.isTrivialString(current) ? "" : current);

        final AlertDialog dlg = new AlertDialog.Builder(ctx)
          .setTitle  (title)
          .setMessage(message)
          .setView   (et)
          .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
              String result = et.getText().toString();
              Log.i(TAG, "Text entered: " + result);
              setter.set(result);
            }
          })
          .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
          })
          .create(); 

        // This probably works only for devices without a hardware keyboard.
        // Automatically bring up the soft keyboard once the dialog is created.
        // http://stackoverflow.com/questions/2403632/android-show-soft-keyboard-automatically-when-focus-is-on-an-edittext/8018630#8018630
        dlg.getWindow().setSoftInputMode (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dlg.show();

    }

    public static void openUrl(Context ctx, String url) {
        Log.i(TAG, "Opening URL: " + url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        // This is required to launch a browser from a context that is not an activity.
        // Go figure...
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
    }

    // Returns a (hopefully unique) ID for the current cell.
    public static int getCellId(TelephonyManager telMan) {
        if (null == telMan) {
            return -1;
        }

        CellLocation loc = telMan.getCellLocation();

        if (null == loc) {
            return -1;
        }

        if (loc instanceof CdmaCellLocation) {
            return ((CdmaCellLocation)loc).getBaseStationId();
        }

        if (loc instanceof GsmCellLocation) {
            return ((GsmCellLocation)loc).getCid();
        }

        return -1;
    }
}
