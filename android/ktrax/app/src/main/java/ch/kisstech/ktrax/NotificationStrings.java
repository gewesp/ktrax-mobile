//
// English UI strings
//
// These do not contain text, just a maximum of a few words
//

package ch.kisstech.ktrax;

public class NotificationStrings {
    // Generic
    final static public String STRING_OK      = "OK";
    final static public String STRING_INIT    = "INIT";
    final static public String STRING_STOP    = "STOP";
    final static public String STRING_TEST    = "TEST";
    final static public String STRING_ACTIVE  = "ACTIVE";
    final static public String STRING_TRACKING = "TRACKING";
    final static public String STRING_UNKNOWN = "UNKNOWN";
    final static public String STRING_DENIED  = "DENIED";
    // final static public String STRING_ERROR   = "ERROR";

    // GPS specific
    final static public String GPS_DISABLED  = "Please touch to enable!";
    final static public String GPS_3D        = "3D";
    final static public String GPS_ACQUIRING = "ACQUIRING";
    final static public String GPS_ERROR     = "ERROR";

      // Network specific
    final static public String ERROR_ADDRESS    = "ERROR (address)";
    final static public String ERROR_INIT       = "ERROR (initialize)";
    final static public String ERROR_SEND       = "ERROR (send)";
    final static public String ERROR_CONNECTION = "ERROR (connection)";
    final static public String ERROR_TIMEOUT    = "ERROR (timeout)";
    final static public String ERROR_PARSE      = "ERROR (parse)";
}
