package ch.kisstech.ktrax;

public class TrackingStatus {
    // Initializing...
    final public static int INIT   = 1;

    // Moving and tracking normally
    final public static int NORMAL = 2;

    // Test mode
    final public static int TEST   = 3;

    private int status = INIT;

    public void setStatus(final int s) { status = s; }

    public int getStatus() { return status; }

    // all_ok from displayInfo
    public String getStatusString(final boolean all_ok) {
        switch (status) {
          case INIT:
            return NotificationStrings.STRING_INIT;
          case NORMAL:
            return all_ok ? NotificationStrings.STRING_TRACKING :
                            NotificationStrings.STRING_ACTIVE;
          case TEST:
            return NotificationStrings.STRING_TEST;
          default:
            return NotificationStrings.STRING_UNKNOWN;
        }
    }
}
