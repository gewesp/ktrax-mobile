//
// Network status
//

package ch.kisstech.ktrax;

public class NetworkManagerStatus {
    // 'Network' errors
    private boolean error_initialize;
    private boolean error_address   ;
    private boolean error_send      ;

    // 'Server' errors
    private boolean error_connection;
    private boolean error_timeout   ;
    private boolean error_parse     ;

    NetworkManagerStatus() { setAllOk(); }

    void setInitializationError() { error_initialize = true; }
    void setAddressError       () { error_address    = true; }
    void setSendError          () { error_send       = true; }

    void setConnectionError    () { error_connection = true; }
    void setTimeoutError       () { error_timeout    = true; }
    void setParseError         () { error_parse      = true; }


    void setAllOk() {
        error_initialize = false;
        error_address    = false;
        error_send       = false;

        error_connection = false;
        error_timeout    = false;
        error_parse      = false;
    }

    boolean networkOk() {
        return !(error_initialize || error_address || error_send);
    }

    boolean serverOk() {
        return !(error_connection || error_timeout || error_parse);
    }

    boolean ok() { return networkOk() && serverOk(); }

    String getNetworkStatus() { 
        if (error_initialize) {
            return NotificationStrings.ERROR_INIT;
        } 
        if (error_address   ) {
            return NotificationStrings.ERROR_ADDRESS;
        } 
        if (error_send      ) {
            return NotificationStrings.ERROR_SEND;
        } 
        return NotificationStrings.STRING_OK;
    }

    String getServerStatus() {
        if (error_connection) {
            return NotificationStrings.ERROR_CONNECTION;
        }
        if (error_timeout) {
            return NotificationStrings.ERROR_TIMEOUT;
        }
        if (error_parse) {
            return NotificationStrings.ERROR_PARSE;
        }
        return NotificationStrings.STRING_OK;
    }
}
