//
// Alarm structure.
// See server alarm.h
//

package ch.kisstech.ktrax;

public class Alarm {
    // Time of validity for this alarm, [ms] since epoch
    public long gpstime_ms;

    // ID of target
    String target;

    // Vehicle type of target
    int vehicle_type;

    // User-selected tracking symbol color of target, Hexadecimal #rrggbb
    // or "-" for default.
    public String tracking_symbol_color = "-";

    // Danger level
    public double danger_level;

    // Notification/alarm level
    public int notification;

    // Distance [m]
    public double distance;

    // Absolute bearing [degrees] wrt. N
    public double bearing;

    // Relative altitude [m], positive: other is higher
    public double relative_altitude;

    // Target speed
    public double speed;

    // Target course over ground [degree], for display purposes
    public double display_course_deg;

    // Target vertical speed [m/s]
    public double vertical_speed;

    // Target heading, if available
    public double heading;

    // Time to CPA, relative to gpstime_ms [s]
    public double tCPA;

    // Distance at CPA [m]
    public double dCPA;
}
