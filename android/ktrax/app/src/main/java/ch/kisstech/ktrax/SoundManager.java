//
// Interfaces with the Android system for playing sounds.
// Runs a thread constantly feeding the sound pipeline.
// 
// TODO: -> Independent from Ktrax, move to AndroidSoundManager
//
// Tutorial for sound:
// https://audioprograming.wordpress.com/2012/10/18/a-simple-synth-in-android-step-by-step-guide-using-the-java-sdk/
//

package ch.kisstech.ktrax;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import android.util.Log;


public class SoundManager extends Thread {

    private final String TAG = "Ktrax.SoundManager";

    // Music, ringtones, alarms, you name it...
    public final static int MY_STREAM = AudioManager.STREAM_MUSIC;

    // Android object for managing audio replay.
    private AudioTrack track;

    // Sample rate [Hz]
    private int sample_rate;

    // Samples to play in a loop with given gain.
    // *Probably* volatile is sufficient here:
    // "any write to a volatile variable establishes a happens-before relationship with
    // subsequent reads of that same variable."
    //
    // https://docs.oracle.com/javase/tutorial/essential/concurrency/atomic.html

    private volatile short samples[];
    private volatile Float gain     ;

    SoundManager() {
        sample_rate = AudioTrack.getNativeOutputSampleRate(MY_STREAM);
        // Number of samples in buffer for 1/8s, *CAUTION*, argument
        // to AudioTrack constructor is in bytes!!!
        final int bufsize_samples = sample_rate / 8;
        track = new AudioTrack(MY_STREAM,
                sample_rate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, 2 * bufsize_samples,
                AudioTrack.MODE_STREAM);
        Log.i(TAG, "Starting up, sample rate = " + sample_rate +
                   ", buffer size = " + bufsize_samples);

        setGain(1);

        // Execute run() in a new thread
        this.start();
    }

    public void shutdown() {
        Log.i(TAG, "Shutting down");

        this.interrupt();

        try {
            this.join();
        } catch (Exception e) {
            Log.e(TAG, "Could not stop audio server: " + e.getMessage());
        }
    }

    // Get samples and remove them from the single-element queue.
    synchronized private short[] consumeSamples() {
        short[] ret = samples;
        samples = null;
        return ret;
    }

    public void setGain(float newgain) {
        gain = newgain;
    }

    /*
    private int repeat_count = 0;
    synchronized void setRepeatCount(final int i) {
        repeat_count = i;
    }

    synchronized int getAndDecreaseRepeatCount() {
        return repeat_count--;
    }
    */

    synchronized public void setSamples(final short[] newsamples) {
        samples = newsamples;
    }

    private int silence_duration = 250;

    public void setSilence(float duration) {
        silence_duration = (int)(duration * 1000);
        samples = null;
    }

    private float getGain() {
        return gain;
    }

    public double getSampleRate() {
        return (double) sample_rate;
    }


    // Thread API implementation
    public void run() {
        try {
            // track.play();
            Log.i(TAG, "Audio server up and running");

            while (true) {
                final short[] mysamples = consumeSamples();
                if (null != mysamples) {
                    // Log.d(TAG, "Playing samples...");
                    final float mygain = AudioTrack.getMaxVolume() * getGain();

                    track.setStereoVolume(mygain, mygain);
                    track.play();
                    track.write(mysamples, 0, mysamples.length);
                } else {
                    track.setStereoVolume(0.0f, 0.0f);
                    track.stop();
                    // Log.d(TAG, "Sleeping...");
                    Thread.sleep(silence_duration);
                }
                
                // Give environment a chance to stop us.
                if (isInterrupted()) {
                    Log.i(TAG, "Audio server stopped, terminating thread");
                    return;
                }            
            }
        } catch (Exception e) {
            Log.e(TAG, "Audio server exiting: " + e.getMessage());
        }

    }

}
