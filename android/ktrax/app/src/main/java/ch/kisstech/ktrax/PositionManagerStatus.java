package ch.kisstech.ktrax;

public class PositionManagerStatus {
    // Enabled by user (Settings -> Location)?
    public boolean gps_enabled = false;

    // Fine location updates allowed for app (based on per-app settings)?
    public boolean updates_allowed = false;

    // Status of the last received position
    public boolean position_valid = false;

    public boolean ok() {
      return gps_enabled && updates_allowed && gps_enabled && position_valid;
    }

    public void setEnabled(final boolean e) { gps_enabled = e; }

    // Should be called once per second, before the string is used.
    public void updateStatus(final boolean new_position_valid) {
        position_valid = new_position_valid;
    }

    public String getStatusString() {
        if (!updates_allowed) {
            return NotificationStrings.STRING_DENIED;
        }
        if (!gps_enabled) {
            return NotificationStrings.GPS_DISABLED;
        }
        if (position_valid) {
            return NotificationStrings.GPS_3D;
        } else {
            return NotificationStrings.GPS_ACQUIRING;
        }
    }
}
