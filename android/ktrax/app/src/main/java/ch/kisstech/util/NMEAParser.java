//
// TODO:
// FIX NMEA parsing of $GPRMC,145350.0,A,4723.331560,N,00831.310966,E,1.0,,241214,0.0,E,A*2D
//
// TODO: DO NOT USE Java Scanner class.
// See http://stackoverflow.com/questions/8135903/java-memory-usage-for-scanner
//
// TODO: Currently not functional.  Need to generalize PositionTime first.
//

/*

public class NMEAParser {

    // final private static String TAG = "kisstech.NMEAParser";

    public static double NmeaLatLonToDecimal(@NonNull double l, @NonNull String hemi) {

        final double minutes = l % 100.0;
        final double degrees = (l - minutes) / 100.0 + minutes / 60.0;
        if (hemi.equals("E") || hemi.equals("N")) {
            return  degrees;
        } else {
            return -degrees;
        }

    }

    public static @NonNull PositionTime ParseGPRMC(@NonNull String nmea) {
        // Log.i(TAG, "NMEA: " + nmea);

        PositionTime new_pt = new PositionTime();

        Scanner sc = new Scanner(nmea);
        sc.useDelimiter(",");
        // $GPRMC
        sc.next();
        // HHMMSS
        sc.next();

        String flag = sc.next();
        if (flag.equals("A")) {
            final double lat_nmea = sc.nextDouble();
            final String lat_hemi = sc.next();
            final double lon_nmea = sc.nextDouble();
            final String lon_hemi = sc.next();

            new_pt.speed = sc.nextDouble() * 0.5144444444444445; // knot -> m/s
            new_pt.course = sc.nextDouble();

            new_pt.latitude = NMEA.NmeaLatLonToDecimal(lat_nmea, lat_hemi);
            new_pt.longitude = NMEA.NmeaLatLonToDecimal(lon_nmea, lon_hemi);
        }

        return new_pt;
    }

    public static void SelfTest() throws Exception {
        final String nmea = "$GPRMC,161827.0,A,4723.330527,N,00831.304709,E,0.0,40.2,261114,0.0,E,A*38";

        final PositionTime pt = ParseGPRMC(nmea);

        if (Math.abs(47.38884 - pt.latitude) > 1e-2
            || Math.abs(8.5217 - pt.longitude) > 1e-3
            || pt.speed > .00001
            || Math.abs(pt.course - 40.2) > 1e-10 ) {
            throw new Exception("NMEA.SelfTest failed");
        }

    }


}
*/
