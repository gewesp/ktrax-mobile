//
// Network management functions, running in a separate thread.
//
// Since this class runs a separate thread in a loop it became
// the central point of the business logic.
//
// TODO: 
// * UI update immediately after packet has been received?
// * Composite NetworkManagerStatus similar to PositionManagerStatus.
// * ktrax_toast fails due to issues in showToast():
//   Can't create handler inside thread that has not called Looper.prepare()
// * Motion detection: Sensors may be switched off during standby, e.g.
//   the acceleration sensor:
//     http://www.saltwebsites.com/2012/android-accelerometers-screen-off
//
// Dec 27, 2014: Replaced Java Scanner class by splitting the
// string on whitespace and individually parsing the components.
// This results in HUGE memory savings.
// See http://stackoverflow.com/questions/8135903/java-memory-usage-for-scanner
//

package ch.kisstech.ktrax;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.LocationManager;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.telephony.TelephonyManager;
import android.util.Log;

import ch.kisstech.AndroidUtil;
import ch.kisstech.Util;

import java.net.*;
import java.util.regex.Pattern;

// import static junit.framework.Assert.assertNotNull;
// import static junit.framework.Assert.assertTrue;


public class NetworkManager extends Thread {
    // Number of fields in header and per target in update packet
    private final static int HEADER_FIELDS = 7;
    private final static int FIELDS_PER_TARGET_VERSION_1 = 15;

    private final NetworkManagerStatus status;
    private final TrackingStatus trackstat;

    final static private String TAG = "Ktrax.NetworkManager";
    final static private String SERVER = "ktrax.kisstech.ch";

    // Prefix for ICAO IDs
    // TODO: Factor out to separate class
    final static private String ID_PREFIX_ICAO = "icao:";
    final static private String ID_PREFIX_FLARM = "flarm:";

    // String identifying packet types
    final static private String UPDATE_PACKET = "ktrax_update";
    final static private String UPDATE2_PACKET = "ktrax_update2";
    final static private String POSITION_PACKET = "ktrax_pos";
    final static private String HELLO_PACKET = "ktrax_hello";
    final static private String INFO2_PACKET = "ktrax_info2";
    final static private String BYE_PACKET = "ktrax_bye";
    final static private String TOAST_PACKET = "ktrax_toast";
    final static private String URL_PACKET = "ktrax_url";

    private final static Pattern whitespace_pattern = Pattern.compile("\\s+");

    // Retry timeout for DNS lookup [ms]
    final static private int RESOLVE_RETRY_TIME = 1000;

    // Last time we tried to resolve...
    private long last_resolve_time = -10000000;

    // Callbacks for UI update
    private volatile IPage parent;

    // Android System managers
    // For operator name etc.
    @NonNull
    private final TelephonyManager telMan;

    // The Android Activity passed in from the constructor
    @NonNull 
    private final Activity my_activity;

    // Our own managers
    // PositionManager, calls our receiveGpsUpdate().
    private volatile PositionManager position_manager;

    // Orientation
    private volatile OrientationManager orMan;

    // Sounds
    private final AlertUIManager alert_manager;

    private DatagramSocket sock;
    private InetAddress address;

    // (Hopefully) unique ID of this device/installation.
    private volatile String android_id;

    private DisplayInfo displayInfo = new DisplayInfo();

    // volatile probably not necessary for class types,
    // but better be safe than sorry.
    // Info received from server
    private volatile ServerInfo server_info = new ServerInfo();

    // Info collected on client
    private final ClientInfo client_info = new ClientInfo();

    // Are we in foreground (i.e., updating the Radar screen)?
    private boolean foreground = true;

    // The current location *and its systime [ms]*
    private volatile android.location.Location location;
    private volatile long location_systime = -1000000000;

    // Last p/t sent
    private volatile PositionTime last_send_pt;

    // Vertical speed [m/s], not provided by Android
    private double vertical_speed;

    // Turn rate [degree/s], not provided by Android
    private double turn_rate;

    // Whether or not we have a valid position (or location update timeout).
    private boolean position_valid;

    // The current packet send rate
    private final int UPDATE_RATE = Config.UPDATE_RATE_NORMAL - 1;

    // System time of send, for latency calculation and send triggering.
    private long last_send_systime;

    // Send hello/info between last_hello_systime and 
    // last_hello_systime + Config.HELLO_REPEAT_INTERVAL
    private long last_hello_systime;

    // System time of last call to updateUI();
    private long last_update_systime;

    // System time of last *update* packet received
    private long last_receive_systime;

    // Last cell ID, used to compare with current one for motion detection
    private int last_cell_id;

    // First and current sequence number for POSITION packets.
    private long first_sequence_number;
    private long current_sequence_number;

    // Last time a test was requested.
    private long last_test_systime;

    // Constructor called here
    private long startup_systime;

    public static PersistentLogic getGlobals() {
        return PersistentLogic.getInstance();
    }

    public NetworkManager(@NonNull IPage parent_in,
                          @NonNull Activity activity,
                          @NonNull SensorManager sensMan,
                          @NonNull LocationManager lm,
                          @NonNull TelephonyManager tm,
                          @NonNull String uid) {
        status = new NetworkManagerStatus();
        trackstat = new TrackingStatus();
        telMan = tm;
        try {
            server_info = new ServerInfo();
            last_send_pt = new PositionTime();
            position_valid = false;
            last_send_systime = -100000000;
            last_update_systime = -100000000;
            // Pretend everything is OK and try GPS acquisition on startup
            startup_systime = last_receive_systime
                    = last_hello_systime
                    = AndroidUtil.systemTimeMs();
            last_test_systime = -100000000;
            last_cell_id = -1;
            // TODO: Start with a sequence number derived from
            // time---check server implications (STANDSTILL etc.)
            // first_sequence_number = AndroidUtil.systemTimeMs() / 1000;
            // current_sequence_number = first_sequence_number;
            first_sequence_number = 0;
            current_sequence_number = 0;

            setParent(parent_in);

            android_id = uid;

            sock = new DatagramSocket();
            // Just a millisecond.  We'll sleep in the send/update/receive
            // loop.
            sock.setSoTimeout(1);
            Log.i(TAG, "Local socket address: " + sock.getLocalSocketAddress());
            Log.i(TAG, "Local address: " + sock.getLocalAddress());
            Log.i(TAG, "Local port: " + sock.getLocalPort());
            Log.i(TAG, "Sequence number: " + current_sequence_number);

            status.setAllOk();

        } catch (Exception e) {
            Log.e(TAG, "Couldn't create socket: " + e.getMessage());
            status.setInitializationError();
            // sock = null;
        }
        alert_manager = new AlertUIManager();
        my_activity = activity;
        position_manager = new PositionManager(activity, lm, this);
        orMan = new OrientationManager(sensMan);
        this.start();
    }

    public void shutdown() {
        // Notice inverse sequence of setup!!
        Log.i(TAG, "Shutting down");

        // Shut down main loop thread
        this.interrupt();
        try {
            this.join();
        } catch (Exception e) {
            Log.e(TAG, "Could not stop loop: " + e.getMessage());
        }

        // Then shut down dependents
        orMan.shutdown();
        position_manager.shutdown();
        alert_manager.shutdown();

        // Finally, close the socket
        sock.close();
    }

    public @NonNull
    String getUniqueId() {
        final String n3 = getGlobals().getName3();
        if (!Util.isTrivialString(n3)) {
            if (6 == n3.length()) {
                if ('D' == n3.charAt(0)
                        && ('D' == n3.charAt(1) || 'E' == n3.charAt(1) || 'F' == n3.charAt(1))) {
                    return ID_PREFIX_FLARM + n3;
                } else {
                    return ID_PREFIX_ICAO + n3;
                }
            }
            Log.i(TAG, "Wrong size of FLARM/ICAO ID");
        }
        return android_id;
    }

    public @NonNull
    String getAndroidID() {
        return android_id;
    }

    public @NonNull
    PositionManager getPositionManager() {
        return position_manager;
    }

    private @NonNull
    PositionTime getPositionTime() {
        if (null != location) {
            return new PositionTime(location, location_systime);
        } else {
            return new PositionTime();
        }
    }

    // Update position/time, timestamp with systime, last_movement_systime
    public void receiveGpsUpdate(@NonNull android.location.Location loc,
                                 final double vertical_speed_in,
                                 final double turn_rate_in) {
        vertical_speed = vertical_speed_in;
        turn_rate = turn_rate_in;
        // Log.i(TAG, "Altitude: " + loc.getAltitude());
        // Log.i(TAG, "Vertical speed: " + vertical_speed);
        // Log.i(TAG, "Turn rate: " + turn_rate);
        location = loc;
        location_systime = AndroidUtil.systemTimeMs();
     }

    public void setParent(@NonNull IPage parent_in) {
        parent = parent_in;
    }

    public void sendTestPacket() {
        Log.i(TAG, "Test button pressed");
        last_test_systime = AndroidUtil.systemTimeMs();
    }

    public void toggleSOS() {
        client_info.sos = !client_info.sos;
        Log.i(TAG, "SOS button pressed, SOS state: " + client_info.sos);
    }

    // TODO: Resolve can take long...
    // http://stackoverflow.com/questions/18217335/can-i-set-the-getaddrinfo-timeout-in-android-for-defaulthttpclient
    private void resolve() {

        try {
            Log.i(TAG, "Resolving: " + SERVER + "...");
            address = InetAddress.getByName(SERVER);

            Log.i(TAG, "Server address: " + address.toString());
            Log.i(TAG, "Server port: " + Config.UDP_PORT);
            status.setAllOk();
        } catch (Exception e) {
            Log.e(TAG, "Couldn't resolve " + SERVER + ": "
                    + e.getClass().getName());
            status.setAddressError();
        }

    }

    // Should be called @ ~1Hz (min 0.9Hz?)
    // Prepare all structures and call activity's and sound manager's UI 
    // update.
    private void updateUI(long now_systime) {
        // Log.d(TAG, "UI update");
        last_update_systime = now_systime;
        updateGpsStatus(now_systime);
        logAllOk();

        client_info.notification = server_info.notification;
        if (AlertUIManager.NOTIFICATION_NONE == client_info.notification) {
            if (client_info.sos) {
                client_info.notification = AlertUIManager.NOTIFICATION_SOS;
                Log.i(TAG, "Client-initiated audio notification: SOS/sound id: "
                        + client_info.notification);
            }
        }

        if (AlertUIManager.NOTIFICATION_NONE == client_info.notification) {
            final long runtime = now_systime - startup_systime;
            final long t = (runtime + Config.AUDIO_STATUS_PERIOD_MS - Config.AUDIO_STATUS_FIRST_MS)
                    % Config.AUDIO_STATUS_PERIOD_MS;

            // Issue a very low volume 'dit' sound if all is OK, or Beethoven if not.
            if (t < Config.AUDIO_STATUS_DURATION_MS) {
                client_info.notification = displayInfo.allOk() ?
                        AlertUIManager.NOTIFICATION_1_DIT
                        : AlertUIManager.NOTIFICATION_ERROR;
                Log.i(TAG, "Client-initiated audio notification: STATUS/sound id: "
                        + client_info.notification);
            }
        }

        displayInfo.pt = getPositionTime();
        displayInfo.si = server_info;
        displayInfo.ci = client_info;
        displayInfo.pmstat = position_manager.getStatus();
        displayInfo.netstat = this.status;
        displayInfo.trackstat = this.trackstat;

        // Parent may also write back into displayInfo, e.g. the 
        // ci.drawing_time for statistics.
        parent.update(displayInfo);

        alert_manager.SetNotification(client_info.notification);
    }

    private void postUIUpdate() {
        last_update_systime = -100000000;
    }

    private void postHelloAndInfoPacket(final long now_systime) {
        last_hello_systime = now_systime;
    }

    public void postHelloAndInfoPacket() {
        postHelloAndInfoPacket(AndroidUtil.systemTimeMs());
    }

    private long getLoopInterval() {
        return foreground ?
                Config.LOOP_INTERVAL_FOREGROUND : Config.LOOP_INTERVAL_BACKGROUND;
    }

    // Wait sufficiently long to make sure a sound has started playing
    private void letPlay() {
        final int duration =
                (int) (1000 * 1.3 * AlertUIManager.SILENCE_DURATION);
        try {
            Thread.sleep(duration);
        } catch (InterruptedException ee) {
            Log.i(TAG, "letPlay() was interrupted");
        }
    }

    @Override
    // No checked exceptions thrown, NullPointer,
    // arithmetic etc. still possible.
    public void run() {
        // The position manager requires a looper, so
        // we use the main thread for location update callbacks
        getGlobals().postStartGpsUpdates();
        Log.i(TAG, "Main loop starting, playing ON sound");
        alert_manager.SetNotification(AlertUIManager.NOTIFICATION_OK);
        letPlay();

        // Pretend we just had a redraw to force foreground mode on startup.
        client_info.last_redraw_time = AndroidUtil.systemTimeMs();
        // Make sure we send hello/info on startup
        postHelloAndInfoPacket();

        while (true) {
            {
                // Detect movement from orientation manager
                // TODO: See top of file.  Many phones switch off the
                // acceleration sensor in standby.
                orMan.detectOrientationChange();

                last_cell_id = -1;
                try {
                  last_cell_id = AndroidUtil.getCellId(telMan);
                } catch (Exception e) {
                  // Most likely, a java.lang.SecurityException due to access of Cell ID
                  // without permission.
                  Log.e(TAG, "Couldn't read cell tower id: " + e.getMessage());
                }
            }

            setTrackingStatus();

            if (null == address) {
                final long now_systime = AndroidUtil.systemTimeMs();

                if (last_resolve_time + RESOLVE_RETRY_TIME <= now_systime) {
                    last_resolve_time = now_systime;
                    resolve();
                }

                postHelloAndInfoPacket(now_systime);
            }

            if (null != address) {
                send();
            } else {
                status.setConnectionError();
            }

            final long now_systime = AndroidUtil.systemTimeMs();

            if (now_systime >= last_hello_systime + Config.HELLO_SEND_INTERVAL) {
                postHelloAndInfoPacket(now_systime);
            }

            final long hello_period =
                    getLoopInterval() * Config.N_HELLO_REPEATS;

            // Log.d("TAG", "hello period: " + hello_period);

            if (null != address
                    && last_hello_systime <= now_systime
                    && now_systime <= last_hello_systime + hello_period) {
                send_hello_and_info(now_systime);
            }

            if (null != address) {
                receive(now_systime);

                // Receive posts a UI update and clears errors
                // on successful reception of an update packet.
                // Here we handle timeout errors where nothing comes back at all.
                // TODO: Cleanly handle timeouts!!!!
                // Probably the best way is to keep track of k recently
                // sent packets in a queue.
                if (now_systime > last_receive_systime + UPDATE_RATE
                        + Config.RECEIVE_TIMEOUT_RTT_MAX) {
                    status.setTimeoutError();
                }
            }

            // UI is updated on received packets (via postUIUpdate()) and here
            if (now_systime >= last_update_systime + Config.UPDATE_UI_INTERVAL) {
                updateUI(now_systime);
            }

            try {
                Thread.sleep(getLoopInterval());
            } catch (InterruptedException e) {
                Log.i(TAG, "Main loop exiting, playing OFF sound");
                alert_manager.SetNotification(AlertUIManager.NOTIFICATION_OFF);
                if (null != address) {
                    try {
                        send_bye();
                    } catch (Exception ee) {
                        Log.e(TAG, "Failed to send bye packet: ", ee);
                    }
                }
                letPlay();
                return;
            }
        }
    }

    // Should be called before UI update, once per second.
    private void updateGpsStatus(long now_systime) {
        position_valid = now_systime <= location_systime + Config.GPS_TIMEOUT;
        position_manager.updateStatus(position_valid);
    }

    // Log a message if status is NOT OK
    private void logAllOk() {
        if (!displayInfo.allOk()) {
            Log.i(TAG, "Status: GPS: "
                    + displayInfo.pmstat.getStatusString()
                    + ", NET: "
                    + displayInfo.netstat.getNetworkStatus()
                    + ", SRV: "
                    + displayInfo.netstat.getServerStatus());
        }
    }

    // Get client info parameters as far as they are system params
    private void updateClientInfo(PositionTime pt) {
        // As of Oct 2021, this requires the READ_PHONE_STATE
        // permission, which is qualified as 'dangerous'.  Let's hope
        // that Alphabet won't hold this against us.
        // https://developer.android.com/reference/android/Manifest.permission#READ_PHONE_STATE


        // This is auto-fix code by Android Studio.
        if (ActivityCompat.checkSelfPermission(
              my_activity.getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            client_info.network_type = -1;
        } else {
            client_info.network_type = (short) telMan.getNetworkType();
        }
        client_info.cell_id      = last_cell_id;
        client_info.roaming      = (short) (telMan.isNetworkRoaming() ? 1 : 0);

        client_info.battery_level_percent = 
            getGlobals().battery_level_percent;

        client_info.heading = orMan.computeAzimut();
        client_info.course  = (float) pt.course;
        // Log.d(TAG, "Heading: " + client_info.heading);
    }

    private int vehicleTypeAndFlags() {
        return    getGlobals().getVehicleType()
               | (getGlobals().getTrackingEnabled() ? 0x100 : 0)
               | (client_info.sos ? 0x200 : 0);
    }

    private void setTrackingStatus() {
        final long now_systime = AndroidUtil.systemTimeMs();

        // Did a redraw happen within 3*update interval?
        // foreground = now_systime < client_info.last_redraw_time
        //           + 3 * Config.UPDATE_UI_INTERVAL;

        int new_tracking_status = TrackingStatus.NORMAL;
        if (now_systime <= last_test_systime + Config.TEST_BUTTON_ACTIVE) {
          new_tracking_status = TrackingStatus.TEST;
        }
        trackstat.setStatus(new_tracking_status);

        // TODO: This may be overkill
        // getGlobals().postStartGpsUpdates();
    }

    void send() {
        final long now_systime = AndroidUtil.systemTimeMs();
        final PositionTime this_pt = getPositionTime();

        // Time delta since last send, considering GPS time and systime
        final long delta_gpstime = this_pt.gpstime_ms - last_send_pt.gpstime_ms;
        final long delta_systime =     now_systime    - last_send_systime;

        // We send if either delta exceeds the update rate.
        if (   delta_gpstime >= UPDATE_RATE
            || delta_systime >= UPDATE_RATE) {
            updateClientInfo(this_pt);

            // Set drawing time to -1 if we're in background
            final int drawing_time = foreground ? client_info.drawing_time : -1;
            // Log.d(TAG, "Drawing: " + drawing_time);

            // Log.i(TAG, "Sending: this_gpstime = " + this_gpstime + " now_systime = " + now_systime);

            // Send gpstime -1 if we don't have a valid position.
            // (TODO: why?)

            // Heap analysis shows that StringBuilder is being used here
            // and the code is relatively memory-friendly.
            // TODO: Use String.format() and sensible accuracies
            String s = POSITION_PACKET + " "
                     + getUniqueId()   + " "

                     + current_sequence_number + " "
                     + now_systime + " "

                     // Numerical client info
                     + client_info.packets_received + " "
                     + client_info.packet_loss_percent + " "
                     + client_info.rtt_latency + " "
                     + drawing_time + " "
                     + client_info.network_type + " "
                     + client_info.cell_id + " "
                     + client_info.roaming + " "
                     + client_info.battery_level_percent + " "
                     + client_info.heading + " "

                     + this_pt.fix_systime + " "

                     + vehicleTypeAndFlags() + " "

                     + (position_valid ? this_pt.gpstime_ms : "-1")  + " "
                     + this_pt.latitude  + " " 
                     + this_pt.longitude + " " 
                     + this_pt.altitude + " "
                    
                     + this_pt.speed + " "
                     + this_pt.course + " "
                     + vertical_speed + " "

                     + this_pt.accuracy + " "

                     + last_test_systime + " "
                     + turn_rate;

            if (!NetFunctions.sendString(sock, address, Config.UDP_PORT, s)) {
                status.setSendError();
                address = null;
            } else {
                // Managed to send off the packet, update
                // the respective data.
                last_send_pt = this_pt;
                last_send_systime = now_systime;
                ++current_sequence_number;
            }
        }
    }

    private void send_bye() {
        String s = BYE_PACKET + ' '
                 + getUniqueId()   + ' '
                 + AndroidUtil.systemTimeMs();

        Log.i(TAG, "Sending bye packet");
        if (!NetFunctions.sendString(sock, address, Config.UDP_PORT, s)) {
            status.setSendError();
            address = null;
        }
    }

    private void send_hello_and_info(final long now_systime) {
        String net = telMan.getNetworkOperatorName();
        if (net.equals("")) {
          net = "NO_NETWORK";
        }

        String hello = HELLO_PACKET + ' '
                 + getUniqueId()   + ' '
                 + AndroidUtil.systemTimeMs() + ' '
                 + getGlobals().getAppVersion() + ' '
                 + Util.URLEncode(Build.MANUFACTURER) + ' '
                 + Util.URLEncode(Build.MODEL) + ' '
                 + Build.VERSION.RELEASE + ' '
                 + Build.VERSION.SDK_INT + ' '
                 + Util.URLEncode(net) + ' '
                 + telMan.getNetworkCountryIso()
        ;

        String info = INFO2_PACKET            + ' '
                 + getUniqueId()              + ' '
                 + AndroidUtil.systemTimeMs() + ' '
                 + getGlobals().getName1()    + ' '
                 + getGlobals().getName2()    + ' '
                 + getAndroidID()             + ' '
                 + getGlobals().getTrackingSymbolColorHtml()
        ;

        // Log.i(TAG, "Sending hello and info: " + info);
        if (   !NetFunctions.sendString(sock, address, Config.UDP_PORT, hello)
            || !NetFunctions.sendString(sock, address, Config.UDP_PORT, info)
            ) {
            status.setSendError();
            address = null;
            // Post re-send
            postHelloAndInfoPacket(now_systime);
        }
    }

    private void receive(final long now_systime) {
        final String msg = NetFunctions.receiveString(sock);

        // Log.d(TAG, "Received: " + msg);
        if (msg.equals("")) {
            // Timeout, nothing to do (we'll come back in 100ms)
            return;
        }
        // Receive error---never observed in practice.  Will
        // retry to connect and cause an address error if
        // that doesn't work.
        if (msg.equals("E")) {
            address = null;
            return;
        }
        
        String[] ss = whitespace_pattern.split(msg);

        if (ss.length < 1) {
            Log.w(TAG, "Bad packet format: " + ss.length + " field(s)");
            status.setParseError();
            return;
        }

        try {

            if (UPDATE_PACKET.equals(ss[0]) || UPDATE2_PACKET.equals(ss[0])) {
                if (ss.length < HEADER_FIELDS) {
                    Log.w(TAG, "Bad packet format: " + ss.length + " field(s)");
                    status.setParseError();
                    return;
                }

                // Update2 adds color to fields
                final int packet_version = UPDATE2_PACKET.equals(ss[0]) ? 2 : 1;
                // Log.i(TAG, "Packet version: " + packet_version);
                final int fields_per_target =
                    FIELDS_PER_TARGET_VERSION_1 + packet_version - 1;

                ServerInfo newinfo = new ServerInfo();
                // System time of the packet the server is replying to
                // with this packet.
                final long sent_systime = Long.parseLong    (ss[1]);
                newinfo.n_clients       = Integer.parseInt  (ss[2]);
                newinfo.map_orientation = Double.parseDouble(ss[3]);
                newinfo.moving          = Short.parseShort(ss[4]);
                newinfo.notification    = Integer.parseInt  (ss[5]);
                newinfo.n_targets       = Integer.parseInt  (ss[6]);

                if (HEADER_FIELDS + newinfo.n_targets * fields_per_target != ss.length) {
                    Log.w(TAG, "Bad packet format: " + ss.length + " field(s)");
                    status.setParseError();
                    return;
                }
                if (newinfo.n_targets > ServerInfo.MAX_TARGETS) {
                    Log.e(TAG, "Too many targets: " + newinfo.n_targets);
                    status.setParseError();
                    return; 
                }

                // Log.d(TAG, "# targets: " + newinfo.n_targets);
                for (int i = 0; i < newinfo.n_targets; ++i) {
                    final int idx = HEADER_FIELDS + i * fields_per_target;
                    assert idx + fields_per_target - 1 < ss.length;
                    assert newinfo.target[i] != null;

                    // ss[idx] is the marker A0, A1 etc.
                    newinfo.target[i].gpstime_ms      = Long.parseLong    (ss[idx + 1]);
                    newinfo.target[i].target          =                    ss[idx + 2];
                    newinfo.target[i].vehicle_type    = Integer.parseInt  (ss[idx + 3]);
                    newinfo.target[i].distance        = Double.parseDouble(ss[idx + 4]);
                    newinfo.target[i].bearing         = Double.parseDouble(ss[idx + 5]);
                    newinfo.target[i].relative_altitude = Double.parseDouble(ss[idx + 6]);
                    newinfo.target[i].speed           = Double.parseDouble(ss[idx + 7]);
                    newinfo.target[i].display_course_deg = Double.parseDouble(ss[idx + 8]);
                    newinfo.target[i].vertical_speed  = Double.parseDouble(ss[idx + 9]);
                    newinfo.target[i].heading         = Double.parseDouble(ss[idx + 10]);

                    newinfo.target[i].tCPA            = Double.parseDouble(ss[idx + 11]);
                    newinfo.target[i].dCPA            = Double.parseDouble(ss[idx + 12]);
                    newinfo.target[i].danger_level    = Double.parseDouble(ss[idx + 13]);
                    newinfo.target[i].notification    = Integer.parseInt  (ss[idx + 14]);
                    if (2 <= packet_version) {
                        newinfo.target[i].tracking_symbol_color =          ss[idx + 15];
                    } else {
                        newinfo.target[i].tracking_symbol_color =          "-";
                    }
                }

                // Packet is OK, but may still be too old
                last_receive_systime = now_systime;
                ++client_info.packets_received;
                final long n_sent = current_sequence_number - first_sequence_number;
                if (n_sent > 0) {
                  client_info.packet_loss_percent = 
                      100.0f * (1.0f -   (float) client_info.packets_received 
                                       / n_sent);
                }

                if (sent_systime <= now_systime) {
                    client_info.rtt_latency = (int)(now_systime - sent_systime);
                    if (client_info.rtt_latency < Config.RECEIVE_TIMEOUT_RTT_MAX) {
                        server_info = newinfo;
                        status.setAllOk();
                        // Packet received OK, post UI update
                        postUIUpdate();
                    } else {
                        // Else the packet is too old, discard it.
                        status.setTimeoutError();
                    }
                } else {
                    // No user-visible error, not observed in practice
                    Log.wtf(TAG, "WTF packet out of the future?!");
                }

            } else if (BYE_PACKET.equals(ss[0])) {
                // ktrax_bye <code> <toast>

                if (ss.length < 3) {
                    Log.w(TAG, "Bad bye packet format: " + ss.length + " field(s)");
                    status.setParseError();
                    return;
                }

                // final long code = Long.parseLong(ss[1]);
                // final String text = msg.substring(ss[0].length() + ss[1].length() + 1);

                // getGlobals().showToast(code, text);
                getGlobals().postStop();
            } else if (URL_PACKET.equals(ss[0])) {
                // ktrax_url <code> <url>

                if (ss.length < 3) {
                    Log.w(TAG, "Bad url packet format: " + ss.length + " field(s)");
                    status.setParseError();
                    return;
                }

                final long urlcode = Long.parseLong(ss[1]);
                final String url   = ss[2];

                getGlobals().openUrl(urlcode, url);
            } else if (TOAST_PACKET.equals(ss[0])) {
                // ktrax_toast <code> <toast>

                Log.w(TAG, "Toast currently not supported");

                /*
                final long code = Long.parseLong(ss[1]);
                final String text = msg.substring(ss[0].length() + ss[1].length() + 1);

                getGlobals().showToast(code, text);
                */
            } else {
                Log.w(TAG, "Unknown packet type: " + ss[0]);
                status.setParseError();
            }
        
        } catch (NumberFormatException e) {
            // exception typically doesn't have a message...
            Log.w(TAG, "Packet parse error: " + msg, e);
            status.setParseError();
        }
    }
}
