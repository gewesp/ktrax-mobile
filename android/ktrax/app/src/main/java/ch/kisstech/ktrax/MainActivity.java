//
// Atomicity in Java:
// http://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html
//
// For the purposes of the Java programming language memory model, a single
// write to a non-volatile long or double value is treated as two separate
// writes: one to each 32-bit half. This can result in a situation where a
// thread sees the first 32 bits of a 64-bit value from one write, and the
// second 32 bits from another write.
//
// Writes and reads of *volatile* long and double values are always atomic.
//
// Writes to and reads of *references* are always atomic, regardless of
// whether they are implemented as 32-bit or 64-bit values.
//
// [...]
//
// Programmers are encouraged to declare shared 64-bit values as
// volatile or synchronize their programs correctly to avoid possible
// complications.
//
// Android notes:
// * Notice that the MainActivity class is recreated e.g. on screen
//   rotation.  Any persistent data must be static.
//

package ch.kisstech.ktrax;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends Activity implements IPage {

    private TextView textView00;
    private TextView textView10;
    private TextView textView20;
    private TextView textView30;
    private TextView textView40;
    private TextView textView50;
    private TextView textView60;
    private TextView textView70;
    private TextView textView80;

    private TextView textView01;
    private TextView textView11;
    private TextView textView21;
    private TextView textView31;
    private TextView textView41;
    private TextView textView51;
    private TextView textView61;
    private TextView textView71;
    private TextView textView81;

    // private Button buttonQuit;
    // private Button buttonTest;

    private final static String TAG = "Ktrax.MainActivity";

    private final static String STRING_HYPHEN = "---";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "Creating text window");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        textView00 = (TextView) findViewById(R.id.textView00);
        textView10 = (TextView) findViewById(R.id.textView10);
        textView20 = (TextView) findViewById(R.id.textView20);
        textView30 = (TextView) findViewById(R.id.textView30);
        textView40 = (TextView) findViewById(R.id.textView40);
        textView50 = (TextView) findViewById(R.id.textView50);
        textView60 = (TextView) findViewById(R.id.textView60);
        textView70 = (TextView) findViewById(R.id.textView70);
        textView80 = (TextView) findViewById(R.id.textView80);


        textView01 = (TextView) findViewById(R.id.textView01);
        textView11 = (TextView) findViewById(R.id.textView11);
        textView21 = (TextView) findViewById(R.id.textView21);
        textView31 = (TextView) findViewById(R.id.textView31);
        textView41 = (TextView) findViewById(R.id.textView41);
        textView51 = (TextView) findViewById(R.id.textView51);
        textView61 = (TextView) findViewById(R.id.textView61);
        textView71 = (TextView) findViewById(R.id.textView71);
        textView81 = (TextView) findViewById(R.id.textView81);


        // buttonQuit = (Button)   findViewById(R.id.buttonQuit);
        // buttonTest = (Button)   findViewById(R.id.buttonTest);
        // Log.d(TAG, "NEW latTextView: " + latTextView.toString());

        // Initialize/update background tasks
        // Reset any variables to their default values.  This is especially
        // important since finish() doesn't actually terminate the process.
        PersistentLogic.getInstance().initialize_statics(this, this);
    }

    public void handleQuitButton(View button_unused) {
        PersistentLogic.getInstance().postStop();
    }

    public void handleTestButton(View button_unused) {
        PersistentLogic.getInstance().network_manager.sendTestPacket();
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_radar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    @Override
    public void update(DisplayInfo di) {
        // GPS based values
        final String t00, t01, t10, t11, t20, t21;
        if (di.pt.gpstime_ms > 0) {
            // 5 decimal places == 1m accuracy
            t00 = String.format("%.5f", di.pt.latitude);
            t01 = String.format("%.5f", di.pt.longitude);
            t10 = String.format("V %.1f/%.0f", di.pt.speed, di.pt.course);
            t11 = STRING_HYPHEN;
            t20 = String.format("A %.0f", di.pt.accuracy);

        } else {
            t00 = "LATITUDE";
            t01 = "LONGITUDE";
            t10 = "SPEED/COURSE";
            t11 = STRING_HYPHEN;
            t20 = "ACCURACY";
        }
        t21 = di.pmstat.getStatusString();

        // Network, server status
        final String t30, t31, t40, t41;
        t30 = String.format("NET %s", di.netstat.getNetworkStatus());
        t40 = String.format("SRV %s", di.netstat.getServerStatus());
        t31 = STRING_HYPHEN;
        t41 = STRING_HYPHEN;

        // System parameters
        final String t50, t51;
        t50 = String.format("BOATS %d", di.si.n_clients);
        t51 = String.format("L %d/PL %.0f", di.ci.rtt_latency, 
                                            di.ci.packet_loss_percent);

        // Target-related info
        final String t60, t61, t70, t71, t80, t81;

        t81 = String.format("NOT %d", di.ci.notification);

        if (di.si.n_targets > 0) {
            t60 = String.format("DIST %.0f", di.si.target[0].distance);
            t61 = String.format("BRG %.0f" , di.si.target[0].bearing);
            t70 = String.format("dCPA %.0f", di.si.target[0].dCPA);
            t71 = String.format("tCPA %.1f", di.si.target[0].tCPA);
            t80 = di.si.target[0].target;
        } else {
            t60 = STRING_HYPHEN;
            t61 = STRING_HYPHEN;
            t70 = STRING_HYPHEN;
            t71 = STRING_HYPHEN;
            t80 = STRING_HYPHEN;
        }

        // Log.d(TAG, "Posting UI update");
        PersistentLogic.getInstance().uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {

                textView00.setText(t00);
                textView10.setText(t10);
                textView20.setText(t20);
                textView30.setText(t30);
                textView40.setText(t40);
                textView50.setText(t50);
                textView60.setText(t60);
                textView70.setText(t70);
                textView80.setText(t80);

                textView01.setText(t01);
                textView11.setText(t11);
                textView21.setText(t21);
                textView31.setText(t31);
                textView41.setText(t41);
                textView51.setText(t51);
                textView61.setText(t61);
                textView71.setText(t71);
                textView81.setText(t81);

            }
        });
    }
}
