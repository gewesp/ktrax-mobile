//
// Orientation manager
//
// Handles updates based on magnetometer and accelerometers.
//
// Goal is to get a good estimate of the device heading.
//
// See
// http://www.codingforandroid.com/2011/01/using-orientation-sensors-simple.html
//
// The SENSOR_DELAY_... values are unspecified, NORMAL seems to be 200ms
// 

package ch.kisstech.ktrax;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import android.util.Log;


public class OrientationManager implements SensorEventListener {

    private final static String TAG = "Ktrax.Orientation";

    private final SensorManager sensMan;

    // Maximum slant for using magnetometer/accelerometer for heading
    // private final static float MAX_SLANT = 45 * (float) Math.PI / 180.0f;

    public final static float AZIMUT_NONE = 1800;

    private final static float ORIENTATION_CHANGE_THRESHOLD_SQUARDED =
              Config.ORIENTATION_CHANGE_THRESHOLD
            * Config.ORIENTATION_CHANGE_THRESHOLD;

    // Arrays kept in object to avoid garbage collect
    float[] mGravity;
    float[] mGeomagnetic;
    float[] mR = new float[9];
    float[] mI = new float[9]; 

    boolean orientation_changed = false;

    // orientation contains: azimut, pitch and roll
    float[] orientation = new float[3];

    public OrientationManager(SensorManager sensMan_in) {
        Log.i(TAG, "Starting up");
        sensMan = sensMan_in;

        final Sensor accelerometer = sensMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // TODO: API >= 18
        // UNCALIBRATED may be available on some phones where the calibrated
        // version isn't (weird, OK).  However, it doesn't deliver correct 
        // headings.
        // final Sensor magnetometer  = sensMan.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        final Sensor magnetometer  = sensMan.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        if (null == accelerometer) {
            Log.e(TAG, "No accelerometer found");
        }
        if (null == magnetometer) {
            Log.e(TAG, "No magnetometer found");
        }

        sensMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensMan.registerListener(this, magnetometer , SensorManager.SENSOR_DELAY_UI);
    }

    public void shutdown() {
        sensMan.unregisterListener(this);
        mGravity = null;
        mGeomagnetic = null;
        Log.i(TAG, "Shutting down");
    }

 
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {  }
 
    @Override
    public void onSensorChanged(SensorEvent event) {
        // Log.d(TAG, "onSensorChanged(): " + event.sensor.getType());
             
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values;
            // Log.d(TAG, "g0 " + mGravity[0]);
            // Log.d(TAG, "g1 " + mGravity[1]);
            // Log.d(TAG, "g2 " + mGravity[2]);
        }

        // if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mGeomagnetic = event.values;
            // Log.d(TAG, "g0 " + mGeomagnetic[0]);
            // Log.d(TAG, "g1 " + mGeomagnetic[1]);
            // Log.d(TAG, "g2 " + mGeomagnetic[2]);
        }
    }

    // Normalized acceleration value
    private float[] ng;

    // Should be called in the main loop
    public void detectOrientationChange() {
        if (null == mGravity) {
          return;
        }

        float[] n = mGravity.clone();

        final float g = 9.81f;
        n[0] /= g;
        n[1] /= g;
        n[2] /= g;

        if (null == ng) {
            ng = n;
            return;
        }

        // Compute difference between normed vectors
        final float dx = n[0] - ng[0];
        final float dy = n[1] - ng[1];
        final float dz = n[2] - ng[2];

        final float delta_squared = dx*dx + dy*dy + dz*dz;

        if (delta_squared >= ORIENTATION_CHANGE_THRESHOLD_SQUARDED) {
            ng = n;
            orientation_changed = true;
            Log.i(TAG, "acceleration threshold triggered: " + delta_squared);
        } else {
            // Log.i(TAG, "acceleration delta^2: " + delta_squared);
        }
    }

    // Gets *and clears* orientation changed flag.
    public boolean orientationChanged() {
        final boolean ret = orientation_changed;
        orientation_changed = false;
        return ret;
    }

    public float computeAzimut() {
        if (mGravity == null) {
            return AZIMUT_NONE + 1;
        }
        if (mGeomagnetic == null) {
            return AZIMUT_NONE + 2;
        }

        if (SensorManager.getRotationMatrix(mR, mI, mGravity, mGeomagnetic)) {
            SensorManager.getOrientation(mR, orientation);
            // Log.d(TAG, "o0 " + orientation[0]);
            // Log.d(TAG, "o1 " + orientation[1]);
            // Log.d(TAG, "o2 " + orientation[2]);

            // if (   Math.abs(orientation[1]) <= MAX_SLANT
            //     && Math.abs(orientation[2]) <= MAX_SLANT) {
            final float azimut  = orientation[0] / (float) Math.PI * 180.0f;
            // Log.d(TAG, "az: " + azimut);
            return azimut;
            // }
        } else {
            return AZIMUT_NONE;
        }
    }

}
