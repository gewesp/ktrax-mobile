//
// Position/time
//
// TODO: *Possibly* it's better to add a copy constructor in
// PositionTime and get rid of the location member in
// NetworkManager---it's a huge memory hog.  Ridiculous.
//
// TODO: Add vertical speed and turn rate here. 
//
// TODO: Do conversion of Location to PositionTime in PositionManager.java.
// I don't remember the reason for *not* doing that...
//

package ch.kisstech.ktrax;

import android.location.Location;

public class PositionTime {

    public final static double DEFAULT_ACCURACY = 5000;

    PositionTime() {
        fix_systime = -1;
        gpstime_ms = -1;
        accuracy  = DEFAULT_ACCURACY;
        latitude  = 0;
        longitude = 0;
        altitude  = 0;
        speed     = 0;
        course    = OrientationManager.AZIMUT_NONE;
    }

    PositionTime(Location location, long systime) {
        fix_systime = systime;
        gpstime_ms = location.getTime();
        accuracy = location.hasAccuracy() ? 
            location.getAccuracy() : DEFAULT_ACCURACY;
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        // Above WGS84 according to doc.
        altitude  = location.hasAltitude() ? 
            location.getAltitude() : 0.0;
        speed     = location.hasSpeed() ? 
            location.getSpeed() : 0.0;
        // Android developers think that bearing is the direction of travel...
        course    = location.hasBearing() ? 
            location.getBearing() : OrientationManager.AZIMUT_NONE;
    }

    // System time [ms] for this fix.
    public long fix_systime;

    // GPS time [ms] for this fix
    public long gpstime_ms;

    // Accuracy [m]
    public double accuracy;

    // Lat/lon [degrees]
    public double latitude;
    public double longitude;

    // Altitude [m] over WGS84 ellipsoid
    public double altitude;

    // Speed [m/s], course [degrees]
    public double speed;
    public double course;
}
