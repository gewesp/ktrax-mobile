//
// Holding of Ktrax business logic objects that need to be persistent across
// activity recreation.
//
// TODO/IMPORTANT:
// * Switch on mobile data:
//   http://stackoverflow.com/questions/11555366/enable-disable-data-connection-in-android-programmatically
// * KTrax may *not* work in Wi-Fi networks, the port may be blocked
//

package ch.kisstech.ktrax;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Handler;
import android.os.PowerManager;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
// import android.widget.Toast;

import java.util.HashSet;

import ch.kisstech.AndroidUtil;

public class PersistentLogic {
    final private static String TAG = "Ktrax.PersistentLogic";

    // OK, apparently holding the context here is a memory leak...
    // Whatever...
    private static PersistentLogic singleton = new PersistentLogic();

    public static PersistentLogic getInstance() {
        return singleton;
    }

    //////////////////////////////////////////////////////////////////////////
    // Public statics
    //////////////////////////////////////////////////////////////////////////
    final public static String ALLOWED_EXTRA_CHARACTERS = "-_";

    public static class NameSetter implements ch.kisstech.IStringSetter {
        private int the_number;

        // Create a name setter to set namei in network_manager
        NameSetter(int i) {
            the_number = i;
        }

        @Override
        public void set(final String name_in) {
            final String name =
                ch.kisstech.Util.sanitizeAlnumAndEmpty(
                    name_in.toUpperCase(), ALLOWED_EXTRA_CHARACTERS);
            
            if (1 <= the_number && the_number <= 3) {
                Log.i(TAG, "Setting name #" + the_number + " to: " + name);
            } else {
                Log.wtf(TAG, "NameSetter with invalid number?!");
                return;
            }
            switch (the_number) {
                case 1: getInstance().name1 = name; break;
                case 2: getInstance().name2 = name; break;
                case 3: getInstance().name3 = name; break;
                default: return;
            }
            getInstance().network_manager.postHelloAndInfoPacket();
        }
    }

    public void setName1(final String n) 
    { new NameSetter(1).set(n); }
    public void setName2(final String n) 
    { new NameSetter(2).set(n); }
    public void setName3(final String n) 
    { new NameSetter(3).set(n); }


    private PersistentLogic() {}

    // The (singleton) application context
    private Context ctx;

    // The activity currenly running.  This is the activity
    // to be stopped in doStop().
    private Activity mActivity;
    private final Object mActivity_lock = new Object();

    // http://stackoverflow.com/questions/5400288/update-textview-from-thread-please-help
    // http://corner.squareup.com/2013/10/android-main-thread-1.html
    public Handler uiThreadHandler;

    // Global variables for data exchange
    public float battery_level_percent;


    // Settings, setters and getters
    private boolean mTrackingEnabled;

    // Vehicle type
    private int vehicle_type = Config.DEFAULT_VEHICLE_TYPE;

    // Tracking symbol color (R.id value)
    private int tracking_symbol_color = R.id.set_color_automatic;

    // Callsign, competition number, etc. for ktrax_info packet
    private volatile String name1 = "-";
    private volatile String name2 = "-";
    private volatile String name3 = "-";

    /////////////////////////////////////////////////////////////////////////
    // Internal data structures
    /////////////////////////////////////////////////////////////////////////
    // @return The activity which called us with initialize_statics().  
    // Hopefully, that would be the one and only RadarActivity.
    public Activity theGlobalActivity() { return mActivity; }

    /////////////////////////////////////////////////////////////////////////
    // Settings interface
    /////////////////////////////////////////////////////////////////////////
    public String getName1() { return name1; }
    public String getName2() { return name2; }
    public String getName3() { return name3; }

    public void setTrackingEnabled(final boolean enable) {
      mTrackingEnabled = enable;
      Log.i(TAG, "Tracking: " + mTrackingEnabled);
    }

    public void setVehicleType(int t) {
        vehicle_type = t;
    }

    public boolean getTrackingEnabled() {
      return mTrackingEnabled;
    }

    public int getTrackingSymbolColor() {
      return tracking_symbol_color;
    }
    public String getTrackingSymbolColorHtml() {
      return ColorMapping.HtmlColorFromId(tracking_symbol_color);
    }
    public void setTrackingSymbolColor(final int col) {
        tracking_symbol_color = col;
        network_manager.postHelloAndInfoPacket();
    }

    public int getVehicleType() {
        return vehicle_type;
    }

    // Gets tracking URI
    public String getTrackingUri() {
      // We did have a NullPointerException here, probably due to a 
      // quit in progress
      final NetworkManager nm = network_manager;
      if (null != nm) {
          return Config.TRACKING_BASE_URI + "?ktraxid=" + nm.getUniqueId();
      } else {
          return Config.TRACKING_BASE_URI;
      }
    }

    // Statics that must persist across onCreate() calls.
    public NetworkManager  network_manager  = null;
    private PowerManager.WakeLock wake_lock  = null;

    private String mAppVersion = "(unknown)";

    // Has postStop() been called?
    private boolean stopPosted = false;

    // URLs or toasts already shown
    private HashSet<Long> urls  ;
    // private HashSet<Long> toasts;

    public String getAppVersion() {
        return mAppVersion;
    }
    public String getApplicationId() {
        return BuildConfig.APPLICATION_ID;
    }

    // A foreground service that designed to prevent Android from
    // killing us without warning
    // This is started indirectly by some Android magic replicating class
    // instantiation, so we don't need to create it here.
    // private DontKillMeService dkm;

    public void initialize_statics(Activity activity, IPage ipage) {
        synchronized(mActivity_lock) {
            mActivity = activity;
        }

        if (null != network_manager) {
            Log.i(TAG, "Background tasks already running");
            // Update to new activity (e.g. after orientation change)
            network_manager.setParent(ipage);
        } else {
            ctx = activity.getApplicationContext();
            mAppVersion = BuildConfig.VERSION_NAME;
            // ID in Play Store
            stopPosted = false;
            urls   = new HashSet<>();
            // toasts = new HashSet<Long>();

            Log.i(TAG, "Application ID: " + getApplicationId());
            Log.i(TAG, "Version: "        + getAppVersion());
            Log.i(TAG, "Starting background tasks...");
            uiThreadHandler = new Handler();

            // Super useless API verbosity here to get various managers...
            LocationManager locationManager = 
                (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

            TelephonyManager telManager =
                (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

            SensorManager sensMan =
                (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);

            // http://developer.android.com/training/monitoring-device-state/battery-monitoring.html#CurrentLevel
            ctx.registerReceiver(
                    null,
                    new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            // TODO: See
            // https://groups.google.com/forum/#!topic/android-developers/U4mOUI-rRPY
            final String unique_id = "andr:"
                        + Settings.Secure.getString(
                    ctx.getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Log.i(TAG, "Starting network manager, UID = " + unique_id);

          network_manager  = 
              new NetworkManager(ipage, activity, sensMan, locationManager, telManager, unique_id);

          PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
          wake_lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                                     "ktrax:gps_update_wakelock");
          Log.i(TAG, "Acquiring wake lock");
          wake_lock.acquire();

          Log.i(TAG, "Creating and starting DontKillMeService");
          if (null == ctx.startService(new Intent(ctx, DontKillMeService.class))) {
              Log.wtf(TAG, "Not found: " + DontKillMeService.class);
          }

          Log.i(TAG, "Background tasks started successfully");
        }
    }

    // The Most Useless API Verbosity here.  Do all destructors by hand in Java.
    private void finalize_statics() {
        if (null == network_manager) { 
            Log.wtf(TAG, "finalize_statics() invoked twice?!");
        } else {
            Log.i(TAG, "Stopping DontKillMeService");
            ctx.stopService(new Intent(ctx, DontKillMeService.class));

            Log.i(TAG, "Releasing wake lock");
            wake_lock.release();

            network_manager.shutdown();
            network_manager = null;
        }
    }

    // As always with Android, there's a multitude of halfway solutions
    // to the problem: 
    // http://stackoverflow.com/questions/5608720/android-preventing-double-click-on-a-button
    // synchronized() not necessary because callback invocations hopefully come
    // from the same thread.
    private void doStop() {
        try {
            Log.i(TAG, "Finalizing application...");
            finalize_statics();
            // Notice that finish does *not* terminate the process!

            synchronized(mActivity_lock) {
                if (null != mActivity) {
                    mActivity.finish();
                    Log.i(TAG, "Application finished");
                }
            }

        } catch (Exception e) {
            // TODO: This will likely leave the app in an inconsistent
            // state, we should rather die.
            Log.e(TAG, "Exiting application failed: ", e);
        }
    }


    // TODO: Do we need to stop GPS updates somehow here?!
    public synchronized void postStop() {
        if (stopPosted) {
            Log.w(TAG, "Multiple application stop requests");
            return;
        }

        stopPosted = true;

        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                PersistentLogic.getInstance().doStop();
            }
        });
        Log.i(TAG, "Stop in progress...");
    }

    public synchronized void postStartGpsUpdates() {
        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if (null != network_manager) {
                    network_manager.getPositionManager().startGpsUpdates();
                }
            }
        });
    }
        
    //public synchronized void postStopGpsUpdates() {
    //    uiThreadHandler.post(new Runnable() {
    //        @Override
    //       public void run() {
    //            if (null != network_manager) {
    //                network_manager.getPositionManager().stopGpsUpdates();
    //            }
    //        }
    //    });
    //}

    public void openUrl(final long code, final String url) {
        if (urls.contains(code)) {
            return;
        }

        urls.add(code);
        AndroidUtil.openUrl(ctx, url);
    }
/*
    public void showToast(final long code, final String text) {
        if (toasts.contains(code)) {
            return;
        }

        toasts.add(code);
        Log.i(TAG, "Showing toast: " + text);
        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
    }
*/
}
