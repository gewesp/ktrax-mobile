//
// TODO: 
// * Destroy PersistentLogic singleton on quit
//

package ch.kisstech.ktrax;

import android.app.Activity;
import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;


import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import ch.kisstech.AndroidUtil;

// import android.app.ActionBar;


public class RadarActivity extends Activity implements IPage {
    private final static String TAG = "Ktrax.RadarActivity";

    // http://developer.android.com/guide/topics/data/data-storage.html#pref
    public static final String PREFS_NAME = Config.APP_NAME + "PrefsFile";
    public static final String PREFS_TRACKING_ENABLED = "tracking_enabled";
    public static final String PREFS_VEHICLE_TYPE     = "vehicle_type";
    public static final String PREFS_TRACKING_SYMBOL_COLOR = "tracking_symbol_color";
    public static final String PREFS_NAME1            = "name1";
    public static final String PREFS_NAME2            = "name2";
    public static final String PREFS_NAME3            = "name3";
       
    private RadarView radarView;
    private Menu menu;



    ////////////////////////////////////////////////////////////////////////
    // BEGIN SPAGHETTI for location permission logic.
    ////////////////////////////////////////////////////////////////////////

    // The sole purpose of these lines is to ask the user for permission
    // for location services.  The same could be accomplished with 2-3
    // lines in the manifest.
    // Java and Android conventions make this a spaghetti monster.
    // https://stackoverflow.com/questions/40142331/how-to-request-location-permission-at-runtime/59857846

// Apparently, we need a magic number
public static final int MY_PERMISSIONS_REQUEST_LOCATION = 4711;

//
// Shows a dialog explaining the user why we really need the location.
// If activatePermissionsDialog is true, take the user to the respective
// dialog.
// Show an explanation to the user *asynchronously* -- don't block
// this thread waiting for the user's response! After the user
// sees the explanation, try again to request the permission.
//
public void showPermissionDialog(final boolean activatePermissionsDialog) {
  new AlertDialog.Builder(this)
        // .setTitle(R.string.title_location_permission)
        .setTitle("Location permission")
        // .setMessage(R.string.text_location_permission)
        .setMessage("We need your location for situational awareness and collision warnings. "
            + "Please grant location access to KTrax in the Android system menu.")
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (activatePermissionsDialog) {
                  // Prompt the user once explanation has been shown
                  ActivityCompat.requestPermissions(RadarActivity.this,
                          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                          MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }
        })
        .create()
        .show();
}

public void checkLocationPermission() {
    Log.i(TAG, "Checking location permissions...");
    if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

        Log.i(TAG, "Fine location permission was not granted");

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.i(TAG, "Explaining to user why we need the permission...");
            showPermissionDialog(true);
        } else {
            Log.i(TAG, "No explanation needed, requesting the permission");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            Log.i(TAG, "requestPermissions() returned without result");
        }
    } else {
        Log.i(TAG, "Fine location permission was granted");
    }
}

@Override
public void onRequestPermissionsResult(int requestCode,
                                       String permissions[], int[] grantResults) {
    Log.i(TAG, "Location permission result received, request code is: " + requestCode);

    for (String p : permissions) {
      Log.i(TAG, "Permission in result: " + p);
    }

    for (int gr : grantResults) {
      Log.i(TAG, "Grant result: " + gr);
    }

    switch (requestCode) {
        case MY_PERMISSIONS_REQUEST_LOCATION : {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission for coarse location was granted");

                // Permission for coarse was granted.  But we need fine permission!!
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                  Log.i(TAG, "Permission for fine location was granted");
                  
                  getGlobals().postStartGpsUpdates();
                } else {
                  Log.w(TAG, "Permission for fine location was not granted");
                  showPermissionDialog(false);
                }

            } else {
              Log.w(TAG, "Permission was not granted");
              showPermissionDialog(false);
            }
            break;
        }

        default : {
            Log.e(TAG, "Unknown request code: " + requestCode);
            break;
        }
    }
}


    ////////////////////////////////////////////////////////////////////////
    // END SPAGHETTI for location permission logic.
    ////////////////////////////////////////////////////////////////////////

    private PersistentLogic getGlobals() {
        return PersistentLogic.getInstance();
    }

    private void setColor(final int id) {
        final String col_html = ColorMapping.HtmlColorFromId(id);
        final String col_name = ColorMapping.colorNameFromId(id);
        menu.findItem(id).setTitle(
            Html.fromHtml("<font color=\"" + col_html + "\">" 
                                           + col_name + "</font>"));
    }

    private void setColors() {
        if (null == menu) {
            return;
        }
        setColor(R.id.set_color_f00);
        setColor(R.id.set_color_0f0);
        setColor(R.id.set_color_ff0);
        setColor(R.id.set_color_00f);
        setColor(R.id.set_color_60a);
        setColor(R.id.set_color_a50);
        setColor(R.id.set_color_0ff);
        setColor(R.id.set_color_000);
        setColor(R.id.set_color_fff);
    }

    private void setColorRadioButtonFromPers() {
        final int id = getGlobals().getTrackingSymbolColor();
        if (null == menu.findItem(id)) {
            Log.wtf(TAG, "Tracking symbol color menu entry not found?!");
        } else {
            menu.findItem(id).setChecked(true);
        }
    }

    private void syncMenu() {
        if (menu != null) {
            // Set state of tracking_enabled checkbox
            // getTrackingEnabled() is set in onCreate().
            menu.findItem(R.id.enable_tracking).setChecked(
                getGlobals().getTrackingEnabled());

            // Set vehicle type ...
            OptionSetter.specificSyncMenu(getGlobals(), menu);

            // ... and tracking color
            setColorRadioButtonFromPers();
        } else {
            Log.wtf(TAG, "No menu defined, but sync called?!");
        }
    }

    private void setTrackingEnabled(final boolean trackingEnabled) {
        getGlobals().setTrackingEnabled(trackingEnabled);
        syncMenu();
    }

    private void setGlobalsFromPreferences() {
        Log.i(TAG, "Loading settings...");
        // 0 stands for 'private' mode.  Whatever...
        final SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);

        // For testing: prefs.edit().clear().commit();

        getGlobals().setTrackingEnabled(
            prefs.getBoolean(PREFS_TRACKING_ENABLED, true));

        getGlobals().setVehicleType(
            prefs.getInt(PREFS_VEHICLE_TYPE, 
                         Config.DEFAULT_VEHICLE_TYPE));

        getGlobals().setTrackingSymbolColor(
            prefs.getInt(PREFS_TRACKING_SYMBOL_COLOR, R.id.set_color_automatic));

        getGlobals().setName1(
            prefs.getString(PREFS_NAME1, "-"));
        getGlobals().setName2(
            prefs.getString(PREFS_NAME2, "-"));
        getGlobals().setName3(
            prefs.getString(PREFS_NAME3, "-"));
    }

    private void saveGlobalsToPreferences() {
        final SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);

        // The commit may actually fail... But hasn't been observed in 
        // practice.
        prefs.edit()
             .putBoolean(PREFS_TRACKING_ENABLED, 
                         getGlobals().getTrackingEnabled())
             .putInt(    PREFS_VEHICLE_TYPE,
                         getGlobals().getVehicleType())
             .putInt(    PREFS_TRACKING_SYMBOL_COLOR,
                         getGlobals().getTrackingSymbolColor())
             .putString( PREFS_NAME1,
                         getGlobals().getName1())
             .putString( PREFS_NAME2,
                         getGlobals().getName2())
             .putString( PREFS_NAME3,
                         getGlobals().getName3())
             .apply();
        Log.i(TAG, "Settings saved successfully");
    }

    // This is called on exit (STOP button)
    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
        saveGlobalsToPreferences();
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop()");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()");

        checkLocationPermission();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);

        // https://developer.android.com/training/scheduling/wakelock.html
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Assume Android calls the correct constructors...
        radarView = (RadarView) findViewById(R.id.radar_view);

        // Should really throw, but then what?
        if (null == radarView) {
            Log.wtf(TAG, "NULL radar view?!");
        }

        // Make sure that the MEDIA stream gets controlled by the
        // Audio volume keys.
        // http://developer.android.com/reference/android/app/Activity.html#setVolumeControlStream(int)
        setVolumeControlStream(SoundManager.MY_STREAM);

        getGlobals().initialize_statics(this, this);

        setGlobalsFromPreferences();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_radar, menu);
        syncMenu();
        setColors();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        // Handle RowingCAS or KTrax specific options
        if (OptionSetter.specificSetOption(getGlobals(), this, id)) {
            syncMenu();
            return true;
        }

        switch (id) {
          case R.id.enable_tracking:
            // Toggle
            setTrackingEnabled(
                !getGlobals().getTrackingEnabled());
            return true;

          case R.id.share_tracking_link:
            setTrackingEnabled(true);
            AndroidUtil.shareText(
                this, 
                Config.APP_NAME + " tracking",
                  "My current location on " + Config.APP_NAME + ":\n"
                + getGlobals().getTrackingUri()
                + "\n\n"
                + Config.SIGNATURE_TEXT
                );
            return true;
          
          case R.id.set_color_automatic:
          case R.id.set_color_f00:
          case R.id.set_color_0f0:
          case R.id.set_color_ff0:
          case R.id.set_color_00f:
          case R.id.set_color_60a:
          case R.id.set_color_a50:
          case R.id.set_color_0ff:
          case R.id.set_color_000:
          case R.id.set_color_fff:
            getGlobals().setTrackingSymbolColor(id);
            syncMenu();
            return true;

          default:
            return super.onOptionsItemSelected(item);
        } // switch()
    }

    public void handleStopButton(View buttonUnused) {
        getGlobals().postStop();
        Log.i(TAG, "handleStopButton() finished");
    }

    public void handleTestButton(View buttonUnused) {
        getGlobals().network_manager.sendTestPacket();
    }

    public void handleSOSButton(View buttonUnused) {
        getGlobals().network_manager.toggleSOS();
    }

    public void handleHelpButton(View buttonUnused) {
        AndroidUtil.openUrl(this, Config.HELP_DOC_URI);
    }

    // IPage interface
    // TODO: Clone info?
    @Override
    public void update(final DisplayInfo info) {
        // Log.i(TAG, "Page update");
        radarView.update(info);
    }

}
