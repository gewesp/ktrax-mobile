package ch.kisstech.ktrax;

import android.app.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ch.kisstech.AndroidUtil;
import ch.kisstech.Util;


// KTrax specific version of OptionSetter.  Sets name1, name2 and name3
// See app/build.gradle for NAME1_TITLE, ...
public class OptionSetter {

    public static int RToVehicleType(final int id) {
      switch (id) {
          case R.id.type_gld : return VehicleType.GLIDER     ;
          case R.id.type_heli: return VehicleType.HELICOPTER ;
          case R.id.type_hgld: return VehicleType.HANG_GLIDER;
          case R.id.type_pgld: return VehicleType.PARAGLIDER ;
          case R.id.type_pwr : return VehicleType.POWER_PLANE;
          case R.id.type_jet : return VehicleType.JET        ;
          case R.id.type_uav : return VehicleType.UAV        ;
          default            : return VehicleType.UNKNOWN    ;
      }
    }

    public static int VehicleTypeToR(final int t) {
      switch (t) {
        case VehicleType.GLIDER     : return R.id.type_gld ;
        case VehicleType.HELICOPTER : return R.id.type_heli;
        case VehicleType.HANG_GLIDER: return R.id.type_hgld;
        case VehicleType.PARAGLIDER : return R.id.type_pgld;
        case VehicleType.POWER_PLANE: return R.id.type_pwr ;
        case VehicleType.JET        : return R.id.type_jet ;
        case VehicleType.UAV        : return R.id.type_uav ;

        default         : return R.id.type_heli;
      }
    }

    public static void specificSyncMenu(
        final PersistentLogic pers, final Menu menu) {
        MenuItem t = menu.findItem(VehicleTypeToR(
              pers.getVehicleType()));
        if (null != t) {
            t.setChecked(true);
        }
    }

    public static boolean specificSetOption(
        final PersistentLogic pers, final Context ctx, final int id) {
        switch (id) {
          case R.id.set_name1:
            AndroidUtil.showTextEditDialog(
                ctx, new PersistentLogic.NameSetter(1),
                Config.NAME1_TITLE,
                Config.NAME1_MESSAGE,
                PersistentLogic.getInstance().getName1(),
                Config.NAME1_HINT);
            return true;
          case R.id.set_name2:
            AndroidUtil.showTextEditDialog(
                ctx, new PersistentLogic.NameSetter(2),
                Config.NAME2_TITLE,
                Config.NAME2_MESSAGE,
                PersistentLogic.getInstance().getName2(),
                Config.NAME2_HINT);
            return true;
          case R.id.set_name3:
            AndroidUtil.showTextEditDialog(
                ctx, new PersistentLogic.NameSetter(3),
                Config.NAME3_TITLE,
                Config.NAME3_MESSAGE,
                PersistentLogic.getInstance().getName3(),
                Config.NAME3_HINT);
            return true;

          case R.id.type_gld : 
          case R.id.type_heli:
          case R.id.type_hgld:
          case R.id.type_pgld:
          case R.id.type_pwr :
          case R.id.type_jet :
          case R.id.type_uav :
            pers.setVehicleType(RToVehicleType(id));
            return true;

          default:
            return false;
        }
    }

}
