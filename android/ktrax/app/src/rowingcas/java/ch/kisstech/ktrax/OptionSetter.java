package ch.kisstech.ktrax;


import android.app.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ch.kisstech.AndroidUtil;


// RowingCAS specific version of OptionSetter.  
// Sets name1, name2.
public class OptionSetter {
    public static void specificSyncMenu(
        final PersistentLogic pers, final Menu menu) {
    }

    public static boolean specificSetOption(
        final PersistentLogic pers_unused, final Context ctx, final int id) {
        switch (id) {
          case R.id.set_name1:
            AndroidUtil.showTextEditDialog(
                ctx, new PersistentLogic.NameSetter(1),
                Config.NAME1_TITLE,
                Config.NAME1_MESSAGE,
                PersistentLogic.getInstance().getName1(),
                Config.NAME1_HINT);
            return true;

          case R.id.set_name2:
            AndroidUtil.showTextEditDialog(
                ctx, new PersistentLogic.NameSetter(2),
                Config.NAME2_TITLE,
                Config.NAME2_MESSAGE,
                PersistentLogic.getInstance().getName2(),
                Config.NAME2_HINT);
            return true;

          default:
            return false;
        }
    }
}
