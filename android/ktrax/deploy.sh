#! /bin/sh
# ./deploy 

function doit() {
appname=$1
buildtype=$2
src=./app/build/outputs/apk/$appname/$buildtype/app-$appname-$buildtype.apk

version=`grep versionName app/build.gradle                       \
         | sed -e "s/^ *versionName \"//"                        \
         | sed -e "s-\".*--"`

filename=$appname-$version.apk
dest=kiss-www@kisstech.ch:www.kisstech.ch/flask-ktrax/static/apk/$filename

ls -laF $src
echo Copying to: $dest
echo "*** Download URL: https://ktrax.kisstech.ch/static/apk/$filename"

scp $src $dest
}

doit ktrax_android debug
