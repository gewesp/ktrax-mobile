public func config_UDP_PORT() -> Int {
    return 48890
}

public struct Config {
    // This app's name
    static public let APPLICATION_NAME = "iCAS"

    // Radar display scale, outer circle [m]
    // 2 nautical miles
    static public let RADAR_SCALE:Float = 3704.0
}
