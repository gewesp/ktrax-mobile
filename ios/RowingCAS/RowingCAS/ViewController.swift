//
//  ViewController.swift
//
//  Created by Gerhard Wesp on 02/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

import UIKit


private let TAG = "ViewController"

private var startStopButtonGlobal:AnyObject? = nil


func setStartStopButton(start:Bool) {
    if (nil == startStopButtonGlobal) {
        NSLog("\(TAG): No start/stop button")
        return
    }

    if (start) {
        startStopButtonGlobal!.setTitle("START" , forState: UIControlState.Normal)
        startStopButtonGlobal!.setNeedsDisplay()
        NSLog("\(TAG): Start/stop button set to START")
    } else {
        startStopButtonGlobal!.setTitle("STOP", forState: UIControlState.Normal)
        startStopButtonGlobal!.setNeedsDisplay()
        NSLog("\(TAG): Start/stop button set to STOP")
    }
}


        

class ViewController: UIViewController {

    @IBOutlet weak var startStopButton: UIButton!
    
    // http://www.raywenderlich.com/74904/swift-tutorial-part-2-simple-ios-app
    @IBAction func testButtonTapped(sender : AnyObject) {
        NSLog("\(TAG): Test button tapped")
        dispatchTest()
    }

    @IBAction func stopButtonTapped(sender : AnyObject) {
        NSLog("\(TAG): Start/stop button tapped")
        startStop()
    }

    var asc: BPCompatibleAlertController?

    func setupMenu() {
        NSLog("\(TAG): Setting up menu")
        asc = BPCompatibleAlertController(
            title: "\(Config.APPLICATION_NAME) options", message: "", 
            alertStyle: BPCompatibleAlertControllerStyle.Alert)

        if nil == asc {
            NSLog("\(TAG): WTF: Failed to create menu")
            return
        }

        asc?.addAction(BPCompatibleAlertAction.defaultActionWithTitle(
            "Help", 
            handler: { (action) in
            NSLog("\(TAG): Help menu item selected")
            iOSUtil.openUrl(NotificationStrings.URL_DOC)
        }))

        asc?.addAction(BPCompatibleAlertAction.defaultActionWithTitle(
            "Share Tracking Link", 
            handler: { (action) in
            NSLog("\(TAG): Tracking")
            presentTrackingDialog(self)
            // iOSUtil.openUrl(NotificationStrings.URL_DOC)
        }))

        asc?.addAction(BPCompatibleAlertAction.cancelActionWithTitle(
            "Cancel",
            handler: { (action) in
            NSLog("\(TAG): Menu cancelled")
        }))
    }

    // http://swiftoverload.com/uialertcontroller-swift-example/ 
    // TODO: rename IBAction
    @IBAction func helpButtonTapped(sender: AnyObject) {
        NSLog("\(TAG): Menu button tapped")

        // asc?.presentFrom(self.navigationController, animated: true) { () in
        asc?.presentFrom(self, animated: true) { () in
            NSLog("\(TAG): Menu dismissed")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("\(TAG): viewDidLoad()")
        startStopButtonGlobal = startStopButton
        setupMenu()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
