//
//  AppDelegate.swift
//
//  Created by Gerhard Wesp on 02/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//
// TODO: Use dispatchStart(), dispatchStop() rather than calling directly?
//

import UIKit

private let TAG = "App"

/*
//
// Auto-launch from background:
// If true, automatically (re)-start the app when the device
// wakes up from background and the app is disabled.
//
// Set to true:
// * on using the start/stop button
// Set to false:
// * Shutdown from server
//

public var autoLaunchFromBackground = true
*/

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        NSLog("\(TAG): Application start")
        soundInit()
        start()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        NSLog("\(TAG): Becoming inactive")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NSLog("\(TAG): Entering background")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NSLog("\(TAG): Entering foreground")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSLog("\(TAG): Becoming active")
/*
        if (autoLaunchFromBackground) {
            start()
        } else {
            NSLog("\(TAG): Auto-launch disabled, not starting the app")
        }
*/
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        NSLog("\(TAG): Terminating")
        stop(NotificationStrings.INACTIVE_TERMINATED)
    }

}
