public func config_UDP_PORT() -> Int {
    return 48889
}

public struct Config {
    // This app's name
    static public let APPLICATION_NAME = "RowingCAS"

    // Radar display scale, outer circle [m]
    static public let RADAR_SCALE:Float = 500.0
}
