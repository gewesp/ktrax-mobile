//
// Network functions (UDP)
// This file collects all the awful low-level Swift code with type casting
// orgies and littered with UnsafePointers.
//
// Overview
//
// https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/NetworkingTopics/Articles/UsingSocketsandSocketStreams.html#//apple_ref/doc/uid/CH73-SW12
//
// Caution:
// * Network interface active/passive mode
//   http://stackoverflow.com/questions/13113435/gcdasyncudpsocket-on-ios-missing-multicasted-datagrams
//
// Stackoverflow CF examples:
//
// http://stackoverflow.com/questions/24796142/does-cfhostgetaddressing-support-ipv6-dns-entries
// http://stackoverflow.com/questions/24898001/do-a-simple-dns-lookup-in-swift
// http://stackoverflow.com/questions/25890533/how-can-i-get-a-real-ip-address-from-dns-query-in-swift?lq=1


import Foundation
import CoreFoundation

private let TAG = "NetFunctions"


func logSocketError(op:String, err:CFSocketError) {
    switch (err) {
      case CFSocketError.Success:
        break
        // NSLog("%@: %@: OK", TAG, op)
      case CFSocketError.Error:
        NSLog("%@: %@: Error", TAG, op)
      case CFSocketError.Timeout:
        NSLog("%@: %@: Timeout", TAG, op)
    }
}

func swiftSetupSocket(sock: CFSocket!) {
    
    let fd = CFSocketGetNative(sock)
    NSLog("%@: Setting up socket: \(fd)", TAG)
   
    // *Apparently*, this is fine with Swift data types...
    // var fd_c:CInt = fd
    
    let ret = disableSigPipe(fd)
    
    if 0 == ret {
        NSLog("\(TAG): OK")
    } else {
        NSLog("\(TAG): ERROR")
    }
    
}

func setReceiveTimeout(sock: CFSocket!, timeout: Double) {
    NSLog("%@: Setting receive timeout: %f", TAG, timeout)

    let fd = CFSocketGetNative(sock)

    // var fd_c:CInt = fd
    // var timeout_c:CDouble = timeout
    
    // *Apparently*, this is fine with Swift data types...
    setSocketTimeout(fd, timeout)
}

let BUFSIZE = 1024
// http://stackoverflow.com/questions/25840276/read-bytes-into-a-swift-string
func receiveString(sock: CFSocket!) -> String {
    let fd = CFSocketGetNative(sock)
    var buffer = UnsafeMutablePointer<Int8>.alloc(BUFSIZE)
    let ptr = UnsafeMutablePointer<Void>(buffer)
    var ret = ""
  
    let nbytes = recv(fd, ptr, BUFSIZE, 0)
    if (nbytes > 0) {
        // NSLog("%@: %d byte(s) received", TAG, nbytes)
        if (nbytes < BUFSIZE) {
            buffer[nbytes] = 0
        } else {
            buffer[BUFSIZE - 1] = 0
        }
        ret = String.fromCString(buffer)!
    } else {
        // NSLog("%@: receive timed out", TAG)
    }
    buffer.dealloc(BUFSIZE)
    return ret
}

func createUDPSocket() -> CFSocket! {
  // https://developer.apple.com/library/mac/documentation/CoreFoundation/Reference/CFSocketRef/index.html#//apple_ref/c/func/CFSocketCreate
  let sock = CFSocketCreate(
      nil,
      PF_INET,
      SOCK_DGRAM,
      -1,  // use default protocol (UDP)
      0, // bitwise OR of callback situations
      nil,
      nil)  // ??
  if (nil === sock) {
    NSLog("%@: CFCreateSocket() failed", TAG)
  } else {
    NSLog("%@: UDP socket created successfully", TAG)
  }
  return sock
}


// https://developer.apple.com/library/mac/documentation/CoreFoundation/Reference/CFHostRef/index.html#//apple_ref/c/func/CFHostStartInfoResolution

func resolveHostPort(host: String, port: Int) -> CFData? {
    // NSLog("%@: Resolving %@:%d", TAG, host, port)

    // Idiotic APIs:
    // "if the called function does not retain the object before returning it,
    // you use the takeUnretainedValue()
    // https://developer.apple.com/library/ios/documentation/Swift/Conceptual/BuildingCocoaApps/WorkingWithCocoaDataTypes.html
    // We assume that CreateWithName() does *not* retain the reference.
    // However, the use count is 3 if we use takeUnretainedValue(),
    // 2 otherwise.
    // 

    let hostRef = CFHostCreateWithName(kCFAllocatorDefault, host)
                  .takeUnretainedValue()
    
    // NSLog("%@: hostRef retain count: %d", TAG, CFGetRetainCount(hostRef))
    CFHostStartInfoResolution(hostRef, CFHostInfoType.Addresses, nil)
    var resolved:Boolean = 0

    let maybe_addresses_unmanaged =
        CFHostGetAddressing(hostRef, &resolved)
    
    if (nil == maybe_addresses_unmanaged) {
        NSLog("%@: Could not resolve %@", TAG, host)
        return nil
    }
    
    let maybe_addresses = maybe_addresses_unmanaged.takeRetainedValue()
    
    
    let addresses:CFArray! = maybe_addresses

    let addresses_ns = addresses as NSArray

    NSLog("%@: %d address(es) found for %@", TAG, addresses_ns.count, host)

    if 0 == addresses_ns.count {
        NSLog("%@: Zero addresses, returning", TAG)
        return nil
    }

    for addr in addresses_ns as! [NSData] {
        // let s = "Interpolation: \(addr)"
        // NSLog("%@: Interpolation: %@", TAG, s)
        var array = [UInt8](count: 16, repeatedValue: 0)
        addr.getBytes(&array, length: 16)
        NSLog("%@: Address %d.%d.%d.%d", TAG, array[4], array[5], array[6], array[7])
    }
    
    let addr1 = addresses_ns[0] as! NSData
    
    NSLog("%@: Address size: %d", TAG, addr1.length)
    
    // Stackoverflow examples have sockaddr_storage which has 128 bytes
    // and then send fails...
    var addr2 = UnsafeMutablePointer<sockaddr_in>.alloc(1)
    addr1.getBytes(addr2, length: sizeof(sockaddr_in))
    let addr_ref_inet = UnsafeMutablePointer<sockaddr_in>(addr2)
    // Caution: network byte order!
    // See http://stackoverflow.com/questions/24977805/socket-server-example-with-swift
    addr_ref_inet.memory.sin_port = UInt16(port).bigEndian

    let addr3 = UnsafeMutablePointer<UInt8>(addr2)
    let addr4 = CFDataCreate(nil, addr3, sizeof(sockaddr_in))

    NSLog("%@: hostRef retain count: %d", TAG, CFGetRetainCount(hostRef))

    // AF_INET is 2
    NSLog("%@: Family: %d", TAG, addr_ref_inet.memory.sin_family)
    NSLog("%@: Port: %d", TAG, addr_ref_inet.memory.sin_port.bigEndian)

    return addr4
}

func sendData(sock: CFSocket, addr: CFData, data: NSData, timeout: Double) -> Bool {
  // NSLog("%@: Address length: %d", TAG, CFDataGetLength(addr))
  let dbg = addr as NSData!
  let interp = "\(dbg)"
  // NSLog("%@: Sending to address: %@", TAG, interp)
  let err = CFSocketSendData(sock, addr, data as CFData!, timeout)
  logSocketError("send", err)
  return CFSocketError.Success == err
}

func sendString(sock: CFSocket, addr: CFData, str: String, timeout: Double) -> Bool {
    let data = str.dataUsingEncoding(NSUTF8StringEncoding)
    return sendData(sock, addr, data!, timeout)
}
