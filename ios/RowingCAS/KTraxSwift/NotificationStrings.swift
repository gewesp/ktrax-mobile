//
// English UI strings
//
// These do not contain text, just a maximum of 2
// short words.
//

struct NotificationStrings {
    // Generic
    static let STRING_OK      = "OK"
    static let STRING_INIT    = "INIT"
    // static let STRING_ERROR   = "ERROR"

    // GPS specific
    static let GPS_DISABLED  = "Please grant permission!"
    static let GPS_3D        = "3D"
    static let GPS_ACQUIRING = "ACQUIRING"
    static let GPS_ERROR     = "ERROR"
    static let GPS_OFF       = "OFF"

      // Network specific
    static let ERROR_ADDRESS    = "ERROR (address)"
    static let ERROR_INIT       = "ERROR (initialize)"
    static let ERROR_SEND       = "ERROR (send)"
    static let ERROR_CONNECTION = "ERROR (connection)"
    static let ERROR_TIMEOUT    = "ERROR (timeout)"
    static let ERROR_PARSE      = "ERROR (parse)"


    // Not really notifications, but constants as well...
    static let URL_DOC =
        "http://www.kisstech.ch/rowingcas/doc.shtml"

    static let INACTIVE_TEXT       = "\(Config.APPLICATION_NAME) stopped"
    static let INACTIVE_TERMINATED = "\(Config.APPLICATION_NAME) terminated by iOS"
}
