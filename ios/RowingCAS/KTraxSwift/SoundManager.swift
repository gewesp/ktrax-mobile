import AudioToolbox
import AVFoundation

private let TAG = "SoundManager"

// Sounds -> ID
private var sounds = [Int:SystemSoundID]()

// Duration of the sounds [ms], a bit shortened.  Used for avoidance of
// multiple sounds at a time.
private var duration = [Int:Int64]()

// Completion time of currently playing sound, [ms]
// in iOSUtil.systemTimeMs()
private var completion:Int64 = -100000000

private func loadSound(mysoundname: String) -> SystemSoundID {
    let soundURL = NSBundle.mainBundle().URLForResource(mysoundname, withExtension: "caf")
    
    NSLog("\(TAG): Sound name: \(mysoundname), URL: \(soundURL)")

    var id: SystemSoundID = 0xffffffff
    let status = AudioServicesCreateSystemSoundID(soundURL, &id)
    NSLog("\(TAG): Status: \(status), ID: \(id)")
    return id

}

func soundInit() {
  if (0 == sounds.count) {
      sounds[Notification.NOTIFICATION_ALARM_1] = loadSound("alarm1")
      sounds[Notification.NOTIFICATION_ALARM_2] = loadSound("alarm2")
      sounds[Notification.NOTIFICATION_ALARM_3] = loadSound("alarm3")
      sounds[Notification.NOTIFICATION_SOS    ] = loadSound("alarm3")

      sounds[Notification.NOTIFICATION_1_DIT  ] = loadSound("1dit")
      sounds[Notification.NOTIFICATION_3_DIT  ] = loadSound("3dit")

      sounds[Notification.NOTIFICATION_OK     ] = loadSound("ok"    )
      sounds[Notification.NOTIFICATION_ERROR  ] = loadSound("error" )

      sounds[Notification.NOTIFICATION_OFF    ] = loadSound("off"   )

      duration = [
          Notification.NOTIFICATION_ALARM_1 : 2000,
          Notification.NOTIFICATION_ALARM_2 : 2000,
          Notification.NOTIFICATION_ALARM_3 : 2000,
          Notification.NOTIFICATION_SOS     : 2000,

          Notification.NOTIFICATION_1_DIT   :  1000,
          Notification.NOTIFICATION_3_DIT   :  1000,

          Notification.NOTIFICATION_OK      :  1000,
          Notification.NOTIFICATION_ERROR   :  2000,
          Notification.NOTIFICATION_OFF     :  1000]

      NSLog("\(TAG): Sounds loaded, dict: \(sounds)")
  } else {
      NSLog("\(TAG): \(sounds.count) already loaded")
  }
}

var notificationPosted:Int = Notification.NOTIFICATION_NONE

func soundPlay(notification_new: Int) {
    notificationPosted = notification_new

    if Notification.NOTIFICATION_NONE == notificationPosted {
        return
    }

    let id = sounds[notificationPosted]
    if id != nil {
        let now = iOSUtil.systemTimeMs()
        if (now < completion) {
            NSLog("\(TAG): Sound in progress, skipping \(notificationPosted)")
        } else {
            // Once a clean Swift API is available, we should use
            // AudioServicesAddSystemSoundCompletion here.
            completion = now + duration[notificationPosted]!
            NSLog("\(TAG): Playing sound for \(notificationPosted)")
            notificationPosted = Notification.NOTIFICATION_NONE
            AudioServicesPlaySystemSound(id!)
        }
    } else {
        NSLog("\(TAG): WTF: Sound for \(notificationPosted) not found")
    }
}
