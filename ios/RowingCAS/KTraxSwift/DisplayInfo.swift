//
// Collection of all info to be displayed
//

struct DisplayInfo {
    var pt = PositionTime()
    var si = ServerInfo()
    var ci = ClientInfo()
    var pmstat = PositionManagerStatus()
    var netstat = NetworkManagerStatus()

    var active = true
    var inactiveText = NotificationStrings.INACTIVE_TEXT

    func allOk() -> Bool {
        return pmstat.ok() && netstat.ok()
    }

    init(
        pt_new: PositionTime,  // *my* position
        si_new: ServerInfo,
        ci_new: ClientInfo,
        pmstat_new: PositionManagerStatus,
        netstat_new: NetworkManagerStatus) {
      pt = pt_new
      si = si_new
      ci = ci_new
      pmstat = pmstat_new
      netstat = netstat_new
      active = true
    }

    init() {

    }

}
