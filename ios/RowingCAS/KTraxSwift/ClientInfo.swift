//
// Client info and statistics.
//

struct ClientInfo {
    // Number of packets received.
    var packets_received = 0

    // Packet loss, based on sequence_number and packets_received.
    var packet_loss_percent = 0.0

    // Round-trip latency [ms]
    var rtt_latency = 0

    // Drawing time for radar [ms]
    var drawing_time = 0

    // Current network type (GSM, 3G, 4G, etc)
    var network_type = -1

    // Cell ID, -1 if unknown
    var cell_id = -1

    // Are we currently roaming?
    var roaming = 0

    // Battery level [percent]
    var battery_level_percent = 100.0

    // Device heading [degree], computed using magnetometer and
    // accelerometers.  AZIMUT_NONE means the respective sensors
    // are not present.
    var heading:Float = 1800.0

    // Device course over ground computed from GPS.
    var course:Float = 1800.0

    // Audio notification, merges server-side notification
    // and client-side (e.g. GPS acquisition/loss, errors, ...).
    // Server notifications have precedence.
    var notification = Notification.NOTIFICATION_NONE
}
