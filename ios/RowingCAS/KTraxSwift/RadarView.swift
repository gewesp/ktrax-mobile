//
// TODO:
// * Dashed inner circle
// * Timing
// * Post redraw in update
// * Number formatting (accuracy)
// 

import UIKit

// For math functions, need to import Darwin!?
import Darwin
import Foundation

// Foo must be a struct in order for that to work
/*
struct Foo {
  static let SIZE = 50
  static let HALF_SIZE = SIZE / 2
}
*/

// http://stackoverflow.com/questions/27052665/swift-playground-core-graphics-core-text-custom-view
func drawText(context: CGContextRef, 
    text: String, 
    attributes: [String: AnyObject], 
    x: Float, y: Float) -> CGSize {
    let font = attributes[NSFontAttributeName] as! UIFont
    let attributedString = NSAttributedString(
        string: text, attributes: attributes)

    let textSize = text.sizeWithAttributes(attributes)

    // y: Add font.descender (its a negative value) to align the text at the baseline
    let textPath    = CGPathCreateWithRect(CGRect(x: CGFloat(x), y: CGFloat(y) - font.descender, width: ceil(textSize.width), height: ceil(textSize.height)), nil)
    let frameSetter = CTFramesetterCreateWithAttributedString(attributedString)
    let frame       = CTFramesetterCreateFrame(frameSetter, CFRange(location: 0, length: attributedString.length), textPath, nil)

    CTFrameDraw(frame, context)
    CGContextStrokePath(context)

    return textSize
}


// http://stackoverflow.com/questions/26804066/does-swift-has-static-variable-in-class
// (the answer is, of course, no...)

private let TAG = "RadarView"

    // Ratio of outer circle wrt. canvas
    let RADAR_RATIO:Float = 0.9

    // Ratio of moving target markers
    let TARGET_RATIO:Float = 0.075

    // Ratio of stopped target marker (circle)
    let STOPPED_TARGET_RATIO:Float = 0.04

    // Size increment for dangerous targets
    let DANGEROUS_INCREMENT:Float = 1.5

    // Ratio for distance of North from center
    let NORTH_DISTANCE_RATIO:Float = 0.92

    // Ratio for North symbol size
    let NORTH_SYMBOL_RATIO:Float = 0.1

    // x/y of 'N', in 'size' units from center
    let TEXT_X:Float = -1.0 * RADAR_RATIO

    let NORTH_UP_Y:Float = -0.9 * RADAR_RATIO
 
    // Text size in 'size' units
    let TEXT_SIZE_RATIO:Float = 0.12

    let LINE_SPACE_RATIO:Float = 0.16

    // Stroke width ratio
    let STROKE_WIDTH_RATIO:Float = 0.025

    // Dash length ratio
    let DASH_RATIO:Float = 0.075

    // 3 coordinates for target triangle, units of TARGET_RATIO
    // Zero rotation: pointing UP
    let P1X:Float =  0.0
    let P1Y:Float = -1.0

    let P2X:Float =  0.5
    let P2Y:Float =  0.5
    let P3X:Float = -0.5
    let P3Y:Float =  0.5

    // 3 coordinates for the North triangle
    // Zero rotation: Pointing NORTH
    let N1X:Float =  1.0
    let N1Y:Float =  0.0
    let N2X:Float =  0.0
    let N2Y:Float = -1.0
    let N3X:Float = -1.0
    let N3Y:Float =  0.0

    // Errors linger for 10s
    let ERROR_DISPLAY_PERIOD_MS = 10000

    // Show accuracy if > 20m
    let ACCURACY_SHOW_THRESHOLD:Float = 20.5

var displayInfo = DisplayInfo()

var myView:UIView? = nil

func dispatchRadarUpdate(di: DisplayInfo) {
    dispatch_async(dispatch_get_main_queue()) {
        if nil != myView {
            displayInfo = di
            myView!.setNeedsDisplay()
            // NSLog("\(TAG): Redraw request posted")
        }
    }
}

class RadarView:  UIView {
    // class let TAG = "Ktrax.RadarView"


    // Time of last error display
    var last_error_display:Int64 = -1
    
    // Weird boilerplate:
    // http://stackoverflow.com/questions/24339145/how-do-i-write-a-custom-init-for-a-uiview-subclass-in-swift
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        myView = self
    }
    
    func errorOkColor(ok: Bool) -> CGColor{
        if (ok) {
            return UIColor.greenColor().CGColor
        } else {
            return UIColor.redColor().CGColor
        }
    }

    // Returns the target color according to notification level
    func setTargetPaint(notification: Int) {
        switch (notification) {
            case Notification.NOTIFICATION_ALARM_1:
                UIColor.yellowColor().set()
            case Notification.NOTIFICATION_ALARM_2:
                UIColor.orangeColor().set()
            case Notification.NOTIFICATION_ALARM_3:
                UIColor.redColor().set()
            case Notification.NOTIFICATION_SOS    :
                UIColor.redColor().set()
            default:
                UIColor.greenColor().set()
        }
    }

    // Set gray or white, depending on ok
    func radarColor(ok: Bool) -> UIColor {
        if ok {
            return UIColor.whiteColor()
        } else {
            return UIColor.grayColor()
        }
    }

    func drawCircle(
        context: CGContext,
        x: Float, y: Float, 
        radius: Float) {
        CGContextAddArc(context, CGFloat(x), CGFloat(y), CGFloat(radius),
                        0, CGFloat(2.0 * M_PI), 1)
        CGContextStrokePath(context);
    }

    func drawStoppedTarget(
        context: CGContext,
        // final Paint paint,
        x: Float, y: Float, 
        size_px: Float) {
        drawCircle(context, x: x, y: y, radius: size_px)
    }

    // Draws a single target with paintTarget, the given center x, y of
    // the shape and the angle.
    // x, y: position, size_px: target size in pixels
    func drawTarget(
        context: CGContext,
        x: Float, y: Float,
        size_px: Float,
        angle: Float) {

        let ca = size_px * cos(angle)
        let sa = size_px * sin(angle)

        // We transform by
        // ( ca -sa )
        // ( sa  ca )
        let p1x = x + ca * P1X - sa * P1Y
        let p1y = y + sa * P1X + ca * P1Y

        let p2x = x + ca * P2X - sa * P2Y
        let p2y = y + sa * P2X + ca * P2Y

        let p3x = x + ca * P3X - sa * P3Y
        let p3y = y + sa * P3X + ca * P3Y


        var path = CGPathCreateMutable()
        
        CGPathMoveToPoint   (path, nil, CGFloat(p1x), CGFloat(p1y))
        CGPathAddLineToPoint(path, nil, CGFloat(p2x), CGFloat(p2y))
        CGPathAddLineToPoint(path, nil, CGFloat(p3x), CGFloat(p3y))
        CGPathCloseSubpath  (path)

        CGContextAddPath(context, path)
        CGContextStrokePath(context)
    }

    func drawTargets(
        context: CGContext,
        map_orientation: Float,
        status_ok: Bool,
        cx: Float, cy: Float, size: Float) {

        for (var i = 0; i < displayInfo.si.n_targets; ++i) {
            let alpha = (displayInfo.si.target[i].bearing - map_orientation)
                        * Float(M_PI / 180.0)
            let ca = cos(alpha)
            let sa = sin(alpha)
            let dist_px =   displayInfo.si.target[i].distance 
                          / Config.RADAR_SCALE * RADAR_RATIO * size

            let x = cx + sa * dist_px
            let y = cy - ca * dist_px

            let cog = displayInfo.si.target[i].display_course_deg
            let noti = displayInfo.si.target[i].notification
            setTargetPaint(noti)

            // Target size increment in case of danger
            let increment = Notification.NOTIFICATION_NONE == noti ?
                    Float(1.0) : DANGEROUS_INCREMENT

            if (cog > 1799.0) {
                drawStoppedTarget(context, x: x, y: y,
                    size_px: size * STOPPED_TARGET_RATIO * increment)
            } else {
                drawTarget(context, x: x, y: y,
                    size_px: size * TARGET_RATIO * increment,
                    angle: Float(M_PI / 180.0) * (cog - map_orientation))
            }
        }

        radarColor(status_ok).set()

        // Draw own ship
        if (0 == displayInfo.si.moving) {
            drawStoppedTarget(context, x: cx, y: cy, size_px: size * STOPPED_TARGET_RATIO)
        } else {
            // Putting everything on one line results in extremely weird (wrong)
            // error messages
            let angle = Float(M_PI / 180.0) * (displayInfo.ci.course - map_orientation)
            drawTarget(context, x: cx, y: cy, 
                       size_px: size * TARGET_RATIO, angle: angle)   
        }
    }
    
    // Draws a triangle pointing North based on map_orientation
    // cx, cy: Center of radar [px]
    // size:  Radar screen size in pixels (square up to screen boundaries).
    func drawNorthIndicator(
        context: CGContext, 
        cx:Float, cy:Float, size:Float, map_orientation:Float) {
        let angle = -map_orientation * Float(M_PI / 180.0)

        let radius_px = size * NORTH_DISTANCE_RATIO
        let ca = cos(angle)
        let sa = sin(angle)

        // Reference point [px, screen] of triangle, outside outer circle
        let x = cx + sa * radius_px
        let y = cy - ca * radius_px

        // Scaled sine/cosine
        let sca = size * NORTH_SYMBOL_RATIO * ca
        let ssa = size * NORTH_SYMBOL_RATIO * sa

        // Three vertices [px, screen]
        let n1x = x + sca * N1X - ssa * N1Y
        let n1y = y + ssa * N1X + sca * N1Y

        let n2x = x + sca * N2X - ssa * N2Y
        let n2y = y + ssa * N2X + sca * N2Y

        let n3x = x + sca * N3X - ssa * N3Y
        let n3y = y + ssa * N3X + sca * N3Y

        var path = CGPathCreateMutable()
        
        CGPathMoveToPoint   (path, nil, CGFloat(n1x), CGFloat(n1y))
        CGPathAddLineToPoint(path, nil, CGFloat(n2x), CGFloat(n2y))
        CGPathAddLineToPoint(path, nil, CGFloat(n3x), CGFloat(n3y))
        CGPathCloseSubpath  (path)

        CGContextAddPath(context, path)
        CGContextFillPath(context);
    }

    // http://stackoverflow.com/questions/2571550/how-i-set-the-background-color-in-uiview-using-cgcontext
    func makeBlack(context: CGContext, canvas: CGRect) {
        var context = UIGraphicsGetCurrentContext();
        let all = CGRect(x: canvas.origin.x,
                         y: canvas.origin.y,
                         width: canvas.size.width,
                         height: canvas.size.height)

        // All black
        CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0)
        CGContextFillRect(context, all)
    }

    // https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/CGContext/
    func drawRadar(canvas: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        makeBlack(context, canvas: canvas)
        
        // http://stackoverflow.com/questions/7213533/cgcontextshowtext-is-rendering-upside-down-text-mirror-image
        let trans = CGAffineTransformMakeScale(1, -1);
        CGContextSetTextMatrix(context, trans);

        let width  = Float(canvas.size.width)
        let height = Float(canvas.size.height)

        // Log.i(TAG, "Canvas width = " + width + ", height = " + height)

        let cx = width  / 2
        let cy = height / 2

        let size = min(cx, cy)

        // Radar and target line width
        let stroke = size * STROKE_WIDTH_RATIO
        CGContextSetLineWidth(context, CGFloat(stroke));

        let dash = size * DASH_RATIO
        //paint_radar_dashed.setPathEffect(
        //         new DashPathEffect(new float[]{dash, dash}, 0.0))

        let low_accuracy = displayInfo.pt.accuracy >= ACCURACY_SHOW_THRESHOLD
        let status_ok = displayInfo.allOk() && !low_accuracy
        
        // %s doesn't work for obscure reasons, %@ doesn't work for integers...
        // https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/Strings/Articles/FormatStrings.html
        // NSLog("%@: Drawing, status: %d", TAG, status_ok ? 1 : 0)

        radarColor(status_ok).set()
        radarColor(status_ok).setFill()

        // Parameters after the first one need to be named, cf.:
        // http://stackoverflow.com/questions/24050844/swift-missing-argument-label-xxx-in-call
        drawCircle(context, x: cx, y: cy, radius:       RADAR_RATIO * size)
        drawCircle(context, x: cx, y: cy, radius: 0.5 * RADAR_RATIO * size)

        let text_x    = cx + size * TEXT_X
        let text_y    = cy + size * NORTH_UP_Y
        let textsize  = size * TEXT_SIZE_RATIO
        let linespace = size * LINE_SPACE_RATIO

        // TODO: Add screen rotation
        //   + AndroidUtil.getScreenRotation(getContext())
        let map_orientation = Float(displayInfo.si.map_orientation)
        drawNorthIndicator(context, cx: cx, cy: cy, 
                           size: size, map_orientation: map_orientation)

        var textAttributes : [String: AnyObject] = [
            NSForegroundColorAttributeName : UIColor(white: 1.0, alpha: 1.0).CGColor,
            NSFontAttributeName : UIFont.systemFontOfSize(CGFloat(textsize))
        ]

        if (!displayInfo.active) {
            textAttributes[NSForegroundColorAttributeName] = UIColor.grayColor().CGColor
            drawText(context,
                displayInfo.inactiveText,
                textAttributes,
                text_x, text_y + 0 * linespace)
            return
        }

        let now = iOSUtil.systemTimeMs()
        if (   !status_ok 
            || displayInfo.netstat.displayStatus()
            || now <= last_error_display + ERROR_DISPLAY_PERIOD_MS) {
            textAttributes[NSForegroundColorAttributeName] =
                errorOkColor(displayInfo.pmstat.ok())
            drawText(context,
                "GPS: " + displayInfo.pmstat.getStatusString(),
                textAttributes,
                text_x, text_y + 0 * linespace)

            textAttributes[NSForegroundColorAttributeName] =
                errorOkColor(displayInfo.netstat.networkOk())
            drawText(context,
                "NET: " + displayInfo.netstat.getNetworkStatus(),
                textAttributes,
                text_x, text_y + 1 * linespace)

            textAttributes[NSForegroundColorAttributeName] =
                errorOkColor(displayInfo.netstat.serverOk())
            drawText(context,
                "SRV: " + displayInfo.netstat.getServerStatus(),
                textAttributes,
                text_x, text_y + 2 * linespace)

            textAttributes[NSForegroundColorAttributeName] =
                errorOkColor(displayInfo.pt.accuracy <= ACCURACY_SHOW_THRESHOLD)
            drawText(context,
                "GPS accuracy: \(displayInfo.pt.accuracy)m",
                textAttributes,
                text_x, text_y + 3 * linespace)

            if (!status_ok) {
                last_error_display = now
            }
        }

        drawTargets(context, map_orientation: map_orientation, 
            status_ok: status_ok, cx: cx, cy: cy, size: size)
    }


    override func drawRect(canvas: CGRect) {
        // final long begin = AndroidUtil.systemTimeMs()
        drawRadar(canvas)
        // final long end   = AndroidUtil.systemTimeMs()
        // displayInfo.ci.drawing_time = (int)(end - begin)
        // Log.i(TAG, "Finished drawing, elapsed time: " + (end - begin) + "ms")
    }

    /*
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        Log.i(TAG, "onTouchEvent(): " + ev)
        if (   null != displayInfo 
            && !displayInfo.pmstat.gps_enabled
            && MotionEvent.ACTION_DOWN == ev.getActionMasked()) {
            // Bring up 'Location' settings
            AndroidUtil.settingsLocation(getContext())
            return true
        } else {
            return false
        }
    }
    */

}
