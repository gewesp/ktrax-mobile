//
// Data received from server
//

// Maximum number of targets
public let MAX_TARGETS = 5

struct ServerInfo {
    // GPS time [ms] for which data applies, echo of latest GPS time
    // of client -> server packet.
    // TODO: An original idea was to use that, why is it now gone?
    // public long gpstime = 0;

    // Total number of clients
    var n_clients = 0

    // Direction of 'up' arrow on the radar map [degree]
    var map_orientation = 0.0

    // Are we moving (yes, that's determined by the server)?
    var moving = 0

    // Number of elements in target array < MAX_TARGETS
    var n_targets = 0

    // Notification from server
    var notification = Notification.NOTIFICATION_NONE

    var target : [Alarm]

    init() {
        target = [Alarm](count: MAX_TARGETS, repeatedValue: Alarm())
    }
}
