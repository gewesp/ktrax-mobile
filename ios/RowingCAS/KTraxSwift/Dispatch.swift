//
//  Dispatch central
//
//  Created by Gerhard Wesp on 02/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

import Foundation

private let TAG = "Dispatch"

public let myQueue: dispatch_queue_t = 
    dispatch_queue_create("ch.kisstech.ktrax", DISPATCH_QUEUE_SERIAL)

var netMan : NetworkManager? = nil

// release queue when you are done with all the work
// dispatch_release(myQueue)



// http://stackoverflow.com/questions/24034544/dispatch-after-gcd-in-swift
private func runNetworkManagerOnceAndRedispatch() {
  if nil != netMan {
    netMan!.runOnce()
    let delayTime = dispatch_time(
        DISPATCH_TIME_NOW, 
        Int64(NETWORK_MANAGER_LOOP_INTERVAL * Double(NSEC_PER_SEC)))
    dispatch_after(delayTime, myQueue) { runNetworkManagerOnceAndRedispatch() }
  }
}



func dispatchPositionTime(pt: PositionTime) {
  dispatch_async(myQueue) {
    if nil != netMan {
      netMan!.setPositionTime(pt)
    }
  }
}

func dispatchTest() {
  dispatch_async(myQueue) {
    if nil != netMan {
      netMan!.sendTestPacket()
    }
  }
}

/*
func dispatchRadarUpdate(di: DisplayInfo) {
    dispatch_async(myQueue) {
        setRadarDisplayInfo(di)
    }
}
*/

func dispatchGpsEnabled() {
  dispatch_async(myQueue) {
    if nil != netMan {
      netMan!.setGpsEnabled(true)
    }
  }
}

func start() {
    if nil == netMan {
        NSLog("\(TAG): Creating and starting network manager")
        netMan = NetworkManager()
        runNetworkManagerOnceAndRedispatch()
        dispatchGpsEnabled()
        setStartStopButton(false)
    } else {
        NSLog("\(TAG): Network manager already running")
        setStartStopButton(false)
    }
}

func stop(inactiveText: String) {
    if nil != netMan {
        netMan!.shutdown()
        netMan = nil
        NSLog("\(TAG): Network manager stopped")
        setStartStopButton(true)
    } else {
        NSLog("\(TAG): Network manager: Was already stopped")
        setStartStopButton(true)
    }
    var di = DisplayInfo()
    di.active = false
    di.inactiveText = inactiveText
    dispatchRadarUpdate(di)
}

func startStop() -> Bool {
    if nil == netMan {
        start()
        return true
    } else {
        stop(NotificationStrings.INACTIVE_TEXT)
        return false
    }
}

func dispatchStop(inactiveText: String) {
    dispatch_async(myQueue) {
        stop(inactiveText)
    }
}
