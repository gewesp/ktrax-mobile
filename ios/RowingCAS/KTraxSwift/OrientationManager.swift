//
// Sensor power consumption:
// http://stackoverflow.com/questions/8752872/between-gps-compass-heading-and-accelerometer-which-consumes-the-most-amount-o
// Orders of magnituded quoted:
// * GPS: 4mA (Assuming it runs on 3.3V. I cannot find the datasheet)
// * Compass: 1.2mA
// * Accelerometer: 0.25mA
//

import CoreMotion


// Update interval [s]
let ORIENTATION_UPDATE_INTERVAL = 1.0

private let TAG = "OrientationManager"

var motMan:CMMotionManager? = nil


private func motionHandler(motion: CMDeviceMotion!, err: NSError!) {
    NSLog("\(TAG): Device motion callback, error: \(err)")
}

func orientationInit() {
    motMan = CMMotionManager()
    if nil != motMan {
        NSLog("\(TAG): Initialized")
        motMan!.deviceMotionUpdateInterval = ORIENTATION_UPDATE_INTERVAL
        motMan!.startDeviceMotionUpdates()
    } else {
        NSLog("\(TAG): Initializion failed")
    }

}

func orientationDeInit() {
    if nil != motMan {
        motMan!.stopDeviceMotionUpdates()
        motMan = nil
        NSLog("\(TAG): Stopped")
    } else {
        NSLog("\(TAG): Stopping, but wasn't initialized")
    }
}

func computeAzimut() -> Double {
    if nil == motMan {
        NSLog("\(TAG): No magnetometer available")
        return AZIMUT_NONE
    }
    let mot = motMan!.deviceMotion
    if nil == mot {
        NSLog("\(TAG): No orientation available")
        return AZIMUT_NONE
    }
    let ret = mot.attitude.yaw * 180.0 / M_PI
    NSLog("\(TAG): Heading \(ret)")
    return ret
}
