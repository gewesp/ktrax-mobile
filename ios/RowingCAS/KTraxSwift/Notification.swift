struct Notification {
    static let NOTIFICATION_NONE    = 0
    static let NOTIFICATION_ALARM_1 = 1
    static let NOTIFICATION_ALARM_2 = 2
    static let NOTIFICATION_ALARM_3 = 3
    static let NOTIFICATION_SOS     = 4

    static let NOTIFICATION_1_DIT   = 11
    static let NOTIFICATION_3_DIT   = 13 

    static let NOTIFICATION_OK      = 100
    static let NOTIFICATION_ERROR   = 101

    static let NOTIFICATION_OFF     = 200
}
