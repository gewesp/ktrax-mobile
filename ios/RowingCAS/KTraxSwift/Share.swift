//
//  Share.swift
//
//  Created by Gerhard Wesp on 02/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

import UIKit

private let TAG = "Share"



class myItem: NSObject, UIActivityItemSource {
    var message = ""
    var url     = ""
    var subject = ""

    func activityViewControllerPlaceholderItem(
        activityViewController: UIActivityViewController) -> AnyObject {
        return message;
    }

    func activityViewController(
        activityViewController: UIActivityViewController, 
        itemForActivityType activityType: String) -> AnyObject? {
        return message + ": " + url
    }

    // Subject: Only for email sharing (?)
    func activityViewController(
        activityViewController: UIActivityViewController, 
        subjectForActivityType activityType: String?) -> String {
        if(activityType == UIActivityTypeMail){
            return subject
        } else {
            return ""
        }
    }
}


func presentTrackingDialog(ctl: UIViewController) {
    let excluded = [ UIActivityTypeAirDrop, UIActivityTypeAddToReadingList ]

    var item = myItem()
    item.subject = "\(Config.APPLICATION_NAME) tracking"
    item.message = "My current location on \(Config.APPLICATION_NAME)"
    item.url     = "http://rowingcas.kisstech.ch/tracking/?ktraxid=\(getAppUUID())"

    // let objects = [text, url]

    let avc = UIActivityViewController(
    //     activityItems: objects,
           activityItems: [item],
    
        applicationActivities: nil)

    avc.excludedActivityTypes = excluded
        
    ctl.presentViewController(avc, animated: true, completion: nil)
}
