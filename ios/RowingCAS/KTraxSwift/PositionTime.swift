//
// Position/time
//
// TODO: *Possibly* it's better to add a copy constructor in
// PositionTime and get rid of the location member in
// NetworkManager---it's a huge memory hog.  Ridiculous.
//
// TODO: Vertical speed!!
// Use baro?  See 
// https://play.google.com/store/apps/details?id=com.igorinov.variometer&hl=en
//

import CoreLocation

public let DEFAULT_ACCURACY:Float = 5000.0
public let AZIMUT_NONE = 1800.0

struct PositionTime {

    // static let DEFAULT_ACCURACY:Float = 5000.0

    // System time [ms] for this fix.
    var fix_systime:Int64 = -1

    // GPS time [ms] for this fix
    var gpstime    :Int64 = -1

    // Accuracy [m]
    var accuracy:Float = DEFAULT_ACCURACY

    // Lat/lon/alt
    var latitude :Double = 0.0
    var longitude:Double = 0.0
    var altitude :Float  = 0.0

    // Speed [m/s], course [degrees]
    var speed :Float =    0.0
    var course:Float = 1800.0

    // Vertical speed [m/s], UP = positive
    var vertical_speed:Float = 0.0

    init() {
    }

    init(loc:CLLocation) {
        self.fix_systime = iOSUtil.systemTimeMs()
        self.gpstime     = Int64(1000.0 * loc.timestamp.timeIntervalSince1970 + 0.5)
        self.accuracy    = Float(loc.horizontalAccuracy >= 0.0 ?
                                 loc.horizontalAccuracy : DEFAULT_ACCURACY)

        self.latitude  = loc.coordinate.latitude
        self.longitude = loc.coordinate.longitude
        self.altitude  = Float(loc.altitude)

        // Don't trust course if speed == -1 (i.e., invalid)
        if (loc.speed >= 0) {
          self.speed     = Float(loc.speed)

          // "Course values may not be available on all devices. A negative 
          // value indicates that the direction is invalid."
          self.course    = Float(loc.course >= 0 ? loc.course : 1800.0)
        } else {
          self.speed  = 0.0
          self.course = 1800.0
        }

        // TBD, may be calculated on server
        self.vertical_speed = 0.0
        if loc.horizontalAccuracy < 0 {
            self.fix_systime = -1
            self.gpstime     = -1
        }
    }


}
