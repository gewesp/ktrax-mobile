//
// Network management functions
//
// TODO:
// * ...
//

import Foundation
import CoreFoundation

public func getAppUUID() -> String {
    return iOSUtil.getStableUUID(Config.APPLICATION_NAME)
}

// Loop interval [s]
public let NETWORK_MANAGER_LOOP_INTERVAL = 0.099

    private let HEADER_FIELDS = 7
    private let FIELDS_PER_TARGET = 15
    
    private let TAG = "NetworkManager"
    private let SERVER = "ktrax.kisstech.ch"

    // String identifying packet types
    private let UPDATE_PACKET   = "ktrax_update"
    private let POSITION_PACKET = "ktrax_pos"
    private let HELLO_PACKET    = "ktrax_hello"
    private let BYE_PACKET      = "ktrax_bye"
    private let TOAST_PACKET    = "ktrax_toast"
    private let URL_PACKET      = "ktrax_url"

    // Keepalive sounds (OK: dit, ERROR: dit dit dit)
    private let KEEPALIVE_FIRST_MS:Int64 = 5000
    private let KEEPALIVE_PERIOD_MS = 30000
    private let KEEPALIVE_INTERVAL_MS = 1000

    // Interval to send packets if no GPS data is received [ms]
    private let PACKET_SEND_INTERVAL = 1000

    // Interval to send hello packets.
    // In addition, they are sent on certain status changes.
    private let HELLO_SEND_INTERVAL = 1800 * 1000

    // Send timeout [s].  Not sure whether this has any meaning
    // (UDP)
    private let SEND_TIMEOUT = 0.1

    // Receive timeout [ms].  Applicable to RTT and also in the case
    // no packet comes over at all.
    private let RECEIVE_TIMEOUT = 4000

    // Longest interval to update UI.  Reception of OK packets
    // always posts an immediate update.
    private let UPDATE_UI_INTERVAL = 1000

    // Show status indications 30s after pressing TEST
    private let TEST_STATUS_DISPLAY_TIME_MS = 30000

    // We consider our position invalid if we haven't
    // received an update in 4 seconds.
    private let GPS_TIMEOUT = 4000

    // Retry timeout for DNS lookup [ms]
    private let RESOLVE_RETRY_TIME = 1000

private func createAndSetupSocket() -> CFSocket! {
    NSLog("\(TAG): Creating socket")
    let sock = createUDPSocket()

    // The receive timeout is very small, we sleep outside in the
    // dispatch framework.
    setReceiveTimeout(sock, 0.001)
    swiftSetupSocket(sock)
    return sock
}

class NetworkManager {
    // Last time we tried to resolve...
    var last_resolve_time:Int64 = -10000000

    // System time of send, for latency calculation and send triggering.
    var last_send_systime:Int64 = -10000000

    // System time of last hello packet
    var last_hello_systime:Int64 = -10000000

    // System time of last call to updateUI()
    var last_update_systime:Int64 = -10000000

    // System time of last *update* packet received
    var last_receive_systime:Int64 = -10000000

    // Last time a test was requested.
    var last_test_systime:Int64 = -10000000

    // init() called here
    var startup_systime:Int64 = 0

    // Complete info for Radar screen and audio alerts
    var displayInfo = DisplayInfo()

    // Info received from server
    var server_info = ServerInfo()

    // Info collected on client
    var client_info = ClientInfo()

    // Last p/t sent
    var last_position_time = PositionTime()

    // Current p/t
    var pt = PositionTime()

    // Whether or not we have a valid position (or location update timeout).
    var position_valid: Bool = false

    var last_location_systime:Int64 = -10000000

    // Sequence number for POSITION packets, increased for each packet sent.
    var sequence_number = 0

    // Sounds
    // private final AlertUIManager alert_manager

    // Own and position manager status
    var status    =  NetworkManagerStatus()
    var status_pm = PositionManagerStatus()

    var unique_id = "ios:1234567"

    // Network infrastructure
    var maybeAddress: CFData? = nil
    var sock: CFSocket!

    // Submanagers
    var locMan: MyLocationManager?
    
    // URL codes already shown
    var urls = [Int:Bool]()

    init() {
        let now_systime = iOSUtil.systemTimeMs()

        position_valid = false
        last_send_systime = -100000000
        last_hello_systime = -100000000
        last_update_systime = -100000000
        // Pretend everything is OK
        last_receive_systime = now_systime
        last_test_systime = -100000000
        // First error indication 30 seconds after startup.
        startup_systime = now_systime
        sequence_number = 0

        unique_id = getAppUUID()
        
        sock = createAndSetupSocket()
        NSLog("\(TAG): UDP port: \(config_UDP_PORT())")

        locMan = MyLocationManager()
        if nil == locMan {
            NSLog("\(TAG): WTF: Nil location manager")
        }

        status.setAllOk()
        NSLog("\(TAG): ID: \(getUniqueId())")

        NSNotificationCenter.defaultCenter().addObserverForName(
            NSUserDefaultsDidChangeNotification, 
            object: nil, queue: NSOperationQueue.mainQueue()) { _ in
                NSLog("\(TAG): Defaults were changed")
            }
        iOSUtil.startBatteryUpdates()
        soundPlay(Notification.NOTIFICATION_OK)
    }

    func shutdown() {
        // sock.close()
        NSLog("\(TAG): Shutting down")
        send_bye()
        soundPlay(Notification.NOTIFICATION_OFF)
        iOSUtil.stopBatteryUpdates()
        if nil != locMan {
            locMan!.shutdown()
        } else {
            NSLog("\(TAG): WTF: Nil location manager in shutdown")
        }
    }


    func setPositionTime(new_pt: PositionTime) {
        pt = new_pt
        last_location_systime = iOSUtil.systemTimeMs()
        status_pm.setInitialized(true)
    }

    // Public API
    func getUniqueId() -> String {
        return unique_id
    }

    func sendTestPacket() {
        last_test_systime = iOSUtil.systemTimeMs()
    }

    func resolve() {

            maybeAddress = resolveHostPort(SERVER, config_UDP_PORT())
            if nil != maybeAddress {
                status.setAllOk()
            } else {
                status.setAddressError()
            }

    }

    // Should be called @ ~1Hz (min 0.9Hz?)
    // Prepare all structures and call activity's and sound manager's UI 
    // update.

    func updateUI(now_systime:Int64) {
        last_update_systime = now_systime
        updateGpsStatus(now_systime)
        logAllOk()

        // Notification from server takes precedence.
        client_info.notification = server_info.notification

        if (Notification.NOTIFICATION_NONE == client_info.notification) {
            let runtime = now_systime - startup_systime
            if (runtime + KEEPALIVE_PERIOD_MS - KEEPALIVE_FIRST_MS)
                % KEEPALIVE_PERIOD_MS < KEEPALIVE_INTERVAL_MS {
                client_info.notification = displayInfo.allOk() ?
                      Notification.NOTIFICATION_1_DIT
                    : Notification.NOTIFICATION_3_DIT
                NSLog("%@: Client-generated notification: %d", TAG, 
                      client_info.notification)
            }
        }

        status.setDisplayStatus(
            now_systime <= last_test_systime + TEST_STATUS_DISPLAY_TIME_MS)

        displayInfo.pt = pt
        displayInfo.si = server_info
        displayInfo.ci = client_info
        displayInfo.netstat = self.status
        displayInfo.pmstat = status_pm

        // TBD: may also write back into displayInfo, e.g. the 
        // ci.drawing_time for statistics.
        // -> separate into two classes
        dispatchRadarUpdate(displayInfo)

    }

    func postUIUpdate() {
        last_update_systime = -100000000
    }

    func runOnce() {
            if (nil == maybeAddress) {
                let now_systime = iOSUtil.systemTimeMs()

                if (last_resolve_time + RESOLVE_RETRY_TIME <= now_systime) {
                    last_resolve_time = now_systime
                    resolve()
                }

                // Send hello next time we can
                last_hello_systime = -100000000
            }

            if (nil != maybeAddress) {
                send()
            } else {
                status.setConnectionError()
            }

            let now_systime = iOSUtil.systemTimeMs()

            if (nil != maybeAddress && last_hello_systime + HELLO_SEND_INTERVAL <= now_systime) {
                send_hello(now_systime)
            }

            if (nil != maybeAddress) {
                receive(now_systime)

                // Receive posts a UI update and clears errors
                // on successful reception of an update packet.
                // Here we handle only the timeout case.
                if (now_systime > last_receive_systime + RECEIVE_TIMEOUT) {
                    status.setTimeoutError()
                }
            }

            if (now_systime >= last_update_systime + UPDATE_UI_INTERVAL) {
                updateUI(now_systime)
            }

            soundPlay(client_info.notification)
    }

    // Should be called before UI update, once per second.
    func updateGpsStatus(now_systime:Int64) {
        position_valid = now_systime <= last_location_systime + GPS_TIMEOUT
        status_pm.setPositionValid(position_valid)
    }

    // Forwarding functions to set GPS status
    func setGpsEnabled(b:Bool) {
        status_pm.setEnabled(b)
    }
    func setGpsInitialized(b:Bool) {
        status_pm.setInitialized(b)
    }

    // Logs a message if status is NOT OK
    func logAllOk() {
        if !displayInfo.allOk() {
            NSLog("%@: Status: GPS: %@, NET: %@, SRV: %@",
                         TAG,
                         displayInfo.pmstat.getStatusString(),
                         displayInfo.netstat.getNetworkStatus(),
                         displayInfo.netstat.getServerStatus())
        }
    }

    // Get client info parameters as far as they are system params
    func updateClientInfo(pt: PositionTime) {
        // client_info.network_type = (short) telMan.getNetworkType()
        // client_info.cell_id      = AndroidUtil.getCellId(telMan)
        // client_info.roaming      = (short) (telMan.isNetworkRoaming() ? 1 : 0)

        // client_info.battery_level_percent = 
        //     PersistentLogic.getInstance().battery_level_percent

        client_info.course = pt.course
        // Log.d(TAG, "Heading: " + client_info.heading)
    }

    func vehicleTypeAndFlags() -> Int {
        // http://www.codingexplorer.com/nsuserdefaults-a-swift-introduction/
        // On iOS 7.x, for some reason the tracking flag doesn't appear
        // until the Settings page is first accessed by the user.
        // Assume it's true until such time.
        let def = NSUserDefaults.standardUserDefaults()
        if def.objectForKey("enable_tracking") == nil {
            NSLog("\(TAG): WTF: No tracking setting?!")
            return 101 | 0x100
        }

        let enable_tracking = def.boolForKey("enable_tracking")
        // NSLog("\(TAG): Tracking: \(enable_tracking)")
        return 101 | (enable_tracking ? 0x100 : 0x0)
    }

    func send() {

        // We send if 
        // (a) A new position is available or 
        // (b) More than 1s has elapsed since the last send.
        
        let now_systime = iOSUtil.systemTimeMs()
        let this_gpstime = pt.gpstime

        if (   (this_gpstime > 0 && this_gpstime > last_position_time.gpstime)
            || now_systime > last_send_systime + PACKET_SEND_INTERVAL) {

            updateClientInfo(pt)

            // NSLog("%@: Sending, now_systime: \(now_systime)", TAG)

            // Set gpstime to -1 if we don't have a valid position.

            // Heap analysis shows that StringBuilder is being used here
            // and the code is relatively memory-friendly.
            // TODO: Use String.format() and sensible accuracies
            let s =    "\(POSITION_PACKET) "
                     + "\(getUniqueId()) "
                     + "\(sequence_number) "
                     + "\(now_systime) "
                     + "\(client_info.packets_received) "
                     + "\(client_info.packet_loss_percent) "
                     + "\(client_info.rtt_latency) "
                     + "\(client_info.drawing_time) "
                     + "\(client_info.network_type) "
                     + "\(iOSUtil.getCellID()) "
                     + "\(client_info.roaming) "
                     + "\(iOSUtil.getBatteryLevel()) "
                     + "\(getHeading()) "

                     + "\(pt.fix_systime) "

                     + "\(vehicleTypeAndFlags()) "

                     + "\(position_valid ? pt.gpstime : -1) "
                     + "\(pt.latitude) " 
                     + "\(pt.longitude) "
                     + "\(pt.altitude) "
                    
                     + "\(pt.speed) "
                     + "\(pt.course) "
                     + "\(pt.vertical_speed) "

                     + "\(pt.accuracy) "

                     + "\(last_test_systime) "

            // NSLog("\(TAG): Sending update packet")

            if (!sendString(sock, maybeAddress!, s, SEND_TIMEOUT)) {
                status.setSendError()
                // iOS hack: The system tends to destroy sockets under
                // our arse, so we use this hack to defend ourselves... 
                sock = createAndSetupSocket()
                maybeAddress = nil
            } else {
                // Managed to send off the packet, update
                // the respective data.
                last_position_time = pt
                last_send_systime = now_systime
                ++sequence_number
            }
        }
    }

    func send_bye() {
        let s = BYE_PACKET + " "
              + getUniqueId() + " "
              + "\(iOSUtil.systemTimeMs())"

        if nil != maybeAddress {
            NSLog("%@: Sending bye packet", TAG)

            if (!sendString(sock, maybeAddress!, s, SEND_TIMEOUT)) {
                status.setSendError()
                maybeAddress = nil
            }
        } else {
            NSLog("%@: Not connected, not sending bye packet", TAG)
        }
    }

    func send_hello(now_systime:Int64) {
        // TODO: Numeric app version?
        let s = "\(HELLO_PACKET) \(getUniqueId()) \(iOSUtil.systemTimeMs()) \(iOSUtil.getAppVersion()) \(iOSUtil.getDeviceModel()) \(iOSUtil.getSystemVersion()) \(iOSUtil.getNetworkOperatorName()) \(iOSUtil.getNetworkCountryIso())"

        // Log.i(TAG, "Sending hello")
        if (!sendString(sock, maybeAddress!, s, SEND_TIMEOUT)) {
            status.setSendError()
            maybeAddress = nil
        } else {
            last_hello_systime = now_systime
        }
    }

    // Reads string, returns after receive timeout
    func receive(now_systime:Int64) {
        let msg = receiveString(sock)

        if msg.isEmpty {
            // Timeout, nothing to do (we'll come back in 100ms)
            return
        }

        // http://stackoverflow.com/questions/25678373/swift-split-a-string-into-an-array
        let ss = split(msg, allowEmptySlices: false, isSeparator: {" " == $0})


        if (ss.count < 1) {
            NSLog("%@: Bad packet format: %d field(s)", TAG, ss.count)
            status.setParseError()
            return
        }

        if (UPDATE_PACKET == ss[0]) {
            if (ss.count < HEADER_FIELDS) {
                NSLog("%@: Bad packet format: %d field(s)", TAG, ss.count)
                status.setParseError()
                return
            }
            var newinfo = ServerInfo()

            // System time of the packet the server is replying to
            // with this packet.
            let sent_systime        = (ss[1] as NSString).longLongValue
            newinfo.n_clients       = (ss[2] as NSString).integerValue
            newinfo.map_orientation = (ss[3] as NSString).doubleValue
            newinfo.moving          = (ss[4] as NSString).integerValue
            newinfo.notification    = (ss[5] as NSString).integerValue
            newinfo.n_targets       = (ss[6] as NSString).integerValue

            if (HEADER_FIELDS + newinfo.n_targets * FIELDS_PER_TARGET != ss.count) {
                NSLog("%@: Bad packet format: %d field(s)", TAG, ss.count)
                status.setParseError()
                return
            }
            if (newinfo.n_targets > MAX_TARGETS) {
                NSLog("%@: Too many targets %d", TAG, newinfo.n_targets)
                status.setParseError()
                return 
            }

            // Log.d(TAG, "# targets: " + newinfo.n_targets)
            for (var i = 0; i < newinfo.n_targets; ++i) {
                let idx = HEADER_FIELDS + i * FIELDS_PER_TARGET
                // assertTrue(idx + FIELDS_PER_TARGET - 1 < ss.count)
                // assertNotNull(newinfo.target[i])

                // ss[idx] is the marker A0, A1 etc.
                newinfo.target[i].gpstime            = (ss[idx + 1 ] as NSString).longLongValue
                newinfo.target[i].target             =  ss[idx + 2 ]
                newinfo.target[i].vehicle_type       = (ss[idx + 3 ] as NSString).integerValue
                newinfo.target[i].distance           = (ss[idx + 4 ] as NSString).floatValue
                newinfo.target[i].bearing            = (ss[idx + 5 ] as NSString).floatValue
                newinfo.target[i].relative_altitude  = (ss[idx + 6 ] as NSString).floatValue
                newinfo.target[i].speed              = (ss[idx + 7 ] as NSString).floatValue
                newinfo.target[i].display_course_deg = (ss[idx + 8 ] as NSString).floatValue
                newinfo.target[i].vertical_speed     = (ss[idx + 9 ] as NSString).floatValue
                newinfo.target[i].heading            = (ss[idx + 10] as NSString).floatValue

                newinfo.target[i].tCPA               = (ss[idx + 11] as NSString).floatValue
                newinfo.target[i].dCPA               = (ss[idx + 12] as NSString).floatValue
                newinfo.target[i].danger_level       = (ss[idx + 13] as NSString).floatValue
                newinfo.target[i].notification       = (ss[idx + 14] as NSString).integerValue
            }

            // Packet is OK, but may still be too old
            last_receive_systime = now_systime
            ++client_info.packets_received
            client_info.packet_loss_percent = 
                100.0 * (1.0 -  Double(client_info.packets_received)
                              / Double(sequence_number))

            // Update latency.
            if (sent_systime <= now_systime) {
                client_info.rtt_latency = Int(now_systime - sent_systime)
                if (client_info.rtt_latency < RECEIVE_TIMEOUT) {
                    server_info = newinfo
                    status.setAllOk()
                    postUIUpdate()
                }
                // Else the packet is too old, discard it.
                // Timeout errors handled by caller.
            } else {
                // No user-visible error, not observed in practice
                NSLog("%@: WTF packet out of the future?!", TAG)
            }

        } else if (BYE_PACKET == ss[0]) {
            // ktrax_bye <code> <msg>

            if (ss.count < 3) {
                NSLog("%@: Bad bye packet format: %d field(s)", TAG, ss.count)
                status.setParseError()
                return
            }

            NSLog("\(TAG): Shutdown command from server")
            dispatchStop("\(NotificationStrings.INACTIVE_TEXT): \(ss[2])")
        } else if (URL_PACKET == ss[0]) {
            // TODO: Show URL
            // ktrax_url <code> <url>

            if (ss.count < 3) {
                NSLog("%@: Bad url packet format: %d field(s)", TAG, ss.count)
                status.setParseError()
                return
            }

            let code = (ss[1] as NSString).integerValue
            let url  =  ss[2]

            if nil == urls[code] {
                urls[code] = true
                iOSUtil.openUrl(url)
            }
        } else if (TOAST_PACKET == ss[0]) {
            // Not implemented, probably doesn't make sense
            // ktrax_toast <code> <toast>

        } else {
            NSLog("%@: Unknown packet type: %@", TAG, ss[0])
            status.setParseError()
        }
    }
}
