import Foundation
import UIKit
import CoreTelephony

private let TAG = "iOSUtil"

private var telInfo: CTTelephonyNetworkInfo? = nil

struct iOSUtil {
    static func systemTimeMs() -> Int64 {
        return Int64(1000.0 * NSProcessInfo.processInfo().systemUptime + 0.5)
    }

    // Returns: A UUID (hopefully) with 21 characters
    static func createUUID() -> String {
        let str = "ios:" 
                + (CFUUIDCreateString(nil, CFUUIDCreate(nil)) as String)
        return str.substringToIndex(advance(str.startIndex, 21))
    }

    // Returns: A stable UUID (hopefully) with 21 characters.  Creates
    // it if not yet present.
    static func getStableUUID(appName: String) -> String {
        let def = NSUserDefaults.standardUserDefaults()
        let defaultKey = "uuid:" + appName
        // We have one, return it
        if let str = def.stringForKey(defaultKey) {
            return str
        } else {
            let str = createUUID()
            def.setObject(str, forKey: defaultKey)
            def.synchronize()
            NSLog("\(TAG): Created and stored UUID: \(str)")
            return str
        }
    }

    // Opens a browser with the URL
    static func openUrl(s: String) {
        if let url = NSURL(string: s) {
            NSLog("\(TAG): Opening URL: \(s)")
            UIApplication.sharedApplication().openURL(url)
        } else {
            NSLog("\(TAG): Couldn't convert \(s) to a URL")
        }
    }


    static func startBatteryUpdates() {
        UIDevice.currentDevice().batteryMonitoringEnabled = true
    }

    static func stopBatteryUpdates() {
        UIDevice.currentDevice().batteryMonitoringEnabled = false
    }

    static func getBatteryLevel() -> Float {
        let lvl = UIDevice.currentDevice().batteryLevel
        if lvl < 0.0 {
            return 100.0
        } else {
            return 100.0 * lvl
        }
    }

    // Get application version
    // http://stackoverflow.com/questions/25965239/how-do-i-get-the-app-version-and-build-number-using-swift
    static func getAppVersion() -> String {        
        if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        } else {
            return "(unknown version)"
        }
    }

    // Get device model: iPhone, iPad, iPod touch, etc.
    static func getDeviceModel() -> String {
        return UIDevice.currentDevice().model
    }

    // Get Operating System version, e.g. 7.1.2
    static func getSystemVersion() -> String {
        return UIDevice.currentDevice().systemVersion
    }

    static func getCTCarrier() -> CTCarrier! {
        if nil == telInfo {
            telInfo = CTTelephonyNetworkInfo()
        }
        if nil == telInfo {
            NSLog("\(TAG): WTF: Nil telInfo")
            return CTCarrier()
        }
        
        if let ret = telInfo!.subscriberCellularProvider {
            return ret
        } else {
            NSLog("\(TAG): WTF: Nil subscriberCellularProvider")
            return CTCarrier()
        }
    }

    // Get telephony network carrier name, e.g. Swisscom, Sunrise etc.
    static func getNetworkOperatorName() -> String {
        if let cn = getCTCarrier().carrierName {
            return cn
        } else {
            return "(unknown carrier)"
        }
    }

    // Get ISO country code of current network
    static func getNetworkCountryIso() -> String {
        if let cc = getCTCarrier().isoCountryCode {
            return cc
        } else {
            return "(unknown country)"
        }
    }

    // Cell ID apparently not provided on iOS, cf.
    // http://stackoverflow.com/questions/4567708/getting-cell-tower-information-in-an-ios-application
    static func getCellID() -> Int {
        return 4711
    }

}
