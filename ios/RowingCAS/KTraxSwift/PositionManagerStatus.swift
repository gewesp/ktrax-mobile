// TODO: Update java to this semantics (java currently has a 
// updateStatusString function..?!)
struct PositionManagerStatus {
    var gps_enabled = false
    var initialized = false
    var position_valid = false

    func ok() -> Bool {
      return gps_enabled && initialized && position_valid
    }

    mutating func setEnabled      (e:Bool)   { gps_enabled = e }
    mutating func setInitialized  (i:Bool)   { initialized = i }
    mutating func setPositionValid(v:Bool) { position_valid = v }

    // Should be called once per second, before the string is used.
    func getStatusString() -> String {
        if (!gps_enabled) {
            return NotificationStrings.GPS_DISABLED
        }
        if (!initialized) {
            return NotificationStrings.GPS_OFF
        }

        return position_valid ? NotificationStrings.GPS_3D
                              : NotificationStrings.GPS_ACQUIRING
    }
}
