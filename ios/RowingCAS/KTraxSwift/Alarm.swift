//
// Alarm structure.
// See server alarm.h
//

struct Alarm {
    // Time of validity for this alarm, [ms] since epoch
    var gpstime:Int64 = -1

    // ID of target
    var target = ""

    // Vehicle type of target
    var vehicle_type = 101

    // Danger level
    var danger_level:Float = 0.0

    // Notification/alarm level
    var notification = 0

    // Distance [m]
    var distance:Float = 0.0

    // Absolute bearing [degrees] wrt. N
    var bearing:Float = 1800.0

    // Relative altitude [m]
    var relative_altitude:Float = 0.0

    // Target speed
    var speed:Float = 0.0

    // Target course over ground [degree], for display purposes
    var display_course_deg:Float = 1800.0

    // Target vertical speed [m/s]
    var vertical_speed:Float = 0.0

    // Target heading, if available
    var heading:Float = 1800.0

    // Time to CPA, relative to gpstime [s]
    var tCPA:Float = 0.0

    // Distance at CPA [m]
    var dCPA:Float = 0.0
}
