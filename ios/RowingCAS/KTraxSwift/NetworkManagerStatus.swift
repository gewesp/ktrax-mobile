//
// Network status
//

struct NetworkManagerStatus {
    // 'Network' errors
    var error_initialize = false
    var error_address    = false
    var error_send       = false

    // 'Server' errors
    var error_connection = false
    var error_timeout    = false
    var error_parse      = false
    
    // Non-errors: Display status info
    var display_status   = false

    mutating func setInitializationError() { error_initialize = true }
    mutating func setAddressError       () { error_address    = true }
    mutating func setSendError          () { error_send       = true }

    mutating func setConnectionError    () { error_connection = true }
    mutating func setTimeoutError       () { error_timeout    = true }
    mutating func setParseError         () { error_parse      = true }

    mutating func setDisplayStatus(enable: Bool) {
        display_status = enable
    }

    mutating func setAllOk() {
        error_initialize = false
        error_address    = false
        error_send       = false

        error_connection = false
        error_timeout    = false
        error_parse      = false

        display_status   = false
    }

    func networkOk() -> Bool {
        return !(error_initialize || error_address || error_send)
    }

    func serverOk() -> Bool {
        return !(error_connection || error_timeout || error_parse)
    }

    func ok() -> Bool { return networkOk() && serverOk() }

    func getNetworkStatus() -> String { 
        if (error_initialize) {
            return NotificationStrings.ERROR_INIT
        } 
        if (error_address   ) {
            return NotificationStrings.ERROR_ADDRESS
        } 
        if (error_send      ) {
            return NotificationStrings.ERROR_SEND
        } 
        return NotificationStrings.STRING_OK
    }

    func getServerStatus() -> String {
        if (error_connection) {
            return NotificationStrings.ERROR_CONNECTION
        }
        if (error_timeout) {
            return NotificationStrings.ERROR_TIMEOUT
        }
        if (error_parse) {
            return NotificationStrings.ERROR_PARSE
        }
        return NotificationStrings.STRING_OK
    }

    func displayStatus() -> Bool {
        return display_status
    }
}
