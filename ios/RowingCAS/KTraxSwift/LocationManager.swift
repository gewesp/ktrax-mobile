//
//  LocationManager.swift
//
//  Created by Gerhard Wesp on 02/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

import CoreLocation

private let TAG = "LocationManager"


// Minimum change of heading to deliver an update [degree]
private let HEADING_RESOLUTION = 15.0

private var heading = 1800.0

public func getHeading() -> Double {
    return heading
}


class MyLocationManager: NSObject, CLLocationManagerDelegate {

    var manager: CLLocationManager?

    //////////////////////////////////////////////////////////////////////// 
    // CLLocationManager interface
    //////////////////////////////////////////////////////////////////////// 

    func locationManager(manager: CLLocationManager!, 
                         didUpdateHeading: CLHeading!) {
        let hdg = didUpdateHeading.magneticHeading
        let acc = didUpdateHeading.headingAccuracy

        // NSLog("\(TAG): Heading: \(hdg), accuracy: \(acc)")

        if acc > 0 {
            heading = hdg
        } else {
            NSLog("\(TAG): Heading unreliable, setting default")
            heading = 1800.0
        }
    }

    func locationManager(manager: CLLocationManager!, didUpdateLocations: [AnyObject]!) {
        if 0 == didUpdateLocations.count {
            NSLog("\(TAG): WTF: Location update called without location")
            return
        }
        // Use last element, just in case...
        let location = didUpdateLocations[didUpdateLocations.count - 1] as! CLLocation
        let pt = PositionTime(loc: location)
        // For some reason, %ld doesn't seem to work with Int64...'
        // NSLog("%@: Location: %f/%f @ %11.3f", TAG, pt.latitude, pt.longitude, Double(pt.gpstime) * 1e-3)
        dispatchPositionTime(pt)
    }

    func locationManager(manager: CLLocationManager!, didFailWithError: NSError!) {
        let err = "\(didFailWithError)"
        NSLog("%@: Error %@", TAG, err)
    }

    // Initialize the location manager and start updates.
    override init() {
        super.init()
        
        NSLog("\(TAG): Initializing")
        manager = CLLocationManager()

        if nil == manager {
            NSLog("\(TAG): Couldn't initialize CLLocationManager")
            return
        }

        manager!.delegate = self;
        // http://stackoverflow.com/questions/3411629/decoding-the-cllocationaccuracy-consts
        // kCLLocationAccuracy:
        // * kCLLocationAccuracyBest
        //   GPS only, no sensor fusion
        // * kCLLocationAccuracyBestForNavigation:
        //   GPS + sensor fusion at the expense of higher battery drain
        manager!.desiredAccuracy = kCLLocationAccuracyBest

        // Asynchronous call, implement
        // https://developer.apple.com/library/ios/documentation/CoreLocation/Reference/CLLocationManagerDelegate_Protocol/index.html#//apple_ref/occ/intfm/CLLocationManagerDelegate/locationManager:didChangeAuthorizationStatus:
        // More info on Location services on iPhone:
        // http://nevan.net/2014/09/core-location-manager-changes-in-ios-8/


        // Special case: iOS 7 doesn't have this method, so we must 
        // wrap it---hope the authorization is automatically asked
        // in iOS 7
        // http://stackoverflow.com/questions/24562739/swift-app-crashes-with-unrecognized-selector-sent-to-instance-on-cllocationman
        if manager!.respondsToSelector(
            Selector("requestAlwaysAuthorization")) {
            manager!.requestAlwaysAuthorization()
        }
        manager!.startUpdatingLocation()

        manager!.headingFilter = HEADING_RESOLUTION
        manager!.startUpdatingHeading()
    }

    func shutdown() {
        NSLog("\(TAG): Shutting down")
        if nil == manager {
            NSLog("\(TAG): WTF: Nil location manager in delegate shutdown")
            return
        }

        manager!.stopUpdatingHeading()
        manager!.stopUpdatingLocation()
        manager = nil
    }

}
