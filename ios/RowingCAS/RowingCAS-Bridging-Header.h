//
//  RowingCAS-Bridging-Header.h
//  RowingCAS
//
//  Created by Gerhard Wesp on 11/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

//
// All C APIs that should be exported to Swift need to be included here
// Swift seems to support only one such 'bridging header' per project.
//
// https://developer.apple.com/library/ios/documentation/Swift/Conceptual/BuildingCocoaApps/MixandMatch.html#//apple_ref/doc/uid/TP40014216-CH10-XID_78
//
// Project build settings are available when clicking on the project
// root in the file explorer.
//

#ifndef RowingCAS_RowingCAS_Bridging_Header_h
#define RowingCAS_RowingCAS_Bridging_Header_h

#include "cfuncs.h"

#endif
