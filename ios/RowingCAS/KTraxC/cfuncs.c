//
//  cfuncs.c
//  RowingCAS
//
//  Created by Gerhard Wesp on 10/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

#include "cfuncs.h"

#include <math.h>

#include <sys/time.h>
#include <sys/socket.h>

int disableSigPipe(int fd) {
  int enable = 1;
  return setsockopt(fd, SOL_SOCKET, SO_NOSIGPIPE, &enable, sizeof(enable));
}

int setSocketTimeout(int fd, double timeout) {
    
    struct timeval tval;
    tval.tv_sec = (long)(timeout);
    tval.tv_usec = (int)(1e6 * fmod(timeout, 1.0));
    
    if (tval.tv_usec >= 1000000) {
        tval.tv_usec = 999999;
    }
    
    return setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tval, sizeof(tval));
}
