//
//  cfuncs.h
//  RowingCAS
//
//  Created by Gerhard Wesp on 10/04/15.
//  Copyright (c) 2015 ___KISS_TECHNOLOGIES___. All rights reserved.
//

#ifndef __RowingCAS__cfuncs__
#define __RowingCAS__cfuncs__

//
// Tried hard to implement those in Swift and failed.  Hence in C.
//
// Use CInt and CDouble in Swift calls.
//

//
// Set NO_SIGPIPE option.  If we don't do this, on iOS 7.1, we get killed 
// on send after waking up from sleep mode.  Apparently, this is due
// to iOS closing sockets for our app.
//

int disableSigPipe(int fd);

//
// Set timeout
//

int setSocketTimeout(int fd, double timeout);

#endif /* defined(__RowingCAS__cfuncs__) */
