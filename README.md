# KTrax Android and iOS clients

(C) 2016 and onwards, KISS Technologies GmbH, Zuerich

Project moved from github as of 10/2019.  Issues are still on 
[github](https://github.com/gewesp/ktrax-mobile/issues).

# Build

## Android Studio (AS) 4.1.3

* Open AS
* 'Open Existing Project'
* Select `android/ktrax/build.gradle`
* Pray

### Troubleshooting

#### Missing SDKs

Errors like `Module: 'app' platform 'android-xx' not found` can mean that
the Android SDK 'API level' xx is not installed.  This can be done in the
preferences and may require a restart/reload of the project afterwards.

#### Errors about DTDs / schemas in AndroidManifest files

If you get any of these, it may be 
* Completely unrelated and instead refer to a missing resource
* You could fix it by adding `http://schemas.android.com/apk/res/android` to
  'Ignored Schemas and DTDs' under Preferences -> Schemas and DTDs.  D'oh,
  who would've guessed.


# Manifest

* `android/ktrax`
  - Build Variants ("flavors"): `ktrax_android` and `rowingcas`.  `powercas` TBD
  - See android/Ktrax/app/build.gradle
* iOS: RowingCAS app.  iCAS to be renamed to KTrax.

# Logos

* See `logo/*.svg`
* `rowingcas.svg` contains layers for the RowingCAS *and* the PowerCAS log
* Use `make-res.sh`, `make-ios.sh` to regenerate PNGs.  This requires inkscape
  and gimp.  TODO: Broken and contains a manual step.  Commit PNGs to
  git.  See `make-res.sh`.
* LOGOS ARE COPYRIGHTED.  Promotional use requires written
  permission by KISS Technologies Gmbh, Zuerich.
